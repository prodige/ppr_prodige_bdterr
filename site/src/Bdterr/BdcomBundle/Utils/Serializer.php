<?php

namespace App\Bdterr\BdcomBundle\Utils;

use JMS\Serializer\SerializationContext;
use JsonSerializable;

class Serializer {
  /**
   * Serialize an entity as a json string.
   * 
   * @see http://jmsyst.com/libs/serializer/master/cookbook/exclusion_strategies
   * 
   * @param Object $entity The entity to serialize
   * @param array  $groups A list of serialization groups (see link above)
   * 
   * @return string The entity formatted as a json string
   */
  public function jsonSerializeEntity($entity, array $groups = null) {
    $serializer = $this->get('jms_serializer');
    $serializer instanceof \JMS\Serializer\Serializer;
    $context = new SerializationContext();
    $context->setSerializeNull(true);
    $context->enableMaxDepthChecks();
    if ($groups) {
      $context->setGroups($groups);
    }

    return $serializer->serialize($entity, 'json', $context);
  }
}