<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Bdterr\BdcomBundle\Utils;

use Doctrine\DBAL\Connection;

/**
 * Description of MessageCode
 *
 * @author ndurand
 */
class PostgresqlUtil {
  /**
   * 
   */
  public static function checkOneColumnInTable($conn, $table, $columnName){
    $sql = "SELECT column_name FROM information_schema.columns 
            WHERE table_name=:table_name and column_name = :columnName"; //"and column_name=:column_name";

    $sth = $conn->prepare($sql);
    $sth->execute(array('table_name' => $table, 'columnName' => $columnName ) );

    $result = $sth->fetchAll();

    return !empty($result);
  }
  
  /**
   * 
   */
  public static function checkColumnsInTable($conn, $table, $columnsFilters = array()){
    $sql = "SELECT column_name FROM information_schema.columns 
            WHERE table_name=:table_name"; //"and column_name=:column_name";

    $sth = $conn->prepare($sql);
    $sth->execute(array('table_name' => $table ) );

    $result = $sth->fetchAll();

    $isOk = true;
    $columnInDb = array();
    foreach($result as $key => $column){
      $columnInDb[] = $column["column_name"];
    }

    foreach($columnsFilters as $key => $column){
      $isOk = $isOk && (in_array($column, $columnInDb) );
    }
    //$isOk = array($result, $columnsFilters);

    return  $isOk;
  }


  /**
   * 
   */
  public static function getColumnInTableWithFilter($conn, $table, $columnsFilters = array()){
    $sql = "SELECT column_name FROM information_schema.columns 
            WHERE table_name=:table_name"; //"and column_name=:column_name";

    $sth = $conn->prepare($sql);
    $sth->execute(array('table_name' => $table ) );

    $result = $sth->fetchAll();

    $columns = array();

    foreach($result as $key => $column){
      if(isset($column['column_name']) ){
        if( empty($columnsFilters) ){
          $columns[] = $column['column_name'];
        }
        else if( in_array($column['column_name'], $columnsFilters) ){
          $columns[] = $column['column_name'];
        }
      } 
   
    }

    return $columns;
  }
 
  /**
   * Vérifie si la table en paramètre existe
   * @param $conn Connection pdo
   * @param $table nom de la table
   * 
   * @return true si table exist sinon false
   */
  public static function checkIfTableExist($conn, $table){
    $sql = "SELECT EXISTS (SELECT relname FROM pg_class WHERE relname = :table_name)";

    $sth = $conn->prepare($sql);
    $sth->execute(array('table_name' => $table));

    $result = $sth->fetchAll();

    if(!empty($result) && isset($result[0]['exists'])){
      return $result[0]['exists'];
    }

    return false;
  }
}
