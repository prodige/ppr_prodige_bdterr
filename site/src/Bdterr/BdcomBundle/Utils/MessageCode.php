<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Bdterr\BdcomBundle\Utils;

/**
 * Description of MessageCode
 *
 * @author mledenmat
 */
abstract class MessageCode {

  const OK = 0;
  const CREATED = 1;
  const UPDATED = 2;
  const EXIST = 3;
  const EXIST_NOT = 5;
  const DELETED = 6;
  const FIELD_NULL = 7;
  const FIELD_NOTUPDATABLEPARAM = 10;
  const LINK_ALREADY_EXISTS = 11;
  const FIELDS_NULL = 12;
  const LINK_DOESNT_EXISTS = 13;
  const LINKED = 14;
  const UDLINKED = 15;
  const ENABLED = 16;
  const DISABLED = 17;
  const FK_NOT_FOUND = 18;
  const UNAUTHORIZED = 19;
  const OTHER_ERROR = 20;
  const EXCEPTION_ERROR = 21;
  const NO_DELETED = 22;
}
