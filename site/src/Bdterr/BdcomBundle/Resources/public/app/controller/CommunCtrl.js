/** */
(function (){

  Ext.define('Bdcom.controller.CommunCtrl',{
    alias: 'load.CommunCtrl',
  });

  /**
   * Chargement des stores avec des promises
   * 
   * @param {array} storeIds id des store
   * 
   * @return false en cas d'erreur, sinon une promise jquery
   */
  function loadStores(storeIds){
    var promises = [];

    if(storeIds && storeIds instanceof Array){
      storeIds.forEach(function(storeId){
        var d = $.Deferred();
        
        promises.push(d);
        store = Ext.StoreMgr.lookup('store.' + storeId)  || Ext.create('Bdcom.store.' + storeId,{storeId : 'store' + storeId});
       
        store.load(function(){
          d.resolve("finish");
        });
      
      });
      if(promises.length > 1){
        return $.when.apply(this, promises);
      }
      else if(promises.length === 1){
        return promises[0].promise();
      }
    }

    return false;
  } 

  /**
   * 
   * @param {*} panel 
   * @param {*} id 
   */
  function activePanel(panel, id, edit){
    
    if(panel && panel.getComponent (id) ){
      var wait  = null;
      // si on a des stores à chargé
      if(typeof panel.getComponent(id).getStoreListId === 'function' ){
        wait = loadStores ( panel.getComponent(id).getStoreListId(edit) );
      }
   
      panel.setActiveItem(id);
    }
  }

  /**
   * 
   * @param {*} id 
   * @param {*} record 
   */
  function switchView(panel, id, record){
    if(panel && panel.getComponent (id) ){
      panel.setActiveItem(id);

      var component = panel.getComponent (id) 

      // Chargement du record dans le component à afficher
      if(record && typeof component.loadRecord === 'function'){
        component.loadRecord(record);
      }

      // Chargement des stores dépendant
      if( typeof panel.loadStoreForView === 'function' || (id === 'tabLot' && record === null) || id === 'formJointure' ){
        panel.loadStoreForView(id, record);
      }

      if(panel.mainId === id && typeof panel.getStoreListId === 'function'){
        loadStores ( panel.getStoreListId() );
      }

    }else{
      console.error("CommunCtrl.alert() => Component", id,  "not found in Panel", panel ? panel.id : panel);
    }
  }
  
  Ext.define('CommunCtrl',{
    statics: {
      loadStores: loadStores,
      activePanel: activePanel,
      switchView: switchView
    }
  });
})();