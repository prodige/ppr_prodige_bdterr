(function(){
  Ext.define('Bdcom.controller.AbstractViewCtrl',{
    requires: ['Bdcom.controller.CommunCtrl'],
    extend : 'Ext.app.ViewController',
    alias: 'controller.AbstractViewCtrl',

    loadStores: CommunCtrl.loadStores,
    activePanel:  CommunCtrl.activePanel,
    switchView:  CommunCtrl.switchView
  });
})();