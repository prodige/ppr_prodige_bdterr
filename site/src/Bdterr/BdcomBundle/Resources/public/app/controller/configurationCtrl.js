/**
 * Controller principale
 */
(function(){
  Ext.define('Bdcom.controller.configurationCtrl', {
    requires: ['Bdcom.controller.AbstractCtrl'],
    extend : 'Bdcom.controller.AbstractViewCtrl',
    alias: 'controller.configurationCtrl',

    deletesTimeout: null,

    /**
     * 
     */
    init: function() {
      //console.log("configCtrl init")
    },
    
    /**
     * 
     * @param {*} combo 
     * @param {*} record 
     */
    onSelectTable(combo, record){
      var store = Ext.StoreMgr.lookup('store.ProdigeColumn') || Ext.create('Bdcom.store.ProdigeColumn',{storeId : 'store.ProdigeColumn'});
  
      var tableName = record.get("couchd_emplacement_stockage");
      if(tableName){
        store.load({
          params:{ table: tableName  }
        })
      }
    },

    /**
     * Evemenet sur un bouton
     * @param {*} widget 
     */
    onWidget(widget){
     
      if(widget.toView){
        this.onSwitchView(widget.toView, widget, true);
      }
    },
    
    /**
     * Evemenet sur un groupe de bouton
     * @param {*} button 
     */
    onButtonGroupEdit(button){
      var widget  =  typeof button.up === 'function' ? button.up() : null;
      
      if(button.toView){
        this.onSwitchView(button.toView, widget, true);
      }
    },

    /**
     * Supression
     * @param {*} button 
     */
    onButtonGroupDelete(button){
      var widget  =  typeof button.up === 'function' ? button.up() : null;

      this.onDeleteButton(widget);
    },
  
    /**
     * Ouverture du formulaire avec le record du widget
     */
    onSwitchView: function(view, widget, withRecord){
      var record = null;
      if(withRecord && widget && typeof widget.getWidgetRecord === 'function'){

        record =  widget.getWidgetRecord();
      }

      var panelCtrl =  widget.ref ? Ext.getCmp(widget.ref) : null;

      if(!panelCtrl){
        console.warn("PanelCtrl ", widget.ref, " not found in Ext.getCmp")
      } 
      

      this.switchView(panelCtrl, view, record );
    }, 

    /**
     * Quand on veut suprimmer un record d'un widget
     */
    onDeleteButton: function( widget ){
      var self = this;
      if(widget && typeof widget.getWidgetRecord === 'function'){
        var record =  widget.getWidgetRecord();
        
        var panelCtrl =  widget.ref ? Ext.getCmp(widget.ref) : null;

        if( record ) {
          record.erase({
            callback: function(record, operation, success){
              if (record.get('prodigeKey') !== undefined){
                var form = Ext.getCmp('tabLot');
                if( form.getComponent("gridChamp")  && typeof  form.getComponent("gridChamp").loadStoreByLot === 'function' && record && record.get('lotId') ){
                  form.getComponent("gridChamp").loadStoreByLot(record.get('lotId'));// Reload fields
                }
              } else {
                if( panelCtrl && typeof panelCtrl.getStoreListId === 'function' ){
                  self.loadStores ( panelCtrl.getStoreListId() );
                }
              }
              Util.showMessageServer(operation, success);
            }
          })
        }
      }
    },
  });
})();