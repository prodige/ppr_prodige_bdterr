/**
 * Controller principale de l'application
 */
(function(){

  Ext.define('Bdcom.controller.mainCtrl', {
    extend : 'Bdcom.controller.AbstractCtrl',
    requires: ['Bdcom.controller.AbstractCtrl'],
    routes : {
      // Route menu
      'nav-:id' : 'onNav',
      'nav-:id-:edit' : 'onEdit',
    },
    init: function() {
    
    },
    onEdit: function(id, edit){
      //console.log(id, edit);
      this.activePanel(this.mainPanel, id, edit);
    },
    onNav : function(id) {
      var self = this;
      //console.log(id, id.indexOf('edit'));
      if(id.indexOf('edit') === -1){
        this.activePanel(this.mainPanel, id);
      }
      
    },
    listen: {
      controller: {
        '#': {
          unmatchedroute: 'onUnmatchedRoute'
        }
      }
    },
    onUnmatchedRoute: function(){
      console.warn("onUnmatchedRoute");
    },

    mainPanel: null
  });
})();