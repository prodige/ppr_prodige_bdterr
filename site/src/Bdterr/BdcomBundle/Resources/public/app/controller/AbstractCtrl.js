(function() {
  Ext.define('Bdcom.controller.AbstractCtrl',{
    extend : 'Ext.app.Controller',
    requires: ['Bdcom.controller.CommunCtrl'],
    alias: 'controller.AbstractCtrl',

    loadStores: CommunCtrl.loadStores,
    activePanel:  CommunCtrl.activePanel,
    switchView:  CommunCtrl.switchView
  });
})();