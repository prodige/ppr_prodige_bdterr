/**
 * Personalisation des column des arbres extJs 5 pour pouvoir utilisé des icon de font-awesome 4.7
 */
(function(){
  //TODO: les mettres en paramètre
  var _classForTree = 'bdcom-tree-color-icon'
  var _iconParent = 'fa fa-lg fa-folder';
  var _iconChild  = 'fa fa-lg fa-file';

  Ext.define('Bdcom.components.CustomTreeColumn', {
    extend: 'Ext.tree.Column',
    xtype: 'Bdcom.CustomTreeColumn',
    cellTpl: [
      '<tpl for="lines">',
          '<img src="{parent.blankUrl}" class="{parent.childCls} {parent.elbowCls}-img ',
          '{parent.elbowCls}-<tpl if=".">line<tpl else>empty</tpl>"/>',
      '</tpl>',
      '<img src="{blankUrl}" class="{childCls} {elbowCls}-img {elbowCls}',
          '<tpl if="isLast">-end</tpl><tpl if="expandable">-plus {expanderCls}</tpl>"/>',
      '<tpl if="checked !== null">',
          '<input type="button" role="checkbox" <tpl if="checked">aria-checked="true" </tpl>',
              'class="{childCls} {checkboxCls}<tpl if="checked"> {checkboxCls}-checked</tpl>"/>',
      '</tpl>',
      '<tpl if="href">',
          '<a href="{href}" target="{hrefTarget}" class="{textCls} {childCls}">{value}',
          '<i class="{childCls} {baseIconCls} ',
          '{baseIconCls}-<tpl if="leaf">'+ _iconChild +'<tpl else>parent</tpl> ' + _iconParent + '"',
          '<tpl if="icon">style="background-image:url({icon})"</tpl>/>',
      '</a>',
      '<tpl else>',
          '<span class="{textCls} {childCls}">',
          '<i class=" '+ _classForTree  +
          '<tpl if="leaf"> '+_iconChild +' <tpl else> ' + _iconParent + ' </tpl> "',
          '<tpl if="icon">style="background-image:url({icon})"</tpl>></i>',
          '{value}</span>',
      '</tpl>'
    ],
  });
})();
