(function (){
  Ext.define('Bdcom.components.ComboLot',{
    alias: 'Bdcom.load.ComboLot'
  });
  // Tous les thèmes
  var _storeTheme = Ext.StoreMgr.lookup('store.Theme') || Ext.create('Bdcom.store.Theme',{storeId : 'store.Theme'});

  // Map file
  var _storeMap = Ext.StoreMgr.lookup('store.ProdigeMap') || Ext.create('Bdcom.store.ProdigeMap',{storeId : 'store.ProdigeMap'});

  // Référentiel
  var _storeRefIntersection = Ext.StoreMgr.lookup('store.ReferentielIntersection') || Ext.create('Bdcom.store.ReferentielIntersection',{storeId : 'store.ReferentielIntersection'});

  var _storeChamps = Ext.StoreMgr.lookup('store.Champ') || Ext.create('Bdcom.store.Champ',{storeId : 'store.Champ'});

  // Thème Parent
  var _storeThemeParent = Ext.create( 'Ext.data.ChainedStore',{
    source: _storeTheme,
    filters: [ function(item) { 
      var result = _storeTheme.queryRecords("parentId", item.get('id')*1 );
      return ( !item.get('parentId') && result && result.length > 0);
    } ]
  }) 

  // Sous-Thème
  var _storeSousTheme = Ext.create( 'Ext.data.ChainedStore',{
    source: _storeTheme,
    filters: [ function(item) {return false;} ]
  }) 


  /**
   * 
   */
  function onSelectTheme(widget, record){

    _storeSousTheme.clearFilter();
    _storeSousTheme.setFilters ( [ function(item){
      return ( (item.get('parentId')*1) === (record.get('id')*1) );
    }] );
  }

  /**
   * 
   */
  function onChangeTheme(widget, newValue){
    _storeSousTheme.clearFilter();
  
    if(newValue){
      _storeSousTheme.setFilters ( [ function(item){
        return ( (item.get('parentId')*1) === (newValue*1) );
      }] );
      // Pour forçer la sélection dans le combo des sous-thèmes
      var form =  widget.up();
      if(form && form.getRecord() && form.getComponent('comboSousTheme') && form.getRecord().get('themeId')){
        var comboSousTheme = form.getComponent('comboSousTheme');
        comboSousTheme.setValue( form.getRecord().get('themeId') );
      }
    }
    else{
      _storeSousTheme.setFilters ( [ function(item){
        return false;
      }] );
    }
  }

  // Combo thème
  Ext.define('Lot.combobox.theme', {
    requires: ['Bdcom.model.Theme'],
    xtype: "combo.lot.theme",
    extend: "Ext.form.ComboBox",
    //   fieldLabel: 'Table',
    store: _storeThemeParent,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'nom',
    valueField: 'id',
    listeners: {
      select : onSelectTheme,
      change: onChangeTheme
    },
  });

  // Combo sous-thème
  Ext.define('Lot.combobox.sousTheme', {
    requires: ['Bdcom.model.Theme'],
    xtype: "combo.lot.sousTheme",
    extend: "Ext.form.ComboBox",
    //   fieldLabel: 'Table',
    store: _storeSousTheme,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'nom',
    valueField: 'id'
  });

  // Combo Référentiel intersection
  Ext.define('Lot.combobox.refIntersection', {
    requires: ['Bdcom.model.ReferentielRecherche'],
    xtype: "combo.lot.refIntersection",
    extend: "Ext.form.ComboBox",

    store: _storeRefIntersection,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'nom',
    valueField: 'id',
  });

  // Combo Map
  Ext.define('Lot.combobox.Map', {
    requires: ['Bdcom.model.Prodige'],
    xtype: "combo.lot.map",
    extend: "Ext.form.ComboBox",

    store: _storeMap,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'title',
    valueField: 'file',
  });

  var cutting = Ext.create('Ext.data.Store', {
      fields: ['value', 'label'],
      data : [
          {"value":"ref", "label":"Referentiel de découpage"},
          {"value":"file", "label":"Code INSEE attributaire par CSV"},
          {"value":"field", "label":"Code INSEE attributaire en base"}
      ]
  });

  // Combo cutting
  Ext.define('Lot.combobox.Cutting', {
    requires: ['Bdcom.model.Prodige'],
    xtype: "combo.lot.cutting",
    extend: "Ext.form.ComboBox",
    store: cutting,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'label',
    valueField: 'value',
    value: 'ref',
    listeners: {
      select: function(combo, record, index) {

        if (combo.value == 'ref') {
          Ext.get('formLot').component.getComponent('lotTampon').setVisible(true);
          Ext.get('formLot').component.getComponent('bufferHelp').setVisible(true);
          Ext.get('formLot').component.getComponent('cuttingRef').setVisible(true).allowBlank = false;
          Ext.get('formLot').component.getComponent('mappingInsee').setVisible(false).allowBlank = true;
          Ext.get('formLot').component.getComponent('mappingHelp').setVisible(false);
          Ext.get('formLot').component.getComponent('mappingField').setVisible(false).allowBlank = true;
          Ext.get('formLot').component.getComponent('mappingFieldHelp').setVisible(false);
        }

        if (combo.value == 'file') {
          Ext.get('formLot').component.getComponent('mappingInsee').setVisible(true).allowBlank = false;
          Ext.get('formLot').component.getComponent('mappingHelp').setVisible(true);
          Ext.get('formLot').component.getComponent('lotTampon').setVisible(false);
          Ext.get('formLot').component.getComponent('bufferHelp').setVisible(false);
          Ext.get('formLot').component.getComponent('cuttingRef').setVisible(false).allowBlank = true;
          Ext.get('formLot').component.getComponent('mappingField').setVisible(false).allowBlank = true;
          Ext.get('formLot').component.getComponent('mappingFieldHelp').setVisible(false);
        }

        if (combo.value == 'field') {
          Ext.get('formLot').component.getComponent('mappingField').setVisible(true).allowBlank = false;
          Ext.get('formLot').component.getComponent('mappingFieldHelp').setVisible(true);
          Ext.get('formLot').component.getComponent('mappingInsee').setVisible(false).allowBlank = true;
          Ext.get('formLot').component.getComponent('mappingHelp').setVisible(false);
          Ext.get('formLot').component.getComponent('lotTampon').setVisible(false);
          Ext.get('formLot').component.getComponent('bufferHelp').setVisible(false);
          Ext.get('formLot').component.getComponent('cuttingRef').setVisible(false).allowBlank = true;
        }
      }

   }
  });

  var display = Ext.create('Ext.data.Store', {
    fields: ['value', 'label'],
    data : [
        {"value":"bt", "label":"Base territoriale"},
        {"value":"pac", "label":"Porter-à-connaissance"},
        {"value":"both", "label":"Les deux"}
    ]
  });

  // Combo display
  Ext.define('Lot.combobox.Display', {
    requires: ['Bdcom.model.Prodige'],
    xtype: "combo.lot.display",
    extend: "Ext.form.ComboBox",
    store: display,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'label',
    valueField: 'value',
    value: 'bt',
  });

  // Combo display
  Ext.define('Lot.combobox.mapping_fields', {
    requires: ['Bdcom.model.Prodige'],
    xtype: "combo.lot.mapping_fields",
    extend: "Ext.form.ComboBox",
    store: _storeChamps,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'nom',
    valueField: 'nom'
  });

 
})();