(function (){
  Ext.define('Bdcom.components.ComboProdige',{
    alias: 'Bdcom.components.ComboProdige'
  });

  // Store 
  var _storeColumn  = Ext.StoreMgr.lookup('store.ProdigeColumn') || Ext.create('Bdcom.store.ProdigeColumn',{storeId : 'store.ProdigeColumn'});
  var _storeTable   = Ext.StoreMgr.lookup('store.ProdigeTable')  || Ext.create('Bdcom.store.ProdigeTable',{storeId : 'store.ProdigeTable'});
  var _storeProfile = Ext.StoreMgr.lookup('store.Profile')       || Ext.create('Bdcom.store.Profile',{storeId : 'store.Profile'});

  // Prodige Column Tag (Pour champs_api)
  Ext.define('components.prodigeColumn.tag', {
    requires: ['Bdcom.model.Prodige'],
    xtype: "tag.prodigeColumn",
    extend: "Ext.form.field.Tag",
    fieldLabel: 'Table',

    queryMode: 'local',
    displayField: 'column_name',
    valueField: 'column_name',

    initComponent : function(){
      this.store = _storeColumn;

      this.callParent(arguments);
    },
  });

  // Catalogue profile
  Ext.define('components.profile.tag', {
    xtype: "tag.profile",
    extend: "Ext.form.field.Tag",
    fieldLabel: 'Table',

    queryMode: 'local',
    displayField: 'nom',
    valueField: 'id',

    initComponent : function(){
      this.store = _storeProfile;

      this.callParent(arguments);
    },
  });

  // Prodige Column Combobox
  Ext.define('components.prodigeColumn.combobox', {
    requires: ['Bdcom.model.Prodige'],
    xtype: "combo.prodigeColumn",
    extend: "Ext.form.ComboBox",
    fieldLabel: 'Table',

    queryMode: 'local',
    displayField: 'column_name',
    valueField: 'column_name',

    initComponent : function(){
      this.store = _storeColumn;
  
      this.callParent(arguments);
    },
  });
  
  // Prodige table Combobox
  Ext.define('components.prodigeTable.combobox', {
    requires: ['Bdcom.model.Prodige', 'Bdcom.controller.configurationCtrl'],
    xtype: "combo.prodigeTable",
    extend: "Ext.form.ComboBox",
    fieldLabel: 'Table',

    queryMode: 'local',

    displayField: 'couchd_emplacement_stockage',
    valueField: 'couchd_emplacement_stockage',
    
    listeners: {
      select : 'onSelectTable'
    },

    initComponent : function(){
      this.store = _storeTable;
  
      this.callParent(arguments);
    },
  });
})();