(function(){
  Ext.define('Bdcom.components.ComboScheduler',{
    alias: 'Bdcom.components.ComboScheduler'
  });
  /***************/
  /*** DATA    ***/
  /***************/

  // Fréquence
  var frequency = Ext.create('Ext.data.Store', {
    fields: ['name', 'value'],
    data : [
      {"name": "hebdomadaire", "value":"weekly"},
      {"name": "quotidienne",  "value":"daily"},
    ]
  });
  
  // Type de tache
  var taskType = Ext.create('Ext.data.Store', {
    fields: ['name', 'value'],
    data : [
      {"name": "ponctuelle", "value": false },
      {"name": "périodique", "value": true  },
    ]
  });

  // Jours de la semaine
  var days = Ext.create('Ext.data.Store', {
    fields: ['name', 'value'],
    data : [
      { "name": "lundi",    "value": 1 },//Odre selon la norme cron
      { "name": "mardi",    "value": 2 },
      { "name": "mercredi", "value": 3 },
      { "name": "jeudi",    "value": 4 },
      { "name": "vendredi", "value": 5 },
      { "name": "samedi",   "value": 6 },
      { "name": "dimanche", "value": 7 },
    ]
  });
  /******************/
  /*** Fonctions ***/
  /*****************/

  /** */
  function onChangeTastType(widget, isPeriodic){
    var form =  widget.up();
    if(form && form.getComponent('frequency') && form.getComponent('date') && form.getComponent('dayWeek') ){
     form.getComponent('frequency').setDisabled ( !isPeriodic); // Désactivé si ce n'est pas périodique (activé si périodique )
     form.getComponent('dayWeek').setDisabled ( !isPeriodic);

     form.getComponent('date').setDisabled ( isPeriodic); // Désactive si c'est périodique (activé si ponctuelle)

    }
  }

  /** */
  function onChangeFrequency(widget, frequency){
    var form =  widget.up();
     
    if(form && form.getComponent('dayWeek')){
      form.getComponent('dayWeek').setDisabled (frequency !== 'weekly');
    }
  }

  /** */
  function onChangeTime(widget, time){
    var form =  widget.up();
    var date = new Date(time);
    if(form && form.getRecord()){
      var record =  form.getRecord();

      record.set('hour',date.getHours() );
      record.set('minute', date.getMinutes() );
  
    }
  }

  /** */
  function onChangeDate(widget, date){
    var form =  widget.up();
    var date = new Date(date);
    if(date && form && form.getRecord()){
      var record =  form.getRecord();

      record.set('date',Ext.Date.format(date, 'm/d/Y') );
    }
  }

  /************************/
  /***      COMBO      ***/
  /***********************/
  // Combo thème
  Ext.define('Scheduler.combobox.taskType', {
    xtype: "combo.scheduler.taskType",
    extend: "Ext.form.ComboBox",
    store: taskType,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    listeners: {
      change: onChangeTastType
    },
  });

  // Combo thème
  Ext.define('Scheduler.combobox.frequency', {
    xtype: "combo.scheduler.frequency",
    extend: "Ext.form.ComboBox",
    store: frequency,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
    listeners: {
      change: onChangeFrequency
    },
  });

  // Combo Jour
  Ext.define('Scheduler.combobox.days', {
    xtype: "combo.scheduler.days",
    extend: "Ext.form.ComboBox",
    store: days,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'value',
  }); 
  
  //Timefield
  Ext.define('Scheduler.timefield.time', {
    xtype: "timefield.scheduler.time",
    extend: "Ext.form.field.Time",
    increment: 60, 
    format: 'H:i',
    listeners : {
      change: onChangeTime
    }
  });

  // datefield
  Ext.define('Scheduler.datefield.date', {
    xtype: "datefield.scheduler.date",
    extend: "Ext.form.field.Date",

    listeners : {
      change: onChangeDate
    }
  });
})();