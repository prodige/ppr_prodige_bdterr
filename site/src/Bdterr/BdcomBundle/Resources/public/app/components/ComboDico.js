(function (){
  Ext.define('Bdcom.components.ComboDico',{
    alias: 'Bdcom.load.ComboDico'
  });


  // Dico générique
  Ext.define('Lot.combobox.Dico', {
    requires: ['Bdcom.model.Dico'],
    xtype: "combo.dico",
    extend: "Ext.form.ComboBox",
 //   fieldLabel: 'Table',
    forceSelection: true,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'id',
  });
 
})();