(function(){
  /** Définition des Profile tiers des lot de données*/
  Ext.define('Bdcom.model.Profile', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'time',  type: 'string' },
      {name: 'nom',   type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'description',            type: 'string'},
      {name: 'isDefaultInstallation',  type: 'boolean'},
      {name: 'nomLdap',type: 'string'},
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      batchActions: true, //<------
      url : Routing.generate('bdcom_api_catalogue') +"/profiles",
      actionMethods : { read: 'GET'  },
      reader: {
        type: 'json',
        rootProperty: 'Profile'
      }
    },
  });
  
  /**  Le store de Profile */
  Ext.define('Bdcom.store.Profile', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Profile',
    model: 'Bdcom.model.Profile',
  });

  Ext.StoreMgr.lookup('store.Profile') || Ext.create('Bdcom.store.Profile',{storeId : 'store.Profile'});

})();