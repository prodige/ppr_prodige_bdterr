(function(){
  Ext.define('Bdcom.model.ReferentielCarto', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'referentielTable',  type: 'string' },
      {name: 'apiChamps', type: 'auto', defaultValue: null}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('bdcom_api_referentiel_carto'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'ReferentielCarto'
      }
    }
  });

  Ext.define('Bdcom.item.ReferentielCarto', {
    requires: ['Bdcom.components.ComboProdige'],

    alias: 'item.ReferentielCarto',

    items : [ //  allowBlank: false => champ obligatoire 
      { name: 'referentielTable',     fieldLabel : "Table",              allowBlank: false, xtype: "combo.prodigeTable"},
      { name: 'apiChamps',            fieldLabel : "Champs délivrés par API",         allowBlank: true,  xtype: "tag.prodigeColumn", itemId: 'tagApiChamps' },
    ]
  });

  Ext.define('Bdcom.store.ReferentielCarto', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.ReferentielCarto',
    model: 'Bdcom.model.ReferentielCarto',
    sortOnLoad: true,
    sorters: {
      property : 'referentielTable',
      direction: 'ASC'
    }
  });

  Ext.StoreMgr.lookup('store.ReferentielCarto') || Ext.create('Bdcom.store.ReferentielCarto',{storeId : 'store.ReferentielCarto'});
})();