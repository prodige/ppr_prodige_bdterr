(function () {
    var _fields = [
        {name: 'id', type: 'string'},
        {name: 'nom', type: 'string'},
        {name: 'alias', type: 'string'},
        {name: 'icon', type: 'string'},
        {name: 'ordre', type: 'number'},
        {name: 'isTheme', type: 'boolean'},
        {name: 'parentId', type: 'number'}
    ];

    Ext.define('Bdcom.model.ThemeChildren', {
        extend: 'Ext.data.TreeModel',
        fields: _fields,
    });

    Ext.define('Bdcom.model.Theme', {
        extend: 'Ext.data.Model',
        fields: _fields,
    });

    Ext.define('Bdcom.treeModel.Theme', {
        extend: 'Ext.data.TreeModel',
        fields: _fields,
        proxy: {
            type: 'rest',
            typeProperty: "id",
            idParam: 'id',

            url: Routing.generate('bdcom_api_theme'),

            actionMethods: {create: 'POST', update: 'PATCH', destroy: 'DELETE'},

            reader: {
                type: 'json',
                rootProperty: 'Theme'
            }
        },
        childType: 'Bdcom.model.ThemeChildren'
    });

    Ext.define('Bdcom.treeStore.Theme', {
        extend: 'Ext.data.TreeStore',
        alias: 'Bdcom.treeStore.Theme',
        model: 'Bdcom.treeModel.Theme',
        expanded: true,
        autoLoad: true,
        proxy: {
            type: 'rest',
            typeProperty: "id",
            idParam: 'id',

            url: Routing.generate('bdcom_api_theme'),

            actionMethods: {create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE'},

            reader: {
                type: 'json',
                rootProperty: 'Theme'
            }
        },
        sortOnLoad: true,
        sorters: {
            property: 'ordre',
            direction: 'ASC'
        }
    });

    Ext.define('Bdcom.store.Theme', {
        extend: 'Ext.data.Store',
        alias: 'Bdcom.store.Theme',
        model: 'Bdcom.model.Theme',
        proxy: {
            type: 'rest',
            typeProperty: "id",
            idParam: 'id',

            url: Routing.generate('bdcom_api_theme'),

            actionMethods: {create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE'},

            reader: {
                type: 'json',
                rootProperty: 'Theme'
            }
        },
        sortOnLoad: true,
        sorters: {
            property: 'ordre',
            direction: 'ASC'
        }
    });

    Ext.StoreMgr.lookup('treeStore.Theme') || Ext.create('Bdcom.treeStore.Theme', {storeId: 'treeStore.Theme'});
    Ext.StoreMgr.lookup('store.Theme') || Ext.create('Bdcom.store.Theme', {storeId: 'store.Theme'});

    var iconHelpText = "Le format attendu est le nom de la classe de l'icône (exemple : fa-address-book). La liste des icônes disponible est accessible à <a href='https://fontawesome.com/v4/icons/' target='_blank'>cette adresse</a>.";

    Ext.define('Bdcom.item.Theme', {
        alias: 'item.Theme',

        items: [ //  allowBlank: false => champ obligatoire
            {name: 'nom', fieldLabel: "Nom", allowBlank: false, xtype: "textfield"},
            {name: 'icon', fieldLabel: "Icône", allowBlank: true, id:'themeIcon', xtype: "textfield"},
            {
                name: 'icon_help',
                text: "Aide",
                width: 50,
                height: 25,
                xtype: "button",
                id: "iconHelp",
                handler: function () {
                    Ext.Msg.alert('Aide', iconHelpText);
                }
            },
            {name: 'alias', fieldLabel: "Alias", allowBlank: true, xtype: "textfield"},
        ]
    });
})();