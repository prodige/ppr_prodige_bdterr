(function(){
  Ext.define('Bdcom.model.ReferentielIntersection', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'referentielTable',  type: 'string' },
      {name: 'insee', type: 'string'},
      {name: 'inseeDeleguee', type: 'string'},
      {name: 'nom', type: 'string'},
      {name: 'statisticIntersect',  type: 'boolean'},
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('bdcom_api_referentiel_intersection'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'ReferentielIntersection'
      }
    }
  });

  Ext.define('Bdcom.item.ReferentielIntersection', {
    alias: 'item.ReferentielIntersection',
    items : [ //  allowBlank: false => champ obligatoire 
      { name: 'nom',               fieldLabel : "Nom",                allowBlank: false },
      { name: 'referentielTable',  fieldLabel : "Table",              allowBlank: false, xtype: "combo.prodigeTable"  },
      { name: 'insee',             fieldLabel : "Champ code Insee",         allowBlank: true,  xtype: "combo.prodigeColumn" },
      { name: 'inseeDeleguee',     fieldLabel : "Champ code Insee communes déléguées", allowBlank: true,  xtype: "combo.prodigeColumn" },
      { name: 'statisticIntersect',fieldLabel : "Referentiel utilisé pour les statistiques", allowBlank: true,  xtype: "checkboxfield" },
    ]
  });

  Ext.define('Bdcom.store.ReferentielIntersection', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.ReferentielIntersection',
    model: 'Bdcom.model.ReferentielIntersection',
    sortOnLoad: true,
    sorters: {
      property : 'referentielTable',
      direction: 'ASC'
    }
  });

  Ext.StoreMgr.lookup('store.ReferentielIntersection') || Ext.create('Bdcom.store.ReferentielIntersection',{storeId : 'store.ReferentielIntersection'});
})();

