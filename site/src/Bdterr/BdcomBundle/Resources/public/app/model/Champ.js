(function(){
  /** Définition des Champ tiers des lot de données*/
  Ext.define('Bdcom.model.Champ', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',     type: 'int'},
      {name: 'lotId',  type: 'int' },
      {name: 'nom',    type: 'string'},
      {name: 'ordre',  type: 'int'},
      {name: 'fiche',  type: 'boolean'},
      {name: 'resultats',  type: 'boolean'},
      {name: 'identifiant',type: 'boolean'},
      {name: 'label',  type: 'boolean'},
      {name: 'typeId', type: 'int'}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      batchActions: true, //<------
      url : Routing.generate('bdcom_api_champs'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'CoucheChamp'
      }
    },
  });
  
  /**  Le store de Champ */
  Ext.define('Bdcom.store.Champ', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Champ',
    model: 'Bdcom.model.Champ',
    sortOnLoad: true,
    name: 'Couche champs',
    sorters: {
      property : 'ordre',
      direction: 'ASC'
    },
  });

  Ext.StoreMgr.lookup('store.Champ') || Ext.create('Bdcom.store.Champ',{storeId : 'store.Champ'});

})();