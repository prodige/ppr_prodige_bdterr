(function(){
  /** Définition des Contenu tiers des lot de données*/
  Ext.define('Bdcom.model.Contenu', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'lotId', type: 'int' },
      {name: 'nom',   type: 'string'},
      {name: 'ordre', type: 'int'},
      {name: 'fiche', type: 'boolean'},
      {name: 'resultats', type: 'boolean'},
      {name: 'url', type: 'string'},
      {name: 'typeId', type: 'int'}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      batchActions: true, //<------
      url : Routing.generate('bdcom_api_contenu'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'ContenuTier'
      }
    },
  });
  
  /**  Le store de Contenu */
  Ext.define('Bdcom.store.Contenu', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Contenu',
    model: 'Bdcom.model.Contenu',
    sortOnLoad: true,
    name: 'Contenu tiers',
    sorters: {
      property : 'ordre',
      direction: 'ASC'
    }
  });

  Ext.StoreMgr.lookup('store.Contenu') || Ext.create('Bdcom.store.Contenu',{storeId : 'store.Contenu'});

})();