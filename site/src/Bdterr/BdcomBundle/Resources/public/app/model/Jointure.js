(function(){
  /** Définition des Champ tiers des lot de données*/
  Ext.define('Bdcom.model.Jointure', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',     type: 'int'},
      {name: 'lotId',  type: 'int' },
      {name: 'lotField',    type: 'string'},
      {name: 'prodigeTable',  type: 'string'},
      {name: 'prodigeKey',  type: 'string'},
      {name: 'prodigeField',  type: 'string'}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      batchActions: true, //<------
      url : Routing.generate('bdcom_api_jointures'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'CoucheJointure'
      }
    },
  });

  Ext.define('Bdcom.item.Jointure', {
    requires: ['Bdcom.components.ComboLot', 'Bdcom.components.ComboProdige'],

    alias: 'item.Jointure',

    items : [ //  allowBlank: false => champ obligatoire
      { name: 'lotId',            allowBlank: false, xtype: "hiddenfield", itemId: 'lotIdHidden'},
      { name: 'lotField',         fieldLabel : "Clé primaire",              allowBlank: false, xtype: "combo.lot.mapping_fields"},
      { name: 'prodigeTable',     fieldLabel : "Table jointe",              allowBlank: false, xtype: "combo.prodigeTable"},
      { name: 'prodigeKey',       fieldLabel : "Clé étrangère",         allowBlank: false,  xtype: "combo.prodigeColumn", itemId: 'tagApiKey' },
      { name: 'prodigeField',     fieldLabel : "Attributs",         allowBlank: false,  xtype: "tag.prodigeColumn", itemId: 'tagApiChamps' },
    ]
  });
  
  /**  Le store de Champ */
  Ext.define('Bdcom.store.Jointure', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Jointure',
    model: 'Bdcom.model.Jointure',
    sortOnLoad: true,
    name: 'Couche jointure',
    sorters: {
      property : 'ordre',
      direction: 'ASC'
    },
  });

  Ext.StoreMgr.lookup('store.Jointure') || Ext.create('Bdcom.store.Jointure',{storeId : 'store.Jointure'});

})();