(function(){
  // Pour pouvoir charger Prodige.js
  Ext.define('Bdcom.model.Prodige', { 
    alias: 'Bdcom.model.prodige'
  }); 
  
  // Tables Prodige
  Ext.define('Bdcom.model.ProdigeTable',{
    extend: 'Ext.data.Model',
    fields: [
      {name: 'nom',    type: 'string'},
      {name: 'couchd_emplacement_stockage',  type: 'string'},
    ],
    proxy: {
      type: 'rest',
      url : Routing.generate('bdcom_prodige_table'),
      actionMethods : {read: 'GET'},
      reader: {
        type: 'json',
        rootProperty: 'ProdigeTable'
      }
    }
  });

 
  Ext.define('Bdcom.store.ProdigeTable', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.ProdigeTable',
    model: 'Bdcom.model.ProdigeTable',
    sortOnLoad: true,
    sorters: {
      property : 'couchd_emplacement_stockage',
      direction: 'ASC'
    }
  });

  // Column des tables
  Ext.define('Bdcom.model.ProdigeColumn',{
    extend: 'Ext.data.Model',
    fields: [
      {name: 'column_name',    type: 'string'},
    ],
    proxy: {
      type: 'rest',
      url : Routing.generate('bdcom_prodige_column'),
      actionMethods : {read: 'GET'},
      reader: {
        type: 'json',
        rootProperty: 'ProdigeColumn'
      }
    }
  });
  
  Ext.define('Bdcom.store.ProdigeColumn', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.ProdigeColumn',
    model: 'Bdcom.model.ProdigeColumn',
  });
  
  // Map server
  Ext.define('Bdcom.model.ProdigeMap',{
    extend: 'Ext.data.Model',
    fields: [
      {name: 'file',    type: 'string'},
      {name: 'title',  type: 'string'},
      {name: 'wmsmetadata_uuid',  type: 'string'},
      {name: 'url_file',  type: 'string'}
    ],
    proxy: {
      type: 'rest',
      url : Routing.generate('bdcom_prodige_map'),
      actionMethods : {read: 'GET'},
      reader: {
        type: 'json',
        rootProperty: 'Map'
      }
    }
  });

  Ext.define('Bdcom.store.ProdigeMap', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.ProdigeMap',
    model: 'Bdcom.model.ProdigeMap',
    sortOnLoad: true,
    sorters: {
      property : 'title',
      direction: 'ASC'
    }
  });


  // création des store
  Ext.StoreMgr.lookup('store.ProdigeTable') || Ext.create('Bdcom.store.ProdigeTable', {storeId : 'store.ProdigeTable'});
  Ext.StoreMgr.lookup('store.ProdigeColumn')|| Ext.create('Bdcom.store.ProdigeColumn',{storeId : 'store.ProdigeColumn'});
  Ext.StoreMgr.lookup('store.ProdigeMap')   || Ext.create('Bdcom.store.ProdigeMap',   {storeId : 'store.ProdigeMap'});

})();