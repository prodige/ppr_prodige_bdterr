(function(){
  Ext.define('Bdcom.model.Scheduler', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',         type: 'int'},
      {name: 'name',       type: 'string' },
      {name: 'isPeriodic', type: 'boolean'},
      {name: 'frequency' , type: 'string'},
      {name: 'year' ,      type: 'string'},
      {name: 'hour' ,      type: 'string'},
      {name: 'minute' ,    type: 'string'},
      {name: 'date' ,      type: 'string'},
      {name: 'month' ,     type: 'string'},
      {name: 'dayWeek' ,   type: 'string'},
      {name: 'dayMonth' ,  type: 'string'},
      {name: 'enabled' ,   type: 'boolean'},
    
    ], 
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('scheduler_api') + '/command',
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'Scheduler'
      }
    }
  });

  Ext.define('Bdcom.item.Scheduler', {
    requires: ['Bdcom.components.ComboScheduler'],
    alias: 'item.Scheduler',

    items : [ //  allowBlank: false => champ obligatoire 
      { name: 'name',       fieldLabel: "Nom",                allowBlank: false, },
      { name: 'isPeriodic', fieldLabel: "Type de tâche",      allowBlank: false, xtype:"combo.scheduler.taskType" },
      { name: 'frequency',  fieldLabel: "Fréquence",          allowBlank: false, xtype:"combo.scheduler.frequency",id:"frequency", disabled: true },
      { name: "timeField",  fieldLabel: "Heure ",             allowBlank: false, xtype:"timefield.scheduler.time", id: "time", minValue: '0:00',   maxValue: '24:00', increment: 60},
      { name: "dateField",  fieldLabel: "Date",               allowBlank: true,  xtype:"datefield.scheduler.date", id: "date", disabled: true, format: 'd/m/Y' },
      { name: "dayWeek",    fieldLabel: "Jour",               allowBlank: true,  xtype:"combo.scheduler.days",     id: "dayWeek",  disabled: true, minValue: new Date() },
      { name: "enabled",    fieldLabel: "Activée",            allowBlank: true,  xtype:"checkboxfield" },

    ]
  });

  Ext.define('Bdcom.store.Scheduler', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Scheduler',
    model: 'Bdcom.model.Scheduler'
  });

  Ext.StoreMgr.lookup('store.Scheduler') || Ext.create('Bdcom.store.Scheduler',{storeId : 'store.Scheduler'});
})();