(function(){
  Ext.define('Bdcom.model.Dico', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'nom',  type: 'string' },
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('bdcom_api_catalogue') + '/dicos',
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'Dico'
      }
    }
  });


  Ext.define('Bdcom.store.Dico', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Dico',
    model: 'Bdcom.model.Dico',
    sortOnLoad: true,
    sorters: {
      property : 'name',
      direction: 'ASC'
    }
  });
})();