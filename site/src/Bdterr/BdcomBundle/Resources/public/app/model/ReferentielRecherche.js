(function(){
  Ext.define('Bdcom.model.ReferentielRecherche', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'referentielTable',  type: 'string' },
      {name: 'insee', type: 'string'},
      {name: 'inseeDeleguee', type: 'string'},
      {name: 'commune', type: 'string'},
      {name: 'nom', type: 'string'},
      {name: 'apiChamps', type: 'auto', defaultValue: null}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('bdcom_api_referentiel_recherche'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'ReferentielRecherche'
      }
    }
  });

  Ext.define('Bdcom.item.ReferentielRecherche', {
    requires: ['Bdcom.components.ComboProdige'],

    alias: 'item.ReferentielRecherche',

    items : [ //  allowBlank: false => champ obligatoire 
      { name: 'referentielTable',  fieldLabel : "Table",              allowBlank: false, xtype: "combo.prodigeTable"},
 //     { name: 'referentielTable',  fieldLabel : "Table",              allowBlank: false },
      { name: 'insee',             fieldLabel : "Champ code INSEE",         allowBlank: true,  xtype: "combo.prodigeColumn" },
      { name: 'inseeDeleguee',     fieldLabel : "Champ code INSEE communes déléguées ", allowBlank: true,  xtype: "combo.prodigeColumn" },
      { name: 'commune',           fieldLabel : "Champ Commune",            allowBlank: true,  xtype: "combo.prodigeColumn" },
      { name: 'nom',               fieldLabel : "Champ",                allowBlank: true,  xtype: "combo.prodigeColumn" },
      { name: 'apiChamps',         fieldLabel : "Champs délivrés par API",         allowBlank: true,  xtype: "tag.prodigeColumn", itemId: 'tagApiChamps' },
    ]
  });

  Ext.define('Bdcom.store.ReferentielRecherche', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.ReferentielRecherche',
    model: 'Bdcom.model.ReferentielRecherche',
    sortOnLoad: true,
    sorters: {
      property : 'referentielTable',
      direction: 'ASC'
    }
  });

  Ext.StoreMgr.lookup('store.ReferentielRecherche') || Ext.create('Bdcom.store.ReferentielRecherche',{storeId : 'store.ReferentielRecherche'});
})();