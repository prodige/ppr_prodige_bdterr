(function(){
  /** Définition des lot de données*/
  Ext.define('Bdcom.model.Rapport', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'nom',  type: 'string' },
      {name: 'gabaritHtml', type: 'string'},
      {name: 'gabaritOdt', type: 'string'},
      {name: 'profilIds', type: 'auto', defaultValue: null}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('bdcom_api_rapport'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'Rapport'
      }
    },
  });

  /** Définition des champs du formulaire d'édition  */
  Ext.define('Bdcom.item.Rapport', {
    alias: 'item.Rapport',
    requires:['Bdcom.components.ComboProdige'],
    items : [ //  allowBlank: false => champ obligatoire Rapport.combobox.theme
      { name: 'nom',          fieldLabel : "Nom *",           allowBlank: false, xtype: "textfield" },
      { name: 'profilIds',    fieldLabel : "Profil prodige",  tooltip : 'Liste des profils concernés par ce rapport', allowBlank: false, xtype: "tag.profile", itemId: 'tagProfil' },
      { name: 'gabaritHtml',  fieldLabel : "Gabarit Html *",  tooltip : 'Nom du gabarit de fichier html utilisé pour le rapport', allowBlank: false, xtype: "textfield" },
      { name: 'gabaritOdt',   fieldLabel : "Gabarit Odt *",   tooltip : 'Nom du gabarit de fichier odt utilisé pour le rapport', allowBlank: false, xtype: "textfield" }//_storeProfile
    ]
  });
  
  /**  Le store de Rapport */
  Ext.define('Bdcom.store.Rapport', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Rapport',
    model: 'Bdcom.model.Rapport'
  });

  Ext.StoreMgr.lookup('store.Rapport') || Ext.create('Bdcom.store.Rapport',{storeId : 'store.Rapport'});

  
})();