(function(){
  // Lot de donnée
  var _storeLot =  Ext.StoreMgr.lookup('store.Lot') || Ext.create('Bdcom.store.Lot',{storeId : 'store.Lot'});

  /** Série de donnée  */
  Ext.define('Bdcom.model.DataSerie', {
    requires: ['Bdcom.model.Lot'],
    extend: 'Ext.data.Model',
    fields: [
      {name: 'title',         type: 'string'},
      {name: 'nom',           type: 'string'},
      {name: 'description',   type: 'string'},
      {name: 'table_postgis', type: 'string'},
    ],
    inLot: function(){
     
      var records =  _storeLot.query('lotTable', this.get('table_postgis'))
  
      this.set('_isInDataLotValue',   records && records.count() > 0);
    
    
      return this.get('_isInDataLotValue');
    },
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('bdcom_api_catalogue') + "/dataserie",
      actionMethods : { read: 'GET' },
      reader: {
        type: 'json',
        rootProperty: 'DataSerie'
      }
    }
  });

  /** Store */
  Ext.define('Bdcom.store.DataSerie', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.DataSerie',
    model: 'Bdcom.model.DataSerie'
  });

  Ext.StoreMgr.lookup('store.DataSerie') || Ext.create('Bdcom.store.DataSerie',{storeId : 'store.DataSerie'});
})();