(function(){
  Ext.define('Bdcom.model.Report', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',         type: 'int'},
      {name: 'commandName',type: 'string' },
      {name: 'dateTime',   type: 'string'},
      {name: 'url',   type: 'string'},
    ], 
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('scheduler_api') + '/reports',
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'ExecutionReport'
      }
    }
  });

  Ext.define('Bdcom.store.Report', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Report',
    model: 'Bdcom.model.Report',
    sortOnLoad: true,
    sorters: {
      property : 'dateTime',
      direction: 'DESC'
    }
  });

  Ext.StoreMgr.lookup('store.Report') || Ext.create('Bdcom.store.Report',{storeId : 'store.Report'});
})();