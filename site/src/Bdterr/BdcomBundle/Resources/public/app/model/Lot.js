(function(){
  /** Définition des lot de données*/
  Ext.define('Bdcom.model.Lot', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'uuid',  type: 'string' },
      {name: 'lotTable', type: 'string'},
      {name: 'alias', type: 'string'},
      {name: 'avertissementMessage', type: 'string'},
      {name: 'carte', type: 'string'},
      {name: 'themeId', type: 'string'},
      {name: 'referentielId', type: 'string'},
      {name: 'statistiques', type: 'boolean'},
      {name: 'visualiseur', type: 'boolean'},
      {name: 'cuttingType', type: 'string'},
      {name: 'buffer', type: 'int'},
      {name: 'mapping', type: 'string'},
      {name: 'mappingField', type: 'string'},
      {name: 'displayType', type: 'string'}
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      batchActions: true, //<------
      url : Routing.generate('bdcom_api_lot'),
      actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
      reader: {
        type: 'json',
        rootProperty: 'Lot'
      }
    },
    isConfigured: function(){
  
      var fields = [ 'uuid','lotTable', 'alias', 'themeId','referentielId' ];
      var test = true;
      
      var self = this;

      fields.forEach(function(field){
        test = test && field  && typeof self.get(field) !== 'undefined' && !!self.get(field);
      });

        
      return test;
    },
  });

  var cuttingHelpText = "le découpage des données s’effectue au choix par un découpage spatial avec ou sans zone tampon ou par code INSEE attributaire csv ou directement en base";
  var bufferHelpText = "Le tampon (en mètre) positif ou négatif sur cette couche permet d'augmenter ou réduire la surface de découpage. Laisser à 0 si vous ne souhaitez pas en appliquer";
  var mappingHelpText = "Charger une table de correspondance code objet / code INSEE pour que les objets soient découpés par commune. Format attendu du CSV : Sans en-tête et séparateur ','";
  var mappingFieldHelpText = "Pour le découpage spatial, choisissez le champ contenant les code-insee";

  /** Définition des champs du formulaire d'édition  */
  Ext.define('Bdcom.item.Lot', {
    alias: 'item.Lot',
    requires: ['Bdcom.components.ComboLot'],
    items : [ //  allowBlank: false => champ obligatoire Lot.combobox.theme
      { name: 'lotTable',             fieldLabel : "Table ",                             allowBlank: false, xtype: "textfield", id:'lotTable', disabled: true, },
      { name: 'alias',                fieldLabel : "Alias* ",                             allowBlank: false, xtype: "textfield" },
      { name: 'themeParent',          fieldLabel : "Thème* ",                             allowBlank: false, xtype: "combo.lot.theme" },
      { name: 'themeId',              fieldLabel : "Sous-Thème* ",                        allowBlank: false, xtype: "combo.lot.sousTheme", id: 'comboSousTheme' },
      { name: 'avertissementMessage', fieldLabel : "Message d'avertissement ",            allowBlank: true,  xtype: "textarea" },
      { name: 'carte',                fieldLabel : "Carte associée ",                     allowBlank: true,  xtype: "combo.lot.map" },
      { name: 'cuttingType',          fieldLabel : "Type de découpage ",                  allowBlank: false, xtype: "combo.lot.cutting", id: 'comboCutting' },
      { name: 'cutting_help',         text : "Aide",                                      width:50, height:25, xtype: "button",  id:"cuttingHelp", handler: function(){
        Ext.Msg.alert('Aide', cuttingHelpText);
      }},
      { name: 'buffer',               fieldLabel : "Tampon ",                             allowBlank: true,  xtype: "numberfield", id:'lotTampon' },
      { name: 'buffer_help',          text : "Aide",                                      width:50, height:25, xtype: "button", id:"bufferHelp", handler: function(){
        Ext.Msg.alert('Aide', bufferHelpText);
      }},
      { name: 'referentielId',        fieldLabel : "Référentiel de découpage* ",          allowBlank: true,  xtype: "combo.lot.refIntersection", id: 'cuttingRef' },
      { name: 'mapping',              xtype: "hidden",                                    allowBlank: true,  id: "mappingContent"},
      { name: 'mapping_file',         fieldLabel : "Table de correspondance* ",           allowBlank: true,  xtype: "filefield", id:'mappingInsee', listeners: {
        change: {
          fn: function (field) {
            var fileTypes = ['csv'];
            var fileObj = field.fileInputEl.dom.files[0];
            var reader = new FileReader();
            reader.onload = function(event) { 
              var extension = field.fileInputEl.dom.files[0].name.split('.').pop().toLowerCase(), 
              isSuccess = fileTypes.indexOf(extension) > -1;
              if (isSuccess) {
                Ext.get('formLot').component.getComponent('mappingContent').setValue(reader.result); 
              } else {
                Ext.Msg.alert("Erreur", "Le fichier fourni doit être au format CSV");
              }
            };
            reader.readAsText(fileObj); 
          }
        }
      }},
      { name: 'mapping_help',         text : "Aide",                                       width:50, height:25, xtype: "button", id:"mappingHelp", handler: function(){
        Ext.Msg.alert('Aide', mappingHelpText);
      }},
      { name: 'mappingField',        fieldLabel : "Champ de découpage* ",                allowBlank: true,  xtype: "combo.lot.mapping_fields", id: 'mappingField' },
      { name: 'mapping_field_help',   text : "Aide",                                       width:50, height:25, xtype: "button", id:"mappingFieldHelp", handler: function(){
        Ext.Msg.alert('Aide', mappingFieldHelpText);
      }},
      { name: 'displayType',          fieldLabel : "Type d'affichage ",                    allowBlank: false, xtype: "combo.lot.display", id: 'comboDisplay' },
      { name: 'statistiques',         fieldLabel : "Statistiques de surface présentées ",  allowBlank: true,  xtype: "checkboxfield" },
      { name: 'visualiseur',          fieldLabel : "Lien visualiseur présenté ",           allowBlank: true,  xtype: "checkboxfield" },
    ] 
  });
  
  /**  Le store de Lot */
  Ext.define('Bdcom.store.Lot', {
    extend: 'Ext.data.Store',
    alias: 'Bdcom.store.Lot',
    model: 'Bdcom.model.Lot',
    sortOnLoad: true,
    sorters: {
      property : 'alias',
      direction: 'ASC'
    }
  });

  Ext.StoreMgr.lookup('store.Lot') || Ext.create('Bdcom.store.Lot',{storeId : 'store.Lot'});

})();