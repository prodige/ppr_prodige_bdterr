(function(){
  // Couche champs
  var _storeChamps = Ext.StoreMgr.lookup('store.Champ') || Ext.create('Bdcom.store.Champ',{storeId : 'store.Champ'});

  // Contenu Tier
  var _storeContenu = Ext.StoreMgr.lookup('store.Contenu') || Ext.create('Bdcom.store.Contenu',{storeId : 'store.Contenu'});

  // Store des combobox
  var _storeMap = Ext.StoreMgr.lookup('store.ProdigeMap') || Ext.create('Bdcom.store.ProdigeMap',{storeId : 'store.ProdigeMap'});
  var _storeTheme = Ext.StoreMgr.lookup('store.Theme') || Ext.create('Bdcom.store.Theme',{storeId : 'store.Theme'});
  var _storeRefIntersection = Ext.StoreMgr.lookup('store.ReferentielIntersection') || Ext.create('Bdcom.store.ReferentielIntersection',{storeId : 'store.ReferentielIntersection'});

  /** */
  function close(tab, component){
    if(component && tab && tab.ref && component.controller && typeof component.controller.onSwitchView === 'function'){
      component.controller.onSwitchView(tab.toView, tab);
    }
  }

  /**
   * 
   * @param {*} tab 
   * @param {*} record 
   */
  function showMap(tab, record){
    if(record && !record.get('carte')){
            
      var carteRecord = _storeMap.findRecord('wmsmetadata_uuid', record.get('uuid') );
      if(carteRecord){
        record.set('carte', carteRecord.get('file') );//themeParent
      }
    }

    tab.getComponent("formLot").loadRecord(record);
  }
 
  /**
   * 
   * @param {*} tab 
   * @param {*} record 
   */
  function showTheme(tab, record){
    var themeRecord = _storeTheme.findRecord('id', record.get('themeId'), 0, false, false, true );
    if(themeRecord){
      record.set('themeParent', themeRecord.get('parentId') );
    }

    tab.getComponent("formLot").loadRecord(record);
  }

  /**
   * 
   * @param {*} widget 
   */
  function saveAllDatas(widget){
    var promises = [];

    if(Ext.getCmp('formLot') && Ext.getCmp('formLot').getRecord()  ){
      var form = Ext.getCmp('formLot');
     
      promises.push( sendDataToSever(form) );

      _storeContenu.each(function(record){
        if(!record.get('lotId')){
          record.set('lotId', form.getRecord().get('id') );
        }
      });
    }
  
    // Synchronisation de contenu tier et couche champs
    [_storeContenu, _storeChamps].forEach(function(store){
      var d = null;
      var isOk = true;
      var isNotDirty = true;
  
       
      store.each(function(record){
        isNotDirty = isNotDirty &&  (!record.dirty);
        if( (store.getStoreId() == 'store.Contenu') && !isNotDirty){
          d = $.Deferred();
    
          if(!record.get('url')) {
            isOk = false;
            d.reject('Contenu Tier : Contenu du champs est vide')
          }
          if(!record.get('nom')) {
            isOk = false;
            d.reject('Contenu Tier : nom est vide')
          }
        }
      });

      if(isOk && !isNotDirty){
        d =  d === null ?  $.Deferred() : d;
         
        promises.push(d);
        //TODO: gérer les erreurs
        store.sync({
          callback: function(batch){

            if(batch.total && batch.operations && batch.operations.length > 0){
              d.resolve({success: batch.operations[0].success , from: store.name });
             
              
            }
          }
        });
      }
   
      
    });

    var wait = null;
    if(promises.length > 1){
      wait = $.when.apply($, promises);
    
    }
    else if(promises.length === 1){
      wait = promises[0].promise();
    }
   
    if(wait){
      wait.then(function(p1, p2, p3){
        var messages = [];
        [p1, p2, p3].forEach(function(promise){
          if(promise  && promise.from){
            messages.push(promise.from+ " : " +  ( promise.success ? "Mise à jour réussie" : "Erreur pendant la mise à jour") ) ;
          }
        });

        if(Ext.getCmp('tabLot') &&  Ext.getCmp('formLot') && Ext.getCmp('formLot').getRecord()){
          loadStores(Ext.getCmp('tabLot'),  Ext.getCmp('formLot').getRecord() );
        }
        Ext.Msg.alert('Enregistrement', messages.join("<br>")).setIcon(Ext.Msg.INFO);

        close(widget,  Ext.getCmp('mainPanel'));
      }, function(message){
        Ext.Msg.alert('Enregistrement', message ? message : 'Error pendant la synchro').setIcon(Ext.Msg.INFO);
      })
    }
  }

  /**
   * @param {*} form 
   */
  function loadStores(form, record){
    // Grille d'édition des contenu tier
    if( form.getComponent("gridContenu")  && typeof  form.getComponent("gridContenu").loadStoreByLot === 'function' && record && record.get('id') ){
    
      form.getComponent("gridContenu").loadStoreByLot(record.get('id'));//loadStoreByLot
    }

    if( form.getComponent("gridJoints")  && typeof  form.getComponent("gridJoints").loadStores === 'function' && record && record.get('id') ){

      form.getComponent("gridJoints").loadStores(record.get('id'));//loadStoreByLot
    }

    // Grille d'édition des champs 
    if( form.getComponent("gridChamp")  && typeof  form.getComponent("gridChamp").loadStoreByLot === 'function' && record && record.get('id') ){
      form.getComponent("gridChamp").loadStoreByLot(record.get('id'));//loadStoreByLot
    }
  }

  /**
   * Envoies les données du formulaire au serveur
   * @param {*} form 
   */
  function sendDataToSever(form){
    var d = $.Deferred();
    var lotId = form.getRecord().getId();

    if ( form.isValid() ){
      form.updateRecord();
      form.getRecord().save({
        callback: function(record, operation, success){
          d.resolve({success: success, from:"Lot"});
        }
      })
    }
    else{
      d.reject("Le formulaire Lot n'est pas valide");
    }

    return d;
  }
  //
  Ext.define("Bdcom.views.lot.tab",{
    extend: 'Ext.tab.Panel',
    xtype: 'lot.tab',
    requires: ['Bdcom.commun.formGeneric', 'Bdcom.model.Contenu', 'Bdcom.views.lot.contenuGrid', 'Bdcom.views.lot.champGrid', 'Bdcom.views.lot.jointureGrid'],
   
    toView: 'grid',
    ref: 'lot',

    lotRecord: null,

    tbar: [{
      ref: "lot",
      toView: "grid",
      text: 'Annuler',
      listeners: {
        click: 'onWidget'
      }
    },{
      ref: "lot",
      toView: "grid",
      id: "saveButton",
      text: 'Enregistrer',
      /** SAUVEGARDE */
      handler: saveAllDatas
    }],

    //// Item
    items: [{
      title: 'Paramétrage du lot',
      id: 'formLot',
      xtype: 'commun.formGeneric' ,
      items: Util.getItemModel('item.Lot'),
      tbar: null,
      buttons: null
    },{
      title: 'Paramétrage du contenu tiers',
      id: 'gridContenu',
      xtype: 'lot.contenu.grid' ,
    },{
      title: 'Paramétrage des champs',
      id: 'gridChamp',
      xtype: 'lot.champ.grid' ,

    },{
      title: 'Paramétrage des jointures',
      id: 'gridJoints',
      xtype: 'lot.jointure.grid' ,

    }],
    // Boutons
    buttons: [],
    /** CHARGEMENT DES DONNÉES */
    /// FONCTIONS :
    loadRecord: function(record){

      Ext.getCmp('mainPanel').controller.redirectTo('nav-lot-edit');
      // Formulaire pour éditer le lot
      if( this.getComponent("formLot")  && typeof  this.getComponent("formLot").loadRecord === 'function' ){
        var self = this;
        record.set('themeParent', null );
        self.getComponent("formLot").loadRecord(record);
        
        // Chargement de la map
        if(!_storeMap.isLoaded()){
          _storeMap.load(function(){ showMap(self, record); });
        }
        else{
          showMap(self, record);
        }
      
        // Chargement du thème
        if(!_storeTheme.isLoaded()){
          _storeTheme.load(function(){ showTheme(self, record) });
        }
        else{
          showTheme(self, record);
        }
        
        // Chargement des intersections
        if(!_storeRefIntersection.isLoaded () ) {
          _storeRefIntersection.load(function() { self.getComponent("formLot").loadRecord(record); });
        }
      }

      loadStores(this, record);
      this.lotRecord = record;

      Ext.create('Ext.tip.ToolTip', {
        target: 'lotTampon',
        html: 'Valeur exprimée en mètres, peut être négative ou positive.'
      });

      var comboCutting = this.getComponent("formLot").getComponent('comboCutting').getValue();

      if (comboCutting != 'file') {
        this.getComponent("formLot").getComponent('mappingInsee').setVisible(false).allowBlank = true;
        this.getComponent("formLot").getComponent('mappingHelp').setVisible(false);
      }

      if (comboCutting != 'field') {
        this.getComponent("formLot").getComponent('mappingField').setVisible(false).allowBlank = true;
        this.getComponent("formLot").getComponent('mappingFieldHelp').setVisible(false);
      }

      if (comboCutting != 'ref') {
        this.getComponent("formLot").getComponent('lotTampon').setVisible(false);
        this.getComponent("formLot").getComponent('bufferHelp').setVisible(false);
        this.getComponent("formLot").getComponent('cuttingRef').setVisible(false).allowBlank = true;
      }

    },

  })
})();