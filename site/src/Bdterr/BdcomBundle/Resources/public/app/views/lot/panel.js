(function(){
  // Stores chargé avant l'activation du panel
  var _storeRequires = ['ProdigeTable', 'Lot', 'Theme'];
  var _storeColumn = Ext.StoreMgr.lookup('store.ProdigeColumn') || Ext.create('Bdcom.store.ProdigeColumn',{storeId : 'store.ProdigeColumn'});
  var lotId;

  Ext.define('Bdcom.views.lot.panel', {
    requires: ['Bdcom.commun.Util', 'Bdcom.model.Lot','Bdcom.views.lot.tab', 'Bdcom.commun.formGeneric', 'Bdcom.views.lot.grid', 'Bdcom.views.lot.addLot'],
    extend: 'Ext.panel.Panel',
    xtype: 'Bdcom.views.lot.panel',
    
    layout: 'card',

    height: 600,
    

    border : false,

    mainId: 'grid',

    items: [{                   
      id: 'grid',
      xtype: 'lot.grid'
    },{                 
      id: 'addLot',
      xtype: 'addLot.grid'
    },{
      id: 'tabLot',
      xtype: 'lot.tab' 
    },{
      id: 'formJointure',
      xtype: 'commun.formGeneric'  ,
      items: Util.getItemModel('item.Jointure')
    }],
    getStoreListId: function(inEdit){
      if(inEdit !== 'edit'){
        this.setActiveItem('grid');
      }
    
      return _storeRequires;
    },
    loadStoreForView: function(id, record){

      if( this.getComponent(id) && typeof this.getComponent(id).loadStore === 'function' ){
        this.getComponent(id).loadStore();
      }

      if(id === 'tabLot'){
        if (record === null) {
          record = Ext.getCmp('formLot').getRecord();
          var form = Ext.getCmp(id);
          if( form.getComponent("gridJoints")  && typeof  form.getComponent("gridJoints").loadStores === 'function' && record && record.get('id') ){
            form.getComponent("gridJoints").loadStores(record.get('id')); // Reload joints
          }
          if( form.getComponent("gridChamp")  && typeof  form.getComponent("gridChamp").loadStoreByLot === 'function' && record && record.get('id') ){
            form.getComponent("gridChamp").loadStoreByLot(record.get('id'));// Reload fields
          }
        } else {
          lotId = record.get('id');
        }
      }

      if(id === 'formLot' || id === 'formJointure' ){

        var tableName = record.get('prodigeTable');

        // Chargement des noms de colonnes
        if(tableName){
          _storeColumn.load({
            params:{ table: tableName  }
          })
        }

        if(this.getComponent('formJointure') && this.getComponent('formJointure').getComponent('tagApiChamps')){
          tagComponent = this.getComponent('formJointure').getComponent('tagApiChamps');
          tagComponent.value = record.get('prodigeField');
        }

        if (this.getComponent('formJointure')) {
          this.getComponent('formJointure').getForm().setValues({lotId: lotId});
        }

        var componentToOpen  = this.getComponent(id);

        if(componentToOpen){
          componentToOpen.setRefTBar('lot', 'tabLot');
        }
      }
    },
    initComponent : function(){
      this.callParent(arguments);
    
      this.on('render', function(){
        this.setActiveItem('grid');
      })
    },
  });
})();
