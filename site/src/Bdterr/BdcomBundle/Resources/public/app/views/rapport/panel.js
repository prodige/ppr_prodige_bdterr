(function(){
  // Stores chargé avant l'activation du panel
  var _storeRequires = ['Rapport'];

  var _store = Ext.StoreMgr.lookup('store.Rapport') || Ext.create('Bdcom.store.Rapport',{storeId : 'store.Rapport'});
  

  // Prodige profile
  var _storeProfile = Ext.StoreMgr.lookup('store.Profile') || Ext.create('Bdcom.store.Profile',{storeId : 'store.Profile'});

  Ext.define('Bdcom.views.rapport.panel', {
    requires: ['Bdcom.commun.Util', 'Bdcom.model.Rapport', 'Bdcom.commun.formGeneric', 'Bdcom.views.rapport.grid'],
    extend: 'Ext.panel.Panel',
    xtype: 'Bdcom.views.rapport.panel',
    
    layout: 'card',

    height: 600,
    

    border : false,

    mainId: 'gridRapport',

    items: [{                   
      id: 'gridRapport',
      xtype: 'rapport.grid'
    },{
      id: 'formRapport',
      xtype: 'commun.formGeneric' ,
      items: Util.getItemModel('item.Rapport')
    }],
    getStoreListId: function(){
      //console.log("Rapport actived");
    
      return _storeRequires;
    },
    loadStoreForView: function(id, record){
      if( this.getComponent(id) && typeof this.getComponent(id).loadStore === 'function' ){
        this.getComponent(id).loadStore();
      }
      
      if(id === 'formRapport' ){
        var componentToOpen  = this.getComponent(id);

        if(componentToOpen){
          componentToOpen.setRefTBar('rapport', 'gridRapport');

          // selection des valeurs dans la sélection de tag
          if(componentToOpen.getComponent('tagProfil')){
            tagComponent = componentToOpen.getComponent('tagProfil');
            tagComponent.value = record.get('profilIds'); 
          }
        }

        _storeProfile.load();
      }
    },
    initComponent : function(){
      this.callParent(arguments);
    
      this.on('render', function(){
        this.setActiveItem('gridRapport');
      })
    },

   

  });
})();
