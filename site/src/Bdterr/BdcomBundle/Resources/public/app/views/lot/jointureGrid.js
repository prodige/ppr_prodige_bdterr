var JointureGrid = (function () {
    // Store
    var _storeJointure = Ext.StoreMgr.lookup('store.Jointure') || Ext.create('Bdcom.store.Jointure', {storeId: 'store.Jointure'});

    function addContenu() {
        var record = Ext.create('Bdcom.model.Jointure');
        _storeJointure.add(record);
    }

    // grille de recherche
    Ext.define('Bdcom.views.lot.jointureGrid', {
        requires: ['Bdcom.model.Champ'],
        extend: 'Ext.grid.Panel',
        xtype: 'lot.jointure.grid',

        border: true,
        height: 200,

        tbar: [{
                text: 'Ajouter',
                ref: 'lot',
                toView: 'formJointure',
                listeners: {
                    click: 'onWidget',
                },
                getWidgetRecord: function () {
                    return Ext.create('Bdcom.model.Jointure');
                }
            },
            {
                text: 'Aide',
                handler: function () {
                    var message = "Paramétrez une jointure et retrouvez les champs joins dans l'onglet 'Paramétrage des champs'"
                    + "<br>"  
                    + "<br> Paramétrez :"
                    + "<br> - La 'Clé primaire' du lot de données "
                    + "<br> - La table que vous souhaitez joindre "
                    + "<br> - La 'Clé étrangère' de la table jointe "
                    + "<br> Vous pouvez ainsi ajouter au lot de données des attributs depuis la table jointe"
                    Ext.Msg.alert('Aide', message);

                }
            }
        ],

        forceFit: true,
        //viewModel: {},
        // Colonne
        columns: [{
            text: 'Table jointe',
            dataIndex: 'prodige_table',
        }, {
            text: "Clé primaire",
            dataIndex: 'lot_field',
        }, {
            text: "Clé étrangère",
            dataIndex: 'prodige_key',
        }, {
            text: "Champs joints",
            dataIndex: 'prodige_field',
        }, {
            text: 'Modifier',
            // This is our Widget column
            xtype: 'widgetcolumn',
            width: '5%',
            widget: {
                ref: 'lot',
                xtype: 'button',
                text : 'Modifier',
                toView: 'formJointure',
                listeners: {
                    click: 'onWidget'
                }
            }
        },{
            text: 'Supprimer',
            // This is our Widget column
            xtype: 'widgetcolumn',
            width: '5%',
            widget: {
                ref: 'lot',
                xtype: 'button',
                text : 'Supprimer',
                listeners: {
                    click: 'onDeleteButton'
                }
            }
        }],

        // Configuration plugin d'édition
        // selModel: 'cellmodel',

        //selType: 'cellmodel',
        plugins: {
            ptype: 'cellediting',
            clicksToEdit: 1
        },

        ////////////////////////////////////////////
        loadStores: function (idLot) {
            if (idLot) {
                _storeJointure.load({
                    params: {
                        idLot: idLot
                    }
                });
            }
        },

        initComponent: function () {
            this.store = Ext.StoreMgr.lookup('store.Jointure') || Ext.create('Bdcom.store.Jointure', {storeId: 'store.Jointure'});

            this.callParent(arguments);
        }
    })
})();
