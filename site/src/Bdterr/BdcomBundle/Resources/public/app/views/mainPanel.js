(function(){

  // Patch pour corriger un bug dans extjs qui empeche de bouger le cursor dans l'édition de champs de grille
  Ext.define('Ext.patch.EXTJS16166', {
    override: 'Ext.view.View',
    compatibility: '5.1.0',
    handleEvent: function(e) {
      var me = this,isKeyEvent = me.keyEventRe.test(e.type),
      nm = me.getNavigationModel();

      e.view = me;
      
      if (isKeyEvent) {
        e.item = nm.getItem();
        e.record = nm.getRecord();
      }

      // If the key event was fired programatically, it will not have triggered the focus
      // so the NavigationModel will not have this information.
      if (!e.item) {
        e.item = e.getTarget(me.itemSelector);
      }
      if (e.item && !e.record) {
        e.record = me.getRecord(e.item);
      }

      if (me.processUIEvent(e) !== false) {
        me.processSpecialEvent(e);
      }
      
      // We need to prevent default action on navigation keys
      // that can cause View element scroll unless the event is from an input field.
      // We MUST prevent browser's default action on SPACE which is to focus the event's target element.
      // Focusing causes the browser to attempt to scroll the element into view.
      
      if (isKeyEvent && !Ext.fly(e.target).isInputField()) {
        if (e.getKey() === e.SPACE || e.isNavKeyPress(true)) {
          e.preventDefault();
        }
      }
    }
  });

  // Pour géré les code d'erreur 401
  Ext.Ajax.on({
    requestexception: function(request, response){
      if(response.status === 401 && Ext.getCmp('mainPanel').casUrl){
        console.error('not connected');
        window.location = Ext.getCmp('mainPanel').casUrl + "/login?service=" + window.location.href;
      }
    },
    scope: this
  });


  // Chargement des xclass venant du menu
  function loadXClass(panel, callback){
    if(panel && panel.menus && panel.menus instanceof Object ){
      var requires = [];
      
      // Pour chaque menu qui ont une classe à charger
      Object.values(panel.menus).forEach(function(menu){
        if(!panel.getComponent (menu.route) && menu.xclass ){
          requires.push( menu.xclass );
        }
      });

      Ext.require(requires, function(){
        if(typeof callback === 'function'){
          callback(true);
        }
      })

    }
    else  if(typeof callback === 'function'){
      callback(false);
    }
  }
  
  // Ajout des composants xtype du menu
  // Chaque item du panel possède un id correspond au nom de route dans menu.yml
  function addMenusItems(panel){
    loadXClass(panel, function(isOk){
      if(isOk){
        Object.values(panel.menus).forEach(function(menu){
       
          if(menu.active){
            panel.firstActive = menu.route;
          }
          if(!panel.getComponent (menu.route) ){
            var item = {
              id: menu.route,
            }

            if(menu.panelTitle !==  false){
              item.title = menu.title;
            }

            if( menu.xclass){
              item.xclass= menu.xtype ? menu.xtype : menu.xclass;
            }
  
            panel.add( item );
          }
        });
        if(panel.firstActive){
          panel.setActiveItem(panel.firstActive);
        }
       
      }
    });
  }
  /**
   * Panel principal de l'application
   */
  Ext.define('Bdcom.views.mainPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'bdcom-view-mainPanel',
    id: 'mainPanel',
    controller:  'configurationCtrl',

    border : true,
    
    height: 800,
    width: '100%',

    renderTo: 'content',

    layout: 'fit',
    menus: null,

    scrollable: false,

    firstActive: null,

    listeners: {
      'render': function(){
        addMenusItems(this);
      }
    },

    items : []
  });
})();