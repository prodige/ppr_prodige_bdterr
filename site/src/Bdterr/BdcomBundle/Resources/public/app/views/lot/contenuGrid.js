(function () {
    // Store
    var _store = Ext.StoreMgr.lookup('store.Contenu') || Ext.create('Bdcom.store.Contenu', {storeId: 'store.Contenu'});

    // Dico Champ type
    var _storeChampTypeAll = Ext.StoreMgr.lookup('store.ChampType') || Ext.create('Bdcom.store.Dico', {storeId: 'store.Dico.ChampType'});

    var _storeChampType = Ext.create('Ext.data.ChainedStore', {
        source: _storeChampTypeAll,
        filters: [function (item) {
            return item.get('inContenuTier');
        }]
    })

    /**
     *
     */
    function addContenu() {
        var record = Ext.create('Bdcom.model.Contenu');
        var type = _storeChampType.findRecord('name', "text");

        record.set("typeId", type ? type.get('id') : null); // par défaut type text
        _store.add(record);
    }

    function moveContenu(fromId, method, targetId, idLot) {
        Ext.Ajax.request({
            url: Routing.generate("bdcom_api_contenu") + "/" + fromId + "/move",
            jsonData: {
                "method": method,
                "target_id": targetId
            },
            method: "PATCH",
            callback: function (record, operation, success) {
                // TODO: prendre en compte les erreurs
                //   Util.showMessageServer(operation, success);

                _store.load({
                    params: {
                        idLot: idLot
                    }
                });

            }
        })
    }

    // grille de recherche
    Ext.define('Bdcom.views.lot.contenuGrid', {
        requires: ['Bdcom.model.Contenu'],
        extend: 'Ext.grid.Panel',
        xtype: 'lot.contenu.grid',

        border: true,
        height: 200,

        forceFit: true,

        tbar: [{
            text: 'Ajouter un contenu tiers',
            listeners: {
                click: addContenu
            }
        }, {
            text: 'Aide',
            handler: function () {
                var message = "Un contenu tiers dynamique peut être paramétré dans le champ 'Contenu du champ'."
                    + "<br>"
                    + "<br> - Soit pour intégrer une variable dynamique dépendante des données de la table attributaire."
                    + "<br>Il faut alors entourer le nom du champ par deux accolades : {{id_mnhn}}"
                    + "<br>Pour construire une url avec une partie dynamique, vous pouvez paramétrer le champ comme ceci :"
                    + "<br>" + window.location.href + "/{{id_mnhn}}.pdf"
                    + "<br>"
                    + "<br> - Soit pour afficher plusieurs documents par objet géographique"
                    + "<br>Il faut alors préciser la couche géographique (son nom de table) et le champ 'code objet'"
                    + "<br>Par exemple pour les ZNIEFF de type 1 ayant pour code objet 'id_mnhn' :"
                    + "<br>Il faut saisir : n_znieff1_r52/{{id_mnhn}"
                    + "<br>Et avoir comme arborescence dossier : /home/prodige/base_territoriale/n_znieff1_r52/"
                    + "<br>Avec dedans un sous-dossier par identifiant 'id_mnhn'"
                    + "<br>Chaque sous-dossier objet peut contenir un ou plusieurs documents."
                    + "<br>Vous pouvez paramétrer ce contenu tiers en dossier images avec visualiseur ou un dossier autres supports avec listing url."
                    + "<br>L'adresse générée sera de la forme suivante : " + window.location.href + "/data/{contenu du champ}"
                    + "<br><u>Exemple :</u> Contenu champ = data/n_enp_rnn_s_r52/{{id_mnhn}}/pdf => URL " + window.location.href + "/data/data/n_enp_rnn_s_r52/{{id_mnhn}}/pdf"
                Ext.Msg.alert('Aide', message);

            }
        }],

        // Colonne
        columns: [{
            text: 'Nom',
            dataIndex: 'nom',
            editor: 'textfield'
        }, {
            text: 'Afficher dans la page Résultats',
            dataIndex: 'resultats',
            xtype: 'checkcolumn'
        }, {
            text: 'Afficher dans la page Ressources',
            dataIndex: 'fiche',
            xtype: 'checkcolumn'
        }, {
            text: 'Contenu du champ',
            dataIndex: 'url',
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }, {
            editor: {
                xtype: 'combo.dico',
                store: _storeChampType,
                triggerAction: 'all',
                editable: false,
                forceSelection: true
            },
            dataIndex: 'typeId',
            text: "Type de champ ",
            renderer: function (typeId) {
                var record = _storeChampType.findRecord('id', typeId);

                return record && record.get('name') ? record.get('name') : "";
            }
        }, {
            text: 'Action',
            // This is our Widget column
            xtype: 'widgetcolumn',
            width: '5%',
            widget: {
                xtype: 'buttongroup',
                columns: 1,
                items: [{
                    xtype: 'button',
                    text: 'Supprimer',//'<i class="fa fa-times" aria-hidden="true" style="color: red" ></i>',
                    listeners: {
                        click: 'onButtonGroupDelete'
                    }
                }],
            },
        }],

        // Configuration plugin d'édition
        // selModel: 'cellmodel',
        plugins: {
            ptype: 'cellediting',
            clicksToEdit: 1
        },

        // Configuration plugin drag and drop
        viewConfig: {
            plugins: {
                ptype: 'gridviewdragdrop',
            },
            listeners: {
                beforedrop: function (node, data, target, dropPosition, dropHandlers) {
                    var from = data.records[0];
                    var isUp = from.get("ordre") > target.get("ordre");
                    //console.log(from.get("id"), isUp ? "up" : "down", dropPosition, target.get("id"));

                    moveContenu(from.get("id"), dropPosition, target.get("id"), from.get("lotId"));

                    return true;
                },
            }
        },
        ////////////////////////////////////////////
        loadStoreByLot: function (idLot) {
            if (idLot) {
                if (!_storeChampTypeAll.isLoaded()) {
                    _storeChampTypeAll.load({
                        params: {
                            name: 'champ_type'
                        }
                    });
                }

                _store.load({
                    params: {
                        idLot: idLot
                    }
                });


            }
        },

        initComponent: function () {
            this.store = Ext.StoreMgr.lookup('store.Contenu') || Ext.create('Bdcom.store.Contenu', {storeId: 'store.Contenu'});

            this.callParent(arguments);
        }
    })


})();