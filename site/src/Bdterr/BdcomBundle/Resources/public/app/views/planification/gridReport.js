(function(){
  _store =  Ext.StoreMgr.lookup('store.Report') || Ext.create('Bdcom.store.Report',{storeId : 'store.Report'});
  
  function clearAllLog(){
    Ext.Ajax.request({
      url:  Routing.generate('scheduler_api') + "/reports/clearall"  ,
      method: "DELETE",
      callback: function(record, operation, success){
        _store.load();
      }
    });
  }

  // Store relier aux Report 
  // grille de recherche
  Ext.define('Bdcom.views.planification.gridReport', {
    requires: [ 'Bdcom.model.Report'],
    extend: 'Ext.grid.Panel',
    xtype: 'planification.gridReport',
  
    border : true,
    height: 200,
  
    forceFit: true,
    
    tbar: [{
      text: "Retour",
      //tooltip : "Afficher rapport d'exécution",
      ref: 'planification',
      toView: 'gridScheduler',
      listeners: {
        click: 'onWidget'
      }
    },{
      text: "Suprimer tous les rapports",
      //tooltip : "Afficher rapport d'exécution",
      ref: 'planification',
      toView: 'gridScheduler',
      listeners: {
        click: clearAllLog
      }
    }],
  
    columns: [{
      text: 'Nom',
      dataIndex: 'commandName',
    },{
      text: 'Date',
      dataIndex: 'dateTime',
      xtype: 'datecolumn',   format:'d/m/Y H:i' 
    },{
      text: 'Log',
      dataIndex: 'id',
      renderer: function(id) {
        return "<a href=\""+Routing.generate('scheduler_api') + '/logfile/'+ id +"\" target=\"b_lank\" > voir les logs </a>";
      }
    },{
      text: 'Supprimer',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '10%',
      widget: {
        xtype: 'button',
        //text: '<i class="fa fa-times" aria-hidden="true" style="color: red" ></i>',
        text: 'Supprimer',
        listeners: {
          click: 'onDeleteButton'
        }
      }
    }],

    /////////////////////////////// FONCTIONS //////////////////////////////////////////////////////////
    initComponent : function(){
      this.store = _store;
                  
      this.callParent(arguments);
    }
  });
})();