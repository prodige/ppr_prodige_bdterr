(function(){
  // grille de recherche
  var _storeDataSerie = Ext.StoreMgr.lookup('store.DataSerie') || Ext.create('Bdcom.store.DataSerie',{storeId : 'store.DataSerie'});

  var _store = Ext.create( 'Ext.data.ChainedStore',{
    source: _storeDataSerie,
    filters: [
      function(item) {
        if(item && typeof item.inLot === 'function' ){
          return !item.inLot();
        }
        console.error("AddLot => l'item",item,"n'a pas de fonction inLot")
        return false;
      }
    ]
  }) 
  // Lot de donnée
  var _storeLot =  Ext.StoreMgr.lookup('store.Lot') || Ext.create('Bdcom.store.Lot',{storeId : 'store.Lot'});

  // Les dicos
  var _storeRubrique = Ext.StoreMgr.lookup('store.Rubrique') || Ext.create('Bdcom.store.Dico',{storeId : 'store.Dico.Rubrique'});
  var _storeDomaine = Ext.StoreMgr.lookup('store.Domaine')  || Ext.create('Bdcom.store.Dico',{storeId : 'store.Dico.Domaine'});
  var _storeSousDomaine = Ext.StoreMgr.lookup('store.SousDomaine')  || Ext.create('Bdcom.store.Dico',{storeId : 'store.Dico.SousDomaine'});

  /**
   * 
   * @param {*} form 
   * @param {*} component 
   */
  function close(form, component){
    if(component && form && form.ref && component.controller && typeof component.controller.onSwitchView === 'function'){
      component.controller.onSwitchView(form.toView, form, true);
    }

  }

  /**
   * Filtre les DataSerie en fonction du SousDomaine, Domaine ou Rubrique
   * @param {*} widget 
   */
  function filterDataLot(widget){
    var toolbar = widget.up();
    
    if(typeof toolbar.getComponent === 'function'){

      var params = {};
      if(toolbar.getComponent('sousDommaine').value){
        params.subdomain  = toolbar.getComponent('sousDommaine').value;
      }
     
      else if(toolbar.getComponent('dommaine').value){
        params.domain  = toolbar.getComponent('dommaine').value;
      }
      else if(toolbar.getComponent('rubrique').value){
        params.rubrique  = toolbar.getComponent('rubrique').value;
      }

      _storeDataSerie.load({params: params});
    } 
    else{
      console.error("toolbar n'a pas de function getComponent");
    }

  }

  /**
   * TODO: trouver une solution pour reset un combobox
   * @param {*} toolbar 
   * @param {*} id 
   */
  function resetSelect(toolbar, id){
    if(typeof toolbar.getComponent === 'function' && toolbar.getComponent(id) ){
      toolbar.getComponent(id).clearValue();
    }
  }

  /**
   * 
   * @param {*} widget 
   */
  function onSelectFilter(widget){
    var toolBarOk = typeof toolbar.getComponent === 'function';
   

    if(widget.id === 'rubrique' && widget.value){
      _storeDomaine.load({
        params:{
          name: 'domaine',
          filter_id: widget.value
        }
      });

      resetSelect(widget.up(), 'domaine');
    }
    if(widget.id === 'dommaine' && widget.value){
      _storeSousDomaine.load({
        params:{
          name: 'sous_domaine',
          filter_id: widget.value
        }
      })
    }

    resetSelect(widget.up(), 'sousDommaine');
  }

  /**
   * 
   * @param {*} widget 
   */
  function addLot(widget){
    var promises = [];

     
    _store.query('selected', true).each(function(model){
      _storeLot.add(Ext.create('Bdcom.model.Lot',{ 
        'uuid' :      model.get('uuid'),
        'lotTable' :  model.get('table_postgis'),
        'alias':      model.get('nom')
       }) );
    });

    _storeLot.sync({
      callback: function(){
        close(widget, Ext.getCmp('mainPanel'));
      }
    });
  }

  /**
   * Vue pour ajouter un lot de données
   */
  Ext.define('Bdcom.views.lot.addLot', {
    requires: [ 'Bdcom.components.ComboDico'],
    extend: 'Ext.grid.Panel',
    xtype: 'addLot.grid',
    
    // Config
    border : true,
    height: 200,
  
    forceFit: true,

    // Toolbar
    tbar: [{
      id: 'rubrique',
      fieldLabel: 'Rubrique',
      xtype: 'combo.dico',
      store: _storeRubrique,
      listeners: {
        select : onSelectFilter
      },
    },{
      id: 'dommaine',
      fieldLabel: 'Domaine',
      xtype: 'combo.dico',
      store: _storeDomaine,
      listeners: {
        select : onSelectFilter
      }
    },{
      id: 'sousDommaine',
      fieldLabel: 'Sous-domaine',
      xtype: 'combo.dico',
      store: _storeSousDomaine
    },{
      text: 'Filtrer',
      listeners: {
        click: filterDataLot
      },
    }],
    //TODO: voir pour customisé les valeurs déjà ajouté et/ou non configuré
    

    // Colonne
    columns: [{
      text: 'Métadonnée',
      dataIndex: 'title',
      
    },{
      text: 'Nom de la couche',
      dataIndex: 'nom',
    },{
      text: 'Description',
      dataIndex: 'description',
    },{
      text: 'Table',
      dataIndex: 'table_postgis',
    },{
      xtype: 'checkcolumn',
      dataIndex: 'selected',
      renderer: function(value, widget, record){
     
        if(typeof record.inLot && !record.inLot()){
          return  new Ext.grid.column.Check().renderer(value);
        }

        return "<input type='checkbox' disabled='disabled'>";            
       
      }
    }],

    // Bouton 
    buttons: [{
      text: 'Ajouter',
      handler: addLot,
      xtype: 'button',
      ref: 'lot',
      toView: 'grid'
    },{
      text: 'Annuler',
      // This is our Widget column
     
      xtype: 'button',
      ref: 'lot',
      toView: 'grid',
      //text: '<i class="fa fa-times" aria-hidden="true"></i>',
      listeners: {
        click: 'onWidget'
      }
    }],
    
    // Fonctions
    
    loadStore: function(){
      Ext.getCmp('mainPanel').controller.redirectTo('nav-lot-edit');

     // [ 'domaine', 'sous_domaine', 'rubrique' ]
      _storeDataSerie.load(function(){
        _store.filter();
      });

      _storeRubrique.load({
        params:{
          name: 'rubrique'
        }
      });

      _storeDomaine.load({
        params:{
          name: 'domaine'
        }
      });

      _storeSousDomaine.load({
        params:{
          name: 'sous_domaine'
        }
      });
    },
    initComponent : function(){   
      this.store = _store;          
      
      this.callParent(arguments);
    }
  });
  })();