/**
 * Panel utilisé pour la gestion de la configuration
 * 
 * la fonction getStoreListId est appelé automaqituement par mainCtrl.js pour charger correctement  les stores au moment de l'activation du panel
 */
(function(){
  // Stores chargé avant l'activation du panel
  var storeRequires = ['ProdigeTable', 'ReferentielRecherche', 'ReferentielIntersection', 'ReferentielCarto'];
  
  var _storeColumn = Ext.StoreMgr.lookup('store.ProdigeColumn') || Ext.create('Bdcom.store.ProdigeColumn',{storeId : 'store.ProdigeColumn'});
  
  /** Grille pour l'afffichage des référentiel **/
  Ext.define('Bdcom.views.configuration.grid', {
    requires: ['Bdcom.views.configuration.gridRefRecherche', 'Bdcom.views.configuration.gridRefIntersection', 'Bdcom.views.configuration.gridRefCarto'],
    extend: 'Ext.panel.Panel',
    xtype: 'configuration.grid',
    height: 900,
    layout: {
      type: 'vbox',       
      align: 'stretch',    
    },

    border : false,

    items: [{                   
      title: 'Référentiel de recherche',
      xtype: 'configuration.gridRefRecherche'
    },{
      xtype: 'splitter'  
    },{     
      title: 'Référentiels cartographiques',        
      xtype: 'configuration.gridRefCarto',
      flex: 1                                       
    },{
      xtype: 'splitter'  
    },{     
      title: 'Référentiels de découpage',        
      xtype: 'configuration.gridRefIntersection',
      flex: 1                                       
    }],

    initComponent : function(){
      this.callParent(arguments);
    }
  });

  /********************************/
  /**   Liste des formulaires    **/
  /********************************/
  Ext.define('Bdcom.views.configuration.panel', {
    requires: ['Bdcom.commun.Util', 'Bdcom.model.ReferentielRecherche', 'Bdcom.commun.formGeneric'],
    extend: 'Ext.panel.Panel',
    xtype: 'Bdcom.views.configuration.panel',
    
    layout: 'card',

    height: 900,
    

    border : false,

    mainId: 'configurationGrid',

    items: [{                   
      id: 'configurationGrid',
      xtype: 'configuration.grid'
    },{
      id: 'fromRefRecherche',
      xtype: 'commun.formGeneric' ,
      items: Util.getItemModel('item.ReferentielRecherche')
    },{
      id: 'formRefCarto',
      xtype: 'commun.formGeneric'  ,
      items: Util.getItemModel('item.ReferentielCarto')
    },{
      id: 'formRefIntersection',
      xtype: 'commun.formGeneric'  ,
      items: Util.getItemModel('item.ReferentielIntersection')
    }],
    getStoreListId: function(){
    //  console.log("Configuration actived");
    
      return storeRequires;
    }, 
    loadStoreForView: function(id, record){
      if( (id === 'fromRefRecherche' || id === 'formRefIntersection' || id === 'formRefCarto') && record ){
        var componentToOpen  = this.getComponent(id);

        if(componentToOpen){
          componentToOpen.setRefTBar('configuration', 'configurationGrid');
        }

        var tableName = record.get('referentielTable');

        // selection des valeurs dans la sélection de tag
        if(this.getComponent('fromRefRecherche') && this.getComponent('fromRefRecherche').getComponent('tagApiChamps')){
          tagComponent = this.getComponent('fromRefRecherche').getComponent('tagApiChamps');
          tagComponent.value = record.get('apiChamps'); 
        }
        
        // selection des valeurs dans la sélection de tag
        if(this.getComponent('formRefCarto') && this.getComponent('formRefCarto').getComponent('tagApiChamps')){
          tagComponent = this.getComponent('formRefCarto').getComponent('tagApiChamps');
          tagComponent.value = record.get('apiChamps'); 
        }

        // Chargement des noms de colonnes 
        if(tableName){
          _storeColumn.load({
            params:{ table: tableName  }
          })
        }
      }
      
     
    },
    initComponent : function(){
      this.callParent(arguments);

      this.on('render', function(){
       
        this.setActiveItem('configurationGrid');
      })
    },
  });
})();