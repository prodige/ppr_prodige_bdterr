(function(){
  Ext.define('Bdcom.views.configuration.gridRefCarto', {
    requires: ['Bdcom.controller.configurationCtrl', 'Bdcom.model.ReferentielCarto'],
    extend: 'Ext.grid.Panel',
    xtype: 'configuration.gridRefCarto',

    border : true,

    height: 500,

    forceFit: true,
    tbar: [{
      text: 'Ajouter',
      ref: 'configuration',
      toView: 'formRefCarto',
      listeners: {
        click: 'onWidget'
      },
      getWidgetRecord: function(){
        return Ext.create('Bdcom.model.ReferentielCarto');
      }
    }],
    columns: [{
      text: 'Table',
      dataIndex: 'referentielTable',
      width: '35%',
    },{
      text: 'Modifier',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '5%',
      widget: {
        ref: 'configuration',
        xtype: 'button',
        text : 'Modifier',
        toView: 'formRefCarto',
        listeners: {
          click: 'onWidget'
        }
      }
    },{
      text: 'Supprimer',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '5%',
      widget: {
        ref: 'configuration',
        xtype: 'button',
        text : 'Supprimer',
        listeners: {
          click: 'onDeleteButton'
        }
      }
    }],
   
    initComponent : function(){
      this.store = Ext.StoreMgr.lookup('store.ReferentielCarto') || 
                   Ext.create('Bdcom.store.ReferentielCarto',{storeId : 'store.ReferentielCarto'});

      this.callParent(arguments);
    }
  });

})();