(function(){
  var _storeRequires = ['Scheduler'];
  //
  Ext.define("Bdcom.views.planification.tab",{
    extend: 'Ext.tab.Panel',
    xtype: 'planification.tab',
    requires: ['Bdcom.views.planification.panel' ],
   
    toView: 'grid',
    ref: 'planification',

    lotRecord: null,

    //// Item
    items: [{
      title: 'Planification',
      id: 'gridScheduler',
      xtype: 'planification.grid'
    },{
      id: 'formScheduler',
      xtype: 'commun.formGeneric' ,
      items: Util.getItemModel('item.Scheduler')
    }],
    // Boutons
    buttons: [],
    
    /** CHARGEMENT DES DONNÉES */
    /// FONCTIONS :

    getStoreListId: function(){
      // console.log("Planification actived");
    
      return _storeRequires;
    },
    loadStoreForView: function(id, record){
      if( this.getComponent(id) && typeof this.getComponent(id).loadStore === 'function' ){
        this.getComponent(id).loadStore();
      } 

      if(id === 'formScheduler' ){
        var componentToOpen  = this.getComponent(id);

        if(componentToOpen){
          componentToOpen.setRefTBar('planification', 'gridScheduler');
        }

        // ponctuelle ou périodique
        var time = new Date();
        time.setHours(record.get('hour'), record.get('minute'));
      
        record.set("timeField", time );
        if( !record.get("isPeriodic") ){
          time.setDate(record.get('dayMonth'));
          time.setMonth(record.get('month')-1);

          time.setFullYear(record.get('year') ? record.get('year') : time.getFullYear() );

          record.set("dateField",time);
        }

        componentToOpen.loadRecord(record);
        
      }

    },
  })
})();