(function(){
  // Stores chargé avant l'activation du panel
  var _storeRequires = ['Theme', 'Lot'];
  
  var _store = Ext.StoreMgr.lookup('treeStore.Theme') || Ext.create('Bdcom.treeStore.Theme',{storeId : 'treeStore.Theme'});
  var _storeWithAllTheme = Ext.StoreMgr.lookup('store.Theme') || Ext.create('Bdcom.store.Theme',{storeId : 'store.Theme'});

  Ext.define('Bdcom.views.themes.panel', {
   requires: ['Bdcom.commun.Util', 'Bdcom.model.Theme', 'Bdcom.commun.formGeneric', 'Bdcom.views.themes.treeTheme'],
    extend: 'Ext.panel.Panel',
    xtype: 'Bdcom.views.themes.panel',
    
    layout: 'card',

    height: 600,
    

    border : false,

    mainId: 'themeTree',

    items: [{                   
      id: 'themeTree',
      xtype: 'theme.tree'
    },{
      id: 'formTheme',
      xtype: 'commun.formGeneric' ,
      items: Util.getItemModel('item.Theme')
    }],
    getStoreListId: function(){
      //console.log("Theme actived");
    
      return _storeRequires;
    }, 
    loadStoreForView: function(id, record){
      if(id === 'formTheme'  && this.getComponent(id) ){
        this.getComponent(id).setRefTBar('thematiques', 'themeTree');
      }

      if(id === 'themeTree'  && this.getComponent(id) ){
        _storeWithAllTheme.load();
      }
    },
    initComponent : function(){
      this.callParent(arguments);
     
      this.on('render', function(){
        this.setActiveItem('themeTree');
      })
    },

   

  });
})();
