(function(){
  Ext.define('Bdcom.views.configuration.gridRefIntersection', {
    requires: ['Bdcom.controller.configurationCtrl', 'Bdcom.model.ReferentielIntersection'],
    extend: 'Ext.grid.Panel',
    xtype: 'configuration.gridRefIntersection',

    border : true,

    height: 500,

    forceFit: true,
    tbar: [{
      text: 'Ajouter',
      ref: 'configuration',
      toView: 'formRefIntersection',
      listeners: {
        click: 'onWidget'
      },
      getWidgetRecord: function(){
        return Ext.create('Bdcom.model.ReferentielIntersection');
      }
    }],
    columns: [{
      text: 'Nom',
      dataIndex: 'nom',
    },{
      text: 'Table',
      dataIndex: 'referentielTable',
    },{
      text: 'Champ code INSEE',
      dataIndex: 'insee',
    },{
      text: 'Champ code INSEE communes déléguées',
      dataIndex: 'inseeDeleguee',
    },{
      text: 'Modifier',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '5%',
      widget: {
        ref: 'configuration',
        xtype: 'button',
        text : 'Modifier',
        toView: 'formRefIntersection',
        listeners: {
          click: 'onWidget'
        }
      }
    },{
      text: 'Supprimer',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '5%',
      widget: {
        xtype: 'button',
        ref: 'configuration',
        text : 'Supprimer',
        listeners: {
          click: 'onDeleteButton'
        }
      }
    }],
   
    initComponent : function(){
      this.store = Ext.StoreMgr.lookup('store.ReferentielIntersection') || 
                  Ext.create('Bdcom.store.ReferentielIntersection',{storeId : 'store.ReferentielIntersection'});

      this.callParent(arguments);
    }
  });

})();