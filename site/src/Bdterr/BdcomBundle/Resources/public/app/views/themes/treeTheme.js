(function(){
  // Stores chargé avant l'activation du panel
  var _storeRequires = ['Theme'];
  
  var _store = Ext.StoreMgr.lookup('treeStore.Theme') || Ext.create('Bdcom.treeStore.Theme',{storeId : 'treeStore.Theme'});
  var _storeWithAllTheme = Ext.StoreMgr.lookup('store.Theme') || Ext.create('Bdcom.store.Theme',{storeId : 'store.Theme'});
  var _storeLot = Ext.StoreMgr.lookup('store.Lot') || Ext.create('Bdcom.store.Lot',{storeId : 'store.Lot'});

  function addTheme(name, parent, recordParent){
    if(name){
      var model = Ext.create('Bdcom.treeModel.Theme');
      model.set('nom', name);
      model.set('alias', name);
      model.set('parent', parent ? parent : 0); // la racine possède l'id 0

      model.save({
        callback: function(record, operation, success){
          Util.showMessageServer(operation, success);
          _store.load();
        }
      })
    }
    
  }

  function moveTheme(fromId, method, targetId){
    Ext.Ajax.request({
      url:  Routing.generate("bdcom_api_theme") +"/"+  fromId +"/move" ,
      jsonData:{
        "method": method,
        "target_id" : targetId
      },
      method: "PATCH",
      callback: function(record, operation, success){
        // TODO: prendre en compte les erreurs
     //   Util.showMessageServer(operation, success);
        if(!success){
          _store.load();
        }
      }
    })
  }
  
  function generateThemes(){
    Ext.Msg.show({
      title:'Génération des thèmes covalis ?',
      message: 'êtes-vous sûr de vouloir générer les thèmes Covalis ?',
      buttons: Ext.Msg.YESNO,
      icon: Ext.Msg.QUESTION,
      fn: function(btn) {
        if (btn === 'yes') {
          Ext.Ajax.request({
            url:  Routing.generate("bdcom_api_theme") +"/generate" ,
            method: "POST",
            callback: function(record, operation, success){
              // TODO: prendre en compte les erreurs
            //   Util.showMessageServer(operation, success);
              _store.load();
            }
          })
        }
      }
   });
   
  }

  function onDeleteButton(widget){
    var record = widget.getWidgetRecord();
    if(record && _storeWithAllTheme.getData().find('parentId', record.get('id')) === null  && _storeLot.getData().find('themeId', record.get('id')) === null  ){
      if( record ) {
        record.erase({
          callback: function(record, operation, success){
            _store.load();
            _storeWithAllTheme.load();
            Util.showMessageServer(operation, success);
          }
        })
      }
    }
  }

  // Arbres
  Ext.define('Bdcom.views.themes.treeTheme', {
    requires: ['Bdcom.components.CustomTreeColumn','Bdcom.model.Theme', 'Bdcom.commun.Util'],
    extend: 'Ext.tree.Panel',
    xtype: 'theme.tree',
    
    tbar:[{
      id: 'themeName',
      xtype: 'textfield'
    },{
      text: 'Ajouter un thème',
      handler: function(){
        addTheme( this.up().getComponent('themeName').getValue() );
      }
    },{
      text: 'Ajouter un sous-thème',
      handler: function(){
        if(Ext.getCmp('themeTree') && typeof Ext.getCmp('themeTree').getSelection === 'function' ){
          var selection =  Ext.getCmp('themeTree').getSelection();
          if(selection && selection instanceof Array && selection.length > 0 && selection[0].get('id') && selection[0].get('isTheme') ){
            var themeNameCpnt =  this.up().getComponent('themeName');
            Ext.getCmp('themeTree').expandNode(selection[0], false, function(){
              addTheme( themeNameCpnt.getValue(), selection[0].get('id'),selection[0]  );
              themeNameCpnt.setValue("");
            });
           
          } 
        }
        else {
          console.error("Ext.getCmp('themeTree') est null ou n'a pas de fonction getSelection : ",Ext.getCmp('themeTree') )
        }
      }
    },{
      text: 'Générer les thématiques à partir des thèmes COVADIS',
      handler: function(){
        generateThemes();
      }
    },{
      text: 'Aide',
      handler: function(){
        Ext.Msg.alert('Aide', "Vous pouvez déplacer les thèmes et sous-thèmes par glisser/déposer.");
      }
    }],

    // Colonnes
    columns:[{
      xtype: 'Bdcom.CustomTreeColumn',
      dataIndex: 'nom',
      name: 'nom',
      width: "30%",
      text: 'Nom'
    },{
      dataIndex: 'leaf',
      width: "20%",
      text: 'Type', 
      renderer: function(value) {
        return value ? 'Sous-thème' : 'Thème';
      }
    },{
      text: "Édition",
      xtype: 'widgetcolumn',
      width: '10%',
      widget: {
        ref: 'thematiques',
        xtype: 'button',
        toView: 'formTheme',

        text: 'Modifier',//'<i class="fa fa-pencil" aria-hidden="true"></i>',
        listeners: {
          click: 'onWidget'
        }
      },
    },{
      text: "Supression",
      xtype: 'widgetcolumn',
      width: '10%',
      id: 'widgetDelete',
      widget: {
        ref: 'thematiques',
        xtype: 'button',
        //text: '<i class="fa fa-times" aria-hidden="true" style="color: red" ></i>',
		    text: 'Supprimer',
        listeners: {
          click: onDeleteButton,
        },
      },
    }],
    // Ajout plugin
    viewConfig: {
      plugins: { 
        ptype: 'treeviewdragdrop',
       
      },
      listeners:{
        beforedrop : function( node, data, target, dropPosition, dropHandlers ) {
          var from  = data.records[0];
          var isUp = from.get("ordre") >  target.get("ordre");
          // console.log(from.get("id"), isUp ? "up" : "down", dropPosition, target.get("id"));

          // Un thème ne peut être placer dans un autre thème
          if(  from.get("isTheme") && target.get("isTheme") && dropPosition === 'append'){
            return false;
          }

          // Un thème ne peut être placer à côté d'un sous-thème
          if(  from.get("isTheme") && !target.get("isTheme") ){
            return false;
          }

          // Un sous-thème ne peut être placer à côté d'un thème
          if(  !from.get("isTheme") && target.get("isTheme") && dropPosition !== 'append' ){
            return false;
          }

         
          moveTheme(from.get("id"), dropPosition, target.get("id"));
          
          
          return true;
        },
      }
      
    },

    // Toolbar
    /*buttons: [{
      id: 'themeName',
      xtype: 'textfield'
    },{
      text: 'Ajouter un thème',
      handler: function(){
        addTheme( this.up().getComponent('themeName').getValue() );
      }
    },{
      text: 'Ajouter un sous-thème',
      handler: function(){
        if(Ext.getCmp('themeTree') && typeof Ext.getCmp('themeTree').getSelection === 'function' ){
          var selection =  Ext.getCmp('themeTree').getSelection();
          if(selection && selection instanceof Array && selection.length > 0 && selection[0].get('id') && selection[0].get('isTheme') ){
            var themeNameCpnt =  this.up().getComponent('themeName');
            Ext.getCmp('themeTree').expandNode(selection[0], false, function(){
              addTheme( themeNameCpnt.getValue(), selection[0].get('id'),selection[0]  );
              themeNameCpnt.setValue("");
            });
           
          } 
        }
        else {
          console.error("Ext.getCmp('themeTree') est null ou n'a pas de fonction getSelection : ",Ext.getCmp('themeTree') )
        }
      }
    }],*/
 
    buttonAlign:'left',
    rootVisible: false,
 
    initComponent: function() {
      this.store = _store;
     
      this.callParent(arguments);
    }
  })

})();
