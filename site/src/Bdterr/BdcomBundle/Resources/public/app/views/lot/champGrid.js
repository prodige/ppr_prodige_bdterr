
var champGrid = (function (){
  // Store
  var _store = Ext.StoreMgr.lookup('store.Champ') || Ext.create('Bdcom.store.Champ',{storeId : 'store.Champ'});

  // Dico Champ type
  var _storeChampType = Ext.StoreMgr.lookup('store.ChampType')  || Ext.create('Bdcom.store.Dico',{storeId : 'store.Dico.ChampType'});

  /**
   * 
   * @param {*} fromId 
   * @param {*} method 
   * @param {*} targetId 
   * @param {*} idLot 
   */
  function moveContenu(fromId, method, targetId, idLot){
    Ext.Ajax.request({
      url:  Routing.generate("bdcom_api_champs") +"/"+  fromId +"/move" ,
      jsonData:{
        "method": method,
        "target_id" : targetId
      },
      method: "PATCH",
      callback: function(record, operation, success){
        // TODO: prendre en compte les erreurs
     //   Util.showMessageServer(operation, success);
  
          _store.load({params:{
            idLot: idLot
          }});
        
      }
    })
  }

  /**
   * 
   * @param {*} value 
   * @param {*} field 
   */
  function onRadioClick(value, field){
    var record = _store.findRecord('nom', value);

    if(record){
      _store.each(function(item){
        item.set(field, false);
      });

      record.set(field, true);
    }
  }

  // grille de recherche
  Ext.define('Bdcom.views.lot.champGrid', {
    requires: [ 'Bdcom.model.Champ'],
    extend: 'Ext.grid.Panel',
    xtype: 'lot.champ.grid',
  
    border : true,
    height: 200,

    tbar: [{
      text: 'Aide',
      handler: function(){
        var message = "Pour les champs de type date, il est possible de personnaliser le format d'affichage selon le style php<br>"
                      +'<a href="http://php.net/manual/fr/function.date.php" target="_blank" >http://php.net/manual/fr/function.date.php</a>';          
        Ext.Msg.alert('Aide', message);

      }
    }], 

    forceFit: true,
    //viewModel: {},
    // Colonne
    columns: [{
      text: 'Nom',
      dataIndex: 'nom',
    },{
      text: "Champ identifiant de l'objet ?",
      dataIndex: 'id',
      renderer: function(id){
        var record = _store.findRecord('id', id );
          
        return record ? '<input type="radio" name="rd_identifiant" ' + (record.get('identifiant') ? 'checked' :  "") 
                  + '  value="true" onclick="champGrid.onRadioClick(\''+record.get('nom')+'\',\'identifiant\')">' : "";
      }
    },{
      text: "Champ nom de l'objet ?",
      dataIndex: 'id',
      renderer: function(id){
        var record = _store.findRecord('id', id );
    
        return record ?  '<input type="radio" name="rd_label" ' + (record.get('label') ? 'checked' :  "") 
                  + '  value="true" onclick="champGrid.onRadioClick(\''+record.get('nom')+'\',\'label\')">' : "";
      }
    },{
      text: "Afficher dans les résultats",
      dataIndex: 'resultats',
      xtype: 'checkcolumn'
    },{
      text: "Afficher dans la fiche descriptive",
      dataIndex: 'fiche',
      xtype: 'checkcolumn'
    },{
      editor:{
        xtype: 'combo.dico',
        store: _storeChampType,
        triggerAction: 'all',
        editable: false,
        forceSelection: true
      },
      dataIndex: 'typeId',
      text: "Type de champ ",
      renderer: function(typeId){
        var record = _storeChampType.findRecord('id', typeId );
    
        return record && record.get('name') ?  record.get('name') : "" ;
      }
    },{
      text: 'Alias',
      dataIndex: 'alias',
      editor: 'textfield'
    },{
      text: "Formatage du champ ",
      dataIndex: 'format',
      editor: {
        xtype: 'textfield',
        allowBlank: false
      }
    }],

    // Configuration plugin d'édition
   // selModel: 'cellmodel',

    //selType: 'cellmodel',
    plugins: {
      ptype: 'cellediting',
      clicksToEdit: 1
    },
    // Configuration plugin drag and drop
    viewConfig: {
      plugins: {
        ptype: 'gridviewdragdrop',
      },
      listeners:{
        beforedrop : function( node, data, target, dropPosition, dropHandlers ) {
       
          var from  = data.records[0];
         // var isUp = from.get("ordre") >  target.get("ordre");
          //console.log(from.get("id"), isUp ? "up" : "down", dropPosition, target.get("id"));
         
          moveContenu(from.get("id"), dropPosition, target.get("id"), from.get("lotId"));

          return true;
        },
      }
    },

    ////////////////////////////////////////////
    loadStoreByLot: function(idLot){
      if(idLot){
        if(!_storeChampType.isLoaded()){
          _storeChampType.load({
            params:{
              name: 'champ_type'
            }
          });
        }
        
       _store.load({
          params:{
            idLot: idLot
          }
        });
      }
    },

    initComponent : function(){
        this.store = Ext.StoreMgr.lookup('store.Champ') || Ext.create('Bdcom.store.Champ',{storeId : 'store.Champ'});
         
        this.callParent(arguments);
      }
  })

 return {
  onRadioClick: onRadioClick
 }
})();
