(function(){


  // grille de recherche
  Ext.define('Bdcom.views.configuration.gridRefRecherche', {
    requires: ['Bdcom.controller.configurationCtrl', 'Bdcom.model.ReferentielRecherche'],
    extend: 'Ext.grid.Panel',
    xtype: 'configuration.gridRefRecherche',

    border : true,
    height: 200,
    forceFit: true,

    columns: [{
      text: 'Table',
      dataIndex: 'referentielTable',
      width: '10%',
    },{
      text: 'Champ code INSEE',
      dataIndex: 'insee',
      width: '10%',
    },{
      text: 'Champ code INSEE communes déléguées',
      dataIndex: 'inseeDeleguee',
      width: '10%',
    },{
      text: 'Champ Commune',
      dataIndex: 'commune',
      width: '10%',
    },{
      text: 'Champ Nom',
      dataIndex: 'nom',
      width: '10%',
    },{
      text: 'Modifier',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '10%',
      widget: {
        xtype: 'button',
        ref: 'configuration',
        toView: 'fromRefRecherche',
        text : 'Modifier',
        listeners: {
          click: 'onWidget'
        }
      }
    }],
   
    initComponent : function(){
      this.store = Ext.StoreMgr.lookup('store.ReferentielRecherche') || 
                   Ext.create('Bdcom.store.ReferentielRecherche',{storeId : 'store.ReferentielRecherche'});
                  
      this.callParent(arguments);
    }
  });

})();