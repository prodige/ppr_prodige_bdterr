(function(){
  // Stores chargé avant l'activation du panel
  var _storeRequires = ['Scheduler', 'Report'];

  Ext.define('Bdcom.views.planification.panel', {
    requires: ['Bdcom.commun.Util', 'Bdcom.commun.formGeneric', 'Bdcom.views.planification.grid', 'Bdcom.views.planification.gridReport'],
    extend: 'Ext.panel.Panel',
    xtype: 'Bdcom.views.planification.panel',
    
    layout: 'card',

    height: 600,
    

    border : false,

    mainId: 'gridScheduler',

    items: [{                   
      id: 'gridScheduler',
      xtype: 'planification.grid'
    },{
      id: 'formScheduler',
      xtype: 'commun.formGeneric' ,
      items: Util.getItemModel('item.Scheduler')
    },{                 
      id: 'repports',
      xtype: 'planification.gridReport'
    }],
    getStoreListId: function(){
      // console.log("Planification actived");
    
      return _storeRequires;
    },
    loadStoreForView: function(id, record){
      if( this.getComponent(id) && typeof this.getComponent(id).loadStore === 'function' ){
        this.getComponent(id).loadStore();
      }
      
      if(id === 'formScheduler' ){
        var componentToOpen  = this.getComponent(id);

        if(componentToOpen){
          componentToOpen.setRefTBar('planification', 'gridScheduler');
        }

        // ponctuelle ou périodique
        var time = new Date();
        time.setHours(record.get('hour'), record.get('minute'));
      
        record.set("timeField", time );
        if( !record.get("isPeriodic") ){
          time.setDate(record.get('dayMonth'));
          time.setMonth(record.get('month')-1);

          time.setFullYear(record.get('year') ? record.get('year') : time.getFullYear() );

          record.set("dateField",time);
        }

        componentToOpen.loadRecord(record);
        
      }

    
    },
    initComponent : function(){
      this.callParent(arguments);
    
      this.on('render', function(){
        this.setActiveItem('gridScheduler');
      })
    },

   

  });
})();
