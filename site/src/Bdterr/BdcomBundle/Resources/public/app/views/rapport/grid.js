(function(){
  // Store relier aux rapports de données
  // grille de recherche
  Ext.define('Bdcom.views.rapport.grid', {
    requires: [ 'Bdcom.model.Rapport'],
    extend: 'Ext.grid.Panel',
    xtype: 'rapport.grid',
  
    border : true,
    height: 200,
  
    forceFit: true,
  
    tbar: [{
      text: 'Ajouter un rapport',
      ref: 'rapport',
      toView: 'formRapport',
      listeners: {
        click: 'onWidget'
      },
      getWidgetRecord: function(){
        return Ext.create('Bdcom.model.Rapport');
      }
    }],
  
    viewConfig: {
      getRowClass: function(record, index, rowParams, store) {
        if(typeof  record.isConfigured === 'function' && record.isConfigured()){
          return  'grey-background';
        }
  
        return '';
      }
    },
    columns: [{
      text: 'Nom',
      dataIndex: 'nom',
      width: '25%',
    },{
      text: 'Modifier',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '10%',
      widget: {
        xtype: 'button',
        text: 'Modifier',
        //text: '<i class="fa fa-pencil" aria-hidden="true" ></i>',
        toView: 'formRapport',
        ref: 'rapport',
        listeners: {
          click: 'onWidget'
        }
      }
    },{
      text: 'Supprimer',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '10%',
      widget: {
        xtype: 'button',
        //text: '<i class="fa fa-times" aria-hidden="true" style="color: red" ></i>',
        text: 'Supprimer',
        listeners: {
          click: 'onDeleteButton'
        }
      }
    },{
      width: '25%',
    }],
    /////////////////////////////// FONCTIONS //////////////////////////////////////////////////////////
    initComponent : function(){
      this.store =  Ext.StoreMgr.lookup('store.Rapport') || Ext.create('Bdcom.store.Rapport',{storeId : 'store.Rapport'});
                  
      this.callParent(arguments);
    }
  });
  })();