(function(){

  function executeCommand(widget){
  
    if(widget && typeof widget.getWidgetRecord === 'function'){

      record =  widget.getWidgetRecord();
    }

    Ext.Ajax.request({
      url:  Routing.generate("scheduler_api") +"/execute/"+  record.get('id')  ,
      method: "GET",
      success: function(response, opts){
        var info = Ext.decode(response.responseText); 
        Ext.Msg.alert('Aide', info.message);
      },
      faillure: function(response){
        console.log(response);
        var info = Ext.decode(response.responseText); 
        Ext.Msg.alert('Aide', info.message);
        
      }
    });
  }

  // Store relier aux schedulers de données
  // grille de recherche
  Ext.define('Bdcom.views.planification.grid', {
    requires: [ 'Bdcom.model.Scheduler'],
    extend: 'Ext.grid.Panel',
    xtype: 'planification.grid',
  
    border : true,
    height: 200,
  
    forceFit: true,
    
    tbar: [{
      text: 'Ajouter une nouvelle tâche ',
	    title : 'Ajouter et configurer une tâche planifiée de découpage des lots',
      ref: 'planification',
      toView: 'formScheduler',
      listeners: {
        click: 'onWidget'
      },
      getWidgetRecord: function(){
        var record = Ext.create('Bdcom.model.Scheduler');
        record.set("date", new Date());
        record.set('dateField', new Date());
        record.set('isPeriodic', false);

        return record;
      },
    },{
      text: "Afficher rapport d'exécution",
      tooltip : "Afficher rapport d'exécution",
      ref: 'planification',
      toView: 'repports',
      listeners: {
        click: 'onWidget'
      }
    }],
  
    viewConfig: {
      getRowClass: function(record, index, rowParams, store) {
        if(typeof  record.isConfigured === 'function' && record.isConfigured()){
          return  'grey-background';
        }
  
        return '';
      }
    },
    columns: [{
      text: 'Nom',
      dataIndex: 'name',
      width: '25%',
    },{
      text: 'Modifier',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '5%',
      widget: {
        xtype: 'button',
        text : 'Modifier',
        //text: '<i class="fa fa-pencil" aria-hidden="true" ></i>',
        toView: 'formScheduler',
        ref: 'planification',
        listeners: {
          click: 'onWidget'
        }
      }
    },{
      text: 'Supprimer',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '5%',
      widget: {
        xtype: 'button',
        //text: '<i class="fa fa-times" aria-hidden="true" style="color: red" ></i>',
        text : 'Supprimer',
        listeners: {
          click: 'onDeleteButton'
        }
      }
    },{
      text: '',
      // This is our Widget column
      xtype: 'widgetcolumn',
      width: '10%',
      widget: {
        xtype: 'button',
        text : 'Mise à jour immédiate',
        tooltip: 'Lancement immédiat de la tâche',
        listeners: {
          click: executeCommand
        }
      }
    },{
      width: '25%',
    }],
    /////////////////////////////// FONCTIONS //////////////////////////////////////////////////////////
    initComponent : function(){
      this.store =  Ext.StoreMgr.lookup('store.Scheduler') || Ext.create('Bdcom.store.Scheduler',{storeId : 'store.Scheduler'});
                  
      this.callParent(arguments);
    }
  });
  })();