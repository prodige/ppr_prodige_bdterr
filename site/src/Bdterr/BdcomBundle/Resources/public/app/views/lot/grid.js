(function(){
// Store relier aux lots de données
var _storeTheme = Ext.StoreMgr.lookup('store.Theme') || Ext.create('Bdcom.store.Theme',{storeId : 'store.Theme'});
_storeTheme.load(); // Fix problème de chargement de l'affichage des thèmes des lots
var _storeRefIntersection = Ext.StoreMgr.lookup('store.ReferentielIntersection') || Ext.create('Bdcom.store.ReferentielIntersection',{storeId : 'store.ReferentielIntersection'});

// grille de recherche
Ext.define('Bdcom.views.lot.grid', {
  requires: [ 'Bdcom.model.Lot'],
  extend: 'Ext.grid.Panel',
  xtype: 'lot.grid',

  border : true,
  height: 200,

  forceFit: true,

  tbar: [{
    text: 'Ajouter un lot de données',
	  tooltip : 'Ajouter un lot de données PRODIGE dans la base territoriale',
    ref: 'lot',
    toView: 'addLot',
    listeners: {
      click: 'onWidget'
    }
  }],

  viewConfig: {
    getRowClass: function(record, index, rowParams, store) {
      if(typeof  record.isConfigured === 'function' && record.isConfigured()){
        return  'grey-background';
      }

      return '';
    }
  },
  columns: [{
    text: 'Alias',
    dataIndex: 'alias',
    width: '20%',
  },{
    text: 'Table',
    dataIndex: 'lotTable',
    width: '20%',
  },{
    text: 'Sous-Thème',
    dataIndex: 'themeId',
    width: '20%',
    sortable: false,
    renderer: function(themeId){
      let result = _storeTheme.getById(themeId);
      return themeId && result ? result.get('nom') : "";
    },
  },{
    text: 'Modifier',
    // This is our Widget column
    xtype: 'widgetcolumn',
    width: '10%',
    widget: {
      xtype: 'button',
      ref: 'lot',
      text: 'Modifier',//'<i class="fa fa-pencil" aria-hidden="true" ></i>',
      tooltip : 'Modifier le paramétrage du lot de données',
      toView: 'tabLot',
      listeners: {
        click: 'onWidget'
      }
    }
  }, {
    text: 'Supprimer',
    // This is our Widget column
    xtype: 'widgetcolumn',
    width: '10%',
    widget: {
      xtype: 'button',
      tooltip: 'Supprimer le lot de données (de la base territoriale)',
      text: 'Supprimer',//'<i class="fa fa-times" aria-hidden="true" style="color: red" ></i>',
      listeners: {
        click: 'onDeleteButton'
      }
    },
  }],
  /////////////////////////////// FONCTIONS //////////////////////////////////////////////////////////

  loadStore: function(){
    _storeTheme.load();
    _storeRefIntersection.load();
    
  },
  initComponent : function(){
    this.store =  Ext.StoreMgr.lookup('store.Lot') || Ext.create('Bdcom.store.Lot',{storeId : 'store.Lot'});
                
    this.callParent(arguments);
  }
});
})();