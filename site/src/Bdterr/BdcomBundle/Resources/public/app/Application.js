(function(){
  // Pour charger le fichier
  Ext.define('Bdcom.Application', {
    alias: 'Bdcom.Application'
  });
})();

function Application(){
  var _mainPanel = null;

  function setMainPanel(mainPanel){
    _mainPanel = mainPanel;
   
  }

  // Application ExtJs
  Ext.define('Bdcom.MainApplication', {
    extend : 'Ext.app.Application',
    name : '_APPLICATION_',
    alias: "Bdcom.MainApplication",
  
    controllers: ['Bdcom.controller.mainCtrl'],

    launch: function() {
     
      if( _mainPanel ){
        var mainCtrl = this.getController('Bdcom.controller.mainCtrl');
       
        mainCtrl.mainPanel = _mainPanel;
        
        if(!window.location.hash){
          mainCtrl.onNav('configuration');
        }
        else if($(window.location.hash)){
          $(window.location.hash).toggleClass('active');
        }
      }
    }
  });

  return {
    setMainPanel: setMainPanel
  }
}