(function(){

  function close(form, component){
    if(component && form && form.ref && component.controller && typeof component.controller.onSwitchView === 'function'){
      component.controller.onSwitchView(form.toView, form);
    }

  }

  
  // Envoies les données du formulaire au serveur
  function sendDataToSever(form){
    if( !form.isDirty() ){
      close(form);
    }
    if ( form.isValid() ){
      form.updateRecord();
      form.getRecord().save({
        callback: function(record, operation, success){
          Util.showMessageServer(operation, success);
          success && close(form,  Ext.getCmp('mainPanel'));
        }
      })
    }
  }

  // Formulaire d'édition du référentiel de recherche
  Ext.define('Bdcom.commun.formGeneric', {
    requires: ['Bdcom.controller.configurationCtrl', 'Bdcom.commun.Util'],
    extend: 'Ext.form.Panel',
    xtype: 'commun.formGeneric',
    
    height: 600,
    width: 170,
      
    bodyPadding: 15,
    border : true,

    forceFit: true,

    tbar: [{
      //text: '<i class="fa fa-times" aria-hidden="true"></i>',
	  text: 'Annuler',
      listeners: {
        click: 'onWidget'
      }
    },{
      text: 'Enregistrer',
      formBind: true, //only enabled once the form is valid
      disabled: true,
      handler: function(){
        sendDataToSever(this.up('form'));
      }
    }],
    defaults: { // defaults are applied to items, not the container
      width: "50%",
	    labelWidth:170
	  
    },
    // The fields
    defaultType: 'textfield',
    items : [],

    buttons: [],
    setFormWithItemModel: function(model){
      var items = Ext.create(model);
      this.items.addAll( items.items );
    },  
    setRefTBar: function(ref, toView){
      this.ref = ref;
      this.toView = toView;
      if(this.down('toolbar') && this.down('toolbar').items && typeof this.down('toolbar').items.each === 'function'){
        this.down('toolbar').items.each(function(item){
          item.ref = ref;
          item.toView = toView;
        });
      }
    },
    initComponent : function(){
      this.callParent(arguments)
    }
  });

})();