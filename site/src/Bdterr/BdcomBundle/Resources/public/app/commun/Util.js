(function() {
  Ext.define('Bdcom.commun.Util',{
    alias: 'load.util'
  })
 
  /**
   * 
   * @param {*} callback 
   */
  function getOptionRequest(callbackNext ){
    return {
      callback: function(record, operation, success){
        Util.showMessageServer(operation, success);
        if(typeof callback === 'function'){
          callbackNext(success);
        }
      }
    }
  }

  /**
   * 
   * @param {*} operation 
   * @param {*} sucess 
   */
  function showMessageServer(operation, sucess){
    response = operation.getResponse() && operation.getResponse().responseText ?  JSON.parse( operation.getResponse().responseText ) : null;
  
    if(sucess){
      Ext.Msg.alert('Enregistrement', response && response.message ?  response.message : 'Mise à jour réussie').setIcon(Ext.Msg.INFO);
    }
    else{
      console.error("error", operation);
      Ext.Msg.alert('Enregistrement', response && response.message ?  response.message : 'Problème pendant la  Mise à jour').setIcon(Ext.Msg.INFO);
    }
  }

  /**
   * 
   * @param {*} model 
   */
  function getItemModel(model){
    var items = Ext.create(model);
    return items.items ? items.items : [];
  }

  Ext.define('Util',{
    statics: {
      showMessageServer: showMessageServer,
      getOptionRequest : getOptionRequest,
      getItemModel: getItemModel
    }
  });


  
})();