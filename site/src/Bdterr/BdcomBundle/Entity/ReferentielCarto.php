<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
/**
 * ReferentielCarto
 *
 * @ORM\Table(name="bdterr.bdterr_referentiel_carto")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\ReferentielCartoRepository")
 */
class ReferentielCarto
{
  /**
   * @var int
   *
   * @ORM\Column(name="referentiel_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_table", type="text")
   */
  private $referentielTable;

  /**
   * @Type("array<string>")
   * @Accessor(getter="getApiChamps",setter="setApiChamps") 
   * @ORM\Column(name="referentiel_champs", type="text", nullable=true)
   */
  private $apiChamps;


  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Get the value of referentielTable
   *
   * @return  string
   */ 
  public function getReferentielTable()
  {
    return $this->referentielTable;
  }

  /**
   * Set the value of referentielTable
   *
   * @param  string  $referentielTable
   *
   * @return  self
   */ 
  public function setReferentielTable( $referentielTable)
  {
    $this->referentielTable = $referentielTable;

    return $this;
  }

  /**
   * Get the value of apiChamps
   */ 
  public function getApiChamps()
  {
    return json_decode($this->apiChamps);
  }

  /**
   * Set the value of apiChamps
   *
   * @return  self
   */ 
  public function setApiChamps($apiChamps)
  {
    $this->apiChamps = json_encode($apiChamps);

    return $this;
  }
}

