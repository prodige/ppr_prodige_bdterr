<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Donnee
 *
 * @ORM\Table(name="bdterr.bdterr_donnee")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\DonneeRepository")
 */
class Donnee 
{
    /**
     * @var int
     *
     * @ORM\Column(name="donnee_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="bdterr.donnee_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;


    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\Lot", inversedBy="donnees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="donnee_lot_id", referencedColumnName="lot_id",   nullable=true )
     * })
     */
    private $lot;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_objet_id", type="text", nullable=true)
     */
    private $objetId;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_objet_nom", type="text", nullable=true)
     */
    private $objetNom;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_geom", type="text", nullable=true)
     */
    private $geom;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_geom_json", type="text", nullable=true)
     */
    private $geomJson;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_attributs_fiche", type="text", nullable=true)
     */
    private $attributsFiche;
    
    /**
     * @var string
     *
     * @ORM\Column(name="donnee_attributs_resultat", type="text", nullable=true)
     */
    private $attributsResultat;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_ressources_resultat", type="text", nullable=true)
     */
    private $ressourcesResultat;
   
    /**
     * @var string
     *
     * @ORM\Column(name="donnee_ressources_fiche", type="text", nullable=true)
     */
    private $ressourcesFiche;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_insee", type="text", nullable=true)
     */
    private $insee;

    /**
     * @var string
     *
     * @ORM\Column(name="donnee_insee_delegue", type="text", nullable=true)
     */
    private $inseeDelegue;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lotId
     *
     * @param integer $lotId
     *
     * @return Donnee
     */
    public function setLot($lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * Get lotId
     *
     * @return int
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * Set objetId
     *
     * @param string $objetId
     *
     * @return Donnee
     */
    public function setObjetId($objetId)
    {
        $this->objetId = $objetId;

        return $this;
    }

    /**
     * Get objetId
     *
     * @return string
     */
    public function getObjetId()
    {
        return $this->objetId;
    }

    /**
     * Set objetNom
     *
     * @param string $objetNom
     *
     * @return Donnee
     */
    public function setObjetNom($objetNom)
    {
        $this->objetNom = $objetNom;

        return $this;
    }

    /**
     * Get objetNom
     *
     * @return string
     */
    public function getObjetNom()
    {
        return $this->objetNom;
    }

    /**
     * Set geom
     *
     * @param geometry $geom
     *
     * @return Donnee
     */
    public function setGeom($geom)
    {

        $this->geom = $geom;

        return $this;
    }

    /**
     * Get geom
     *
     * @return geometry
     */
    public function getGeom()
    {
        return $this->geom;
    }

    /**
     * Set geomJson
     *
     * @param geometry $geom
     *
     * @return Donnee
     */
    public function setGeomJson($geomJson)
    {

        $this->geomJson = $geomJson;

        return $this;
    }

    /**
     * Get geomJson
     *
     * @return geometry
     */
    public function getGeomJson()
    {
        return $this->geomJson;
    }

    /**
     * Set attributsFiche
     *
     * @param string $attributsFiche
     *
     * @return Donnee
     */
    public function setAttributsFiche($attributsFiche)
    {
        $this->attributsFiche = $attributsFiche;

        return $this;
    }

    /**
     * Get attributsFiche
     *
     * @return string
     */
    public function getAttributsFiche()
    {
        return $this->attributsFiche;
    }

    /**
     * Set ressources
     *
     * @param string $ressourcesResultat
     *
     * @return Donnee
     */
    public function setRessourcesResultat($ressourcesResultat)
    {
        $this->ressourcesResultat = $ressourcesResultat;

        return $this;
    }

    /**
     * Get ressources
     *
     * @return string
     */
    public function getRessourcesResultat()
    {
        return $this->ressourcesResultat;
    }

    /**
     * Set insee
     *
     * @param string $insee
     *
     * @return Donnee
     */
    public function setInsee($insee)
    {
        $this->insee = $insee;

        return $this;
    }

    /**
     * Get insee
     *
     * @return string
     */
    public function getInsee()
    {
        return $this->insee;
    }

    /**
     * Set inseeDelegue
     *
     * @param string $inseeDelegue
     *
     * @return Donnee
     */
    public function setInseeDelegue($inseeDelegue)
    {
        $this->inseeDelegue = $inseeDelegue;

        return $this;
    }

    /**
     * Get inseeDelegue
     *
     * @return string
     */
    public function getInseeDelegue()
    {
        return $this->inseeDelegue;
    }

    /**
     * Get the value of ressourcesFiche
     *
     * @return  string
     */ 
    public function getRessourcesFiche()
    {
        return $this->ressourcesFiche;
    }

    /**
     * Set the value of ressourcesFiche
     *
     * @param  string  $ressourcesFiche
     *
     * @return  self
     */ 
    public function setRessourcesFiche( $ressourcesFiche)
    {
        $this->ressourcesFiche = $ressourcesFiche;

        return $this;
    }

    /**
     * Get the value of attributsResultat
     *
     * @return  string
     */ 
    public function getAttributsResultat()
    {
        return $this->attributsResultat;
    }

    /**
     * Set the value of attributsResultat
     *
     * @param  string  $attributsResultat
     *
     * @return  self
     */ 
    public function setAttributsResultat( $attributsResultat)
    {
        $this->attributsResultat = $attributsResultat;

        return $this;
    }
}

