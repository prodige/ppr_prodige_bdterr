<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChampType
 *
 * @ORM\Table(name="bdterr.bdterr_champ_type")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\ChampTypeRepository")
 */
class ChampType
{
  /**
   * @var int
   *
   * @ORM\Column(name="champtype_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.champtype_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="champtype_nom", type="string", length=255, nullable=true)
   */
  private $nom;

  /**
   * @var bool
   *
   * @ORM\Column(name="champtype_inContenuTier", type="boolean", nullable=true, options={"default" : true})
   */
  private $inContenuTier;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return ChampType
   */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Get the value of inContenuTier
   *
   * @return  bool
   */ 
  public function getInContenuTier()
  {
    return $this->inContenuTier;
  }

  /**
   * Set the value of inContenuTier
   *
   * @param  bool  $inContenuTier
   *
   * @return  self
   */ 
  public function setInContenuTier($inContenuTier)
  {
    $this->inContenuTier = $inContenuTier;

    return $this;
  }
}

