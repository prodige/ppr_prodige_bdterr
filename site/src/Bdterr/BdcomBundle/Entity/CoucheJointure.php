<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * CoucheJointure
 *
 * @ORM\Table(name="bdterr.bdterr_couche_jointure")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\CoucheJointureRepository")
 */
class CoucheJointure
{
    /**
     * @var int
     *
     * @ORM\Column(name="join_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="bdterr.join_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     *
     * @Exclude
     *
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\Lot", inversedBy="coucheJointures")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="join_lot_id", referencedColumnName="lot_id",   nullable=false )
     * })
     */
    private $lot;

    /**
     * @var string
     *
     * @ORM\Column(name="join_lot_field", type="string", length=150)
     */
    private $lot_field;

    /**
     * @var string
     *
     * @ORM\Column(name="join_prod_table", type="string", length=255, nullable=true)
     */
    private $prodige_table;

    /**
     * @var string
     *
     * @ORM\Column(name="join_prod_key", type="string", length=255, nullable=true)
     */
    private $prodige_key;

    /**
     * @var int
     *
     * @ORM\Column(name="join_prod_field", type="string", length=255, nullable=true)
     */
    private $prodige_field;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get lotId
     *
     * @return int
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * Set lot
     *
     * @param integer $lot
     *
     * @return CoucheJointure
     */
    public function setLot($lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * Get lotField
     *
     * @VirtualProperty
     * @SerializedName("lotField")
     *
     * @return string
     */
    public function getLotField()
    {
        return $this->lot_field;
    }

    /**
     * Set lot
     *
     * @param string $lotField
     *
     * @return CoucheJointure
     */
    public function setLotField($lotField)
    {
        $this->lot_field = $lotField;

        return $this;
    }


    /**
     * Get prodigeTable
     *
     * @VirtualProperty
     * @SerializedName("prodigeTable")
     *
     * @return string
     */
    public function getProdigeTable()
    {
        return $this->prodige_table;
    }

    /**
     * Set prodigeTable
     *
     * @param string $prodigeTable
     *
     * @return CoucheJointure
     */
    public function setProdigeTable($prodigeTable)
    {
        $this->prodige_table = $prodigeTable;

        return $this;
    }

    /**
     * Get prodigeTable
     *
     * @VirtualProperty
     * @SerializedName("prodigeKey")
     *
     * @return string
     */
    public function getProdigeKey()
    {
        return $this->prodige_key;
    }

    /**
     * Set prodigeTable
     *
     * @param string $prodigeKey
     *
     * @return CoucheJointure
     */
    public function setProdigeKey($prodigeKey)
    {
        $this->prodige_key = $prodigeKey;

        return $this;
    }

    /**
     * Get prodigeField
     *
     * @VirtualProperty
     * @SerializedName("prodigeField")
     *
     * @return string
     */
    public function getProdigeField()
    {
        return $this->prodige_field;
    }

    /**
     * Set lot
     *
     * @param string $prodigeField
     *
     * @return CoucheJointure
     */
    public function setProdigeField($prodigeField)
    {
        $this->prodige_field = $prodigeField;

        return $this;
    }

    /**
     * Get lot Id
     * @VirtualProperty
     * @SerializedName("lotId")
     *
     * @return array
     */
    public function getLotId() {
        return $this->getLot() ?  $this->getLot()->getId() : null;
    }
}

