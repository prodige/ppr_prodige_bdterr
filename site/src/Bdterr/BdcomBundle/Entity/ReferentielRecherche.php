<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\Type;
/**
 * ReferentielRecherche
 *
 * @ORM\Table(name="bdterr.bdterr_referentiel_recherche")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\ReferentielRechercheRepository")
 */
class ReferentielRecherche
{
  /**
   * @var int
   *
   * @ORM\Column(name="referentiel_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.referentiel_recherche_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_table", type="string", length=255, nullable=true)
   */
  private $referentielTable;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_insee", type="string", length=255, nullable=true)
   */
  private $insee;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_insee_delegue", type="string", length=255, nullable=true)
   */
  private $inseeDeleguee;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_commune", type="string", length=255, nullable=true)
   */
  private $commune;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_nom", type="text", nullable=true)
   */
  private $nom;

  /**
   * @Type("array<string>")
   * @Accessor(getter="getApiChamps",setter="setApiChamps") 
   * @var string
   *
   * @ORM\Column(name="referentiel_api_champs", type="text", nullable=true)
   */
  private $apiChamps;


  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set referentielTable
   *
   * @param string $referentielTable
   *
   * @return ReferentielRecherche
   */
  public function setReferentielTable($referentielTable)
  {
      $this->referentielTable = $referentielTable;

      return $this;
  }

  /**
   * Get referentielTable
   *
   * @return string
   */
  public function getReferentielTable()
  {
      return $this->referentielTable;
  }

  /**
   * Set insee
   *
   * @param string $insee
   *
   * @return ReferentielRecherche
   */
  public function setInsee($insee)
  {
      $this->insee = $insee;

      return $this;
  }

  /**
   * Get insee
   *
   * @return string
   */
  public function getInsee()
  {
      return $this->insee;
  }

  /**
   * Set inseeDeleguee
   *
   * @param string $inseeDeleguee
   *
   * @return ReferentielRecherche
   */
  public function setInseeDeleguee($inseeDeleguee)
  {
      $this->inseeDeleguee = $inseeDeleguee;

      return $this;
  }

  /**
   * Get inseeDeleguee
   *
   * @return string
   */
  public function getInseeDeleguee()
  {
      return $this->inseeDeleguee;
  }

  /**
   * Set commune
   *
   * @param string $commune
   *
   * @return ReferentielRecherche
   */
  public function setCommune($commune)
  {
      $this->commune = $commune;

      return $this;
  }

  /**
   * Get commune
   *
   * @return string
   */
  public function getCommune()
  {
      return $this->commune;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return ReferentielRecherche
   */
  public function setNom($nom)
  {
      $this->nom = $nom;

      return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
      return $this->nom;
  }

  /**
   * Set apiChamps
   *
   * @param string $apiChamps
   *
   * @return ReferentielRecherche
   */
  public function setApiChamps($apiChamps)
  {
    $this->apiChamps = json_encode($apiChamps);

    return $this;
  }

  /**
   * Get apiChamps
   *
   * @return string
   */
  public function getApiChamps()
  {
    return json_decode($this->apiChamps, true);
  }
}

