<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
/**
 * Rapport
 *
 * @ORM\Table(name="bdterr.bdterr_rapports")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\RapportRepository")
 */
class Rapport
{
  /**
   * @var int
   *
   * @ORM\Column(name="rapport_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.rapport_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="rapport_nom", type="string", length=255, nullable=true)
   */
  private $nom;

  /**
   * @var string
   *
   * @ORM\Column(name="rapport_gabarit_html", type="string", length=255, nullable=true)
   */
  private $gabaritHtml;

  /**
   * @var string
   *
   * @ORM\Column(name="rapport_gabarit_odt", type="string", length=255, nullable=true)
   */
  private $gabaritOdt;

  /**
   * @var \Doctrine\Common\Collections\Collection
   * @Exclude
   *
   * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\RapportsProfil", mappedBy="rapport")
   */
  private $profils;
  
  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return rapports
   */
  public function setNom($nom)
  {
      $this->nom = $nom;

      return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
      return $this->nom;
  }

  /**
   * Set gabaritHtml
   *
   * @param string $gabaritHtml
   *
   * @return rapports
   */
  public function setGabaritHtml($gabaritHtml)
  {
      $this->gabaritHtml = $gabaritHtml;

      return $this;
  }

  /**
   * Get gabaritHtml
   *
   * @return string
   */
  public function getGabaritHtml()
  {
      return $this->gabaritHtml;
  }

  /**
   * Set gabaritOdt
   *
   * @param string $gabaritOdt
   *
   * @return rapports
   */
  public function setGabaritOdt($gabaritOdt)
  {
      $this->gabaritOdt = $gabaritOdt;

      return $this;
  }

  /**
   * Get gabaritOdt
   *
   * @return string
   */
  public function getGabaritOdt()
  {
      return $this->gabaritOdt;
  }

  /**
   * Get theme Id
   * @VirtualProperty
   * @SerializedName("themeId")
   *
   * @return array
   */
  public function getProfilIds() {
    $result = array();

    foreach($this->getProfils() as $profil){
      if($profil->getProdigeProfilId()){
        $result[] = $profil->getProdigeProfilId();
      }
    }

    return $result;
  }


  /**
   * Get the value of profils
   *
   * @return  \Doctrine\Common\Collections\Collection
   */
  public function getProfils()
  {
    return $this->profils;
  }
}

