<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RapportsProfil
 *
 * @ORM\Table(name="bdterr.bdterr_rapports_profils")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\RapportsProfilRepository")
 */
class RapportsProfil
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.rapports_profils_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

    /**
   * @var int
   * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\Rapport", inversedBy="profils")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="bterr_rapport_id", referencedColumnName="rapport_id", nullable=true )
   * })
   */
  private $rapport;

  /**
   * @var int
   *
   * @ORM\Column(name="prodige_profil_id", type="integer", nullable=true)
   */
  private $prodigeProfilId;


  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }



  /**
   * Set prodigeProfilId
   *
   * @param integer $prodigeProfilId
   *
   * @return rapportsProfil
   */
  public function setProdigeProfilId($prodigeProfilId)
  {
    $this->prodigeProfilId = $prodigeProfilId;

    return $this;
  }

  /**
   * Get prodigeProfilId
   *
   * @return int
   */
  public function getProdigeProfilId()
  {
    return $this->prodigeProfilId;
  }

  /**
   * Get })
   *
   * @return  int
   */
  public function getRapport()
  {
    return $this->rapport;
  }

  /**
   * Set })
   *
   * @param  int  $rapport  })
   *
   * @return  self
   */
  public function setRapport($rapport)
  {
    $this->rapport = $rapport;

    return $this;
  }
}

