<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * CoucheChamp
 *
 * @ORM\Table(name="bdterr.bdterr_couche_champ")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\CoucheChampRepository")
 */
class CoucheChamp
{
  /**
   * @var int
   *
   * @ORM\Column(name="champ_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.champ_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

  /**
   * 
   * @Exclude
   * 
   * @var int
   * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\Lot", inversedBy="coucheChamps")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="champ_lot_id", referencedColumnName="lot_id",   nullable=false )
   * })
   */
  private $lot;

  /**
   * @var string
   *
   * @ORM\Column(name="champ_nom", type="string", length=150)
   */
  private $nom;

  /**
   * @var string
   *
   * @ORM\Column(name="champ_alias", type="string", length=255, nullable=true)
   */
  private $alias;

  /**
   * @var int
   *
   * @ORM\Column(name="champ_ordre", type="integer", nullable=true)
   */
  private $ordre;

  /**
   * @Exclude
   * 
   * @var int
   * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\ChampType", inversedBy="coucheChamps")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="champ_type", referencedColumnName="champtype_id",   nullable=true )
   * })
   */
  private $type;

  /**
   * @var string
   *
   * @ORM\Column(name="champ_format", type="string", length=255, nullable=true)
   */
  private $format;

  /**
   * @var bool
   *
   * @ORM\Column(name="champ_fiche", type="boolean", nullable=true)
   */
  private $fiche;

  /**
   * @var bool
   *
   * @ORM\Column(name="champ_resultats", type="boolean", nullable=true)
   */
  private $resultats;
  
  /**
   * @var bool
   *
   * @ORM\Column(name="champ_identifiant", type="boolean", nullable=true)
   */
  private $identifiant;

  /**
   * @var bool
   *
   * @ORM\Column(name="champ_label", type="boolean", nullable=true)
   */
  private $label;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set lot
   *
   * @param integer $lot
   *
   * @return CoucheChamp
   */
  public function setLot($lot)
  {
    $this->lot = $lot;

    return $this;
  }

  /**
   * Get lotId
   *
   * @return int
   */
  public function getLot()
  {
    return $this->lot;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return CoucheChamp
   */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set alias
   *
   * @param string $alias
   *
   * @return CoucheChamp
   */
  public function setAlias($alias)
  {
    $this->alias = $alias;

    return $this;
  }

  /**
   * Get alias
   *
   * @return string
   */
  public function getAlias()
  {
    return $this->alias;
  }

  /**
   * Set ordre
   *
   * @param integer $ordre
   *
   * @return CoucheChamp
   */
  public function setOrdre($ordre)
  {
    $this->ordre = $ordre;

    return $this;
  }

  /**
   * Get ordre
   *
   * @return int
   */
  public function getOrdre()
  {
    return $this->ordre;
  }

  /**
   * Set type
   *
   * @param integer $type
   *
   * @return CoucheChamp
   */
  public function setType($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Get type
   *
   * @return int
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set format
   *
   * @param string $format
   *
   * @return CoucheChamp
   */
  public function setFormat($format)
  {
    $this->format = $format;

    return $this;
  }

  /**
   * Get format
   *
   * @return string
   */
  public function getFormat()
  {
    return $this->format;
  }

  /**
   * Set fiche
   *
   * @param boolean $fiche
   *
   * @return CoucheChamp
   */
  public function setFiche($fiche)
  {
    $this->fiche = $fiche;

    return $this;
  }

  /**
   * Get fiche
   *
   * @return bool
   */
  public function getFiche()
  {
    return $this->fiche;
  }

  /**
   * Set resultats
   *
   * @param boolean $resultats
   *
   * @return CoucheChamp
   */
  public function setResultats($resultats)
  {
    $this->resultats = $resultats;

    return $this;
  }

  /**
   * Get resultats
   *
   * @return bool
   */
  public function getResultats()
  {
    return $this->resultats;
  }

  /**
   * Get the value of identifiant
   *
   * @return  bool
   */ 
  public function getIdentifiant()
  {
    return $this->identifiant;
  }

  /**
   * Set the value of identifiant
   *
   * @param  bool  $identifiant
   *
   * @return  self
   */ 
  public function setIdentifiant( $identifiant)
  {
    $this->identifiant = $identifiant;

    return $this;
  }

  /**
   * Get the value of label
   *
   * @return  bool
   */ 
  public function getLabel()
  {
    return $this->label;
  }

  /**
   * Set the value of label
   *
   * @param  bool  $label
   *
   * @return  self
   */ 
  public function setLabel( $label)
  {
    $this->label = $label;

    return $this;
  }

  /** 
   * Get lot Id  
   * @VirtualProperty
   * @SerializedName("lotId")
   * 
   * @return array
   */
  public function getLotId() {
    return $this->getLot() ?  $this->getLot()->getId() : null;
  }

  /** 
   * Get lot Id  
   * @VirtualProperty
   * @SerializedName("typeId")
   * 
   * @return array
   */
  public function getTypeId() {
    return $this->getType() ?  $this->getType()->getId() : null;
  }
}

