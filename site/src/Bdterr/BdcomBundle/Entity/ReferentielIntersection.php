<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * ReferentielIntersection
 *
 * @ORM\Table(name="bdterr.bdterr_referentiel_intersection")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\ReferentielIntersectionRepository")
 */
class ReferentielIntersection
{ 
  public function __constructor(){
    $this->lots = new \Doctrine\Common\Collections\ArrayCollection();
  }
  /**
   * @var int
   *
   * @ORM\Column(name="referentiel_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.referentiel_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_table", type="string", length=255, nullable=true)
   */
  private $referentielTable;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_insee", type="string", length=255, nullable=true)
   */
  private $insee;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_insee_deleguee", type="string", length=255, nullable=true)
   */
  private $inseeDeleguee;

  /**
   * @var string
   *
   * @ORM\Column(name="referentiel_nom", type="string", length=255, nullable=true)
   */
  private $nom;

  /**
   * @var \Doctrine\Common\Collections\Collection
   * @Exclude
   *
   * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\Lot", mappedBy="referentiel")
   */
  private $lots;

  /**
    * @var bool
    *
    * @ORM\Column(name="referentiel_statistics_ref", type="boolean", nullable=true, options={"default" : false})
    */
  private $statisticIntersect;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set referentielTable
   *
   * @param string $referentielTable
   *
   * @return ReferentielIntersection
   */
  public function setReferentielTable($referentielTable)
  {
      $this->referentielTable = $referentielTable;

      return $this;
  }

  /**
   * Get referentielTable
   *
   * @return string
   */
  public function getReferentielTable()
  {
      return $this->referentielTable;
  }

  /**
   * Set insee
   *
   * @param string $insee
   *
   * @return ReferentielIntersection
   */
  public function setInsee($insee)
  {
      $this->insee = $insee;

      return $this;
  }

  /**
   * Get insee
   *
   * @return string
   */
  public function getInsee()
  {
      return $this->insee;
  }

  /**
   * Set inseeDeleguee
   *
   * @param string $inseeDeleguee
   *
   * @return ReferentielIntersection
   */
  public function setInseeDeleguee($inseeDeleguee)
  {
      $this->inseeDeleguee = $inseeDeleguee;

      return $this;
  }

  /**
   * Get inseeDeleguee
   *
   * @return string
   */
  public function getInseeDeleguee()
  {
      return $this->inseeDeleguee;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return ReferentielIntersection
   */
  public function setNom($nom)
  {
      $this->nom = $nom;

      return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
      return $this->nom;
  }

  /**
   * Get the value of lots
   *
   * @return  \Doctrine\Common\Collections\Collection
   */ 
  public function getLots()
  {
      return $this->lots;
  }

  /**
   * Get the value of statisticIntersect
   *
   * @return  bool
   */
  public function getStatisticIntersect()
  {
      return $this->statisticIntersect;
  }

  /**
   * Set the value of statisticIntersect
   *
   * @param  boolean  $statisticIntersect
   *
   * @return  self
   */
  public function setStatisticIntersect($statisticIntersect)
  {
      $this->statisticIntersect = $statisticIntersect;

      return $this;
  }
}

