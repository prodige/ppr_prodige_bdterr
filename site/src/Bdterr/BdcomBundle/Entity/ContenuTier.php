<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * contenusTiers
 *
 * @ORM\Table(name="bdterr.bdterr_contenus_tiers")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\ContenuTierRepository")
 */
class ContenuTier
{
  /**
   * @var int
   *
   * @ORM\Column(name="contenu_id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="SEQUENCE")
   * @ORM\SequenceGenerator(sequenceName="bdterr.contenu_id_seq", allocationSize=1, initialValue=1)
   */
  private $id;

  /**
   * @Exclude
   * 
   * @var int
   * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\Lot", inversedBy="contenusTiers")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="lot_id", referencedColumnName="lot_id",   nullable=true )
   * })
   */
  private $lot;

  /**
   * @var string
   *
   * @ORM\Column(name="contenu_nom", type="string", length=255, nullable=true)
   */
  private $nom;

  /**
   * @var int
   *
   * @ORM\Column(name="contenu_ordre", type="integer", nullable=true)
   */
  private $ordre;

  /**
   * @var bool
   *
   * @ORM\Column(name="contenu_fiche", type="boolean", nullable=true)
   */
  private $fiche;

  /**
   * @var bool
   *
   * @ORM\Column(name="contenu_resultats", type="boolean", nullable=true)
   */
  private $resultats;

  /**
   * @var int
   *
   * @ORM\Column(name="contenu_url", type="string", length=255, nullable=false)
   */
  private $url;

  /**
   * @Exclude
   * 
   * @var int
   * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\ChampType", inversedBy="contenuTier")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="contenu_type", referencedColumnName="champtype_id",   nullable=true )
   * })
   */
  private $type;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set lotId
   *
   * @param integer $lotId
   *
   * @return contenusTiers
   */
  public function setLot($lot)
  {
    $this->lot = $lot;

    return $this;
  }

  /**
   * Get lotId
   *
   * @return int
   */
  public function getLot()
  {
    return $this->lot;
  }

  /**
   * Set nom
   *
   * @param string $nom
   *
   * @return contenusTiers
   */
  public function setNom($nom)
  {
    $this->nom = $nom;

    return $this;
  }

  /**
   * Get nom
   *
   * @return string
   */
  public function getNom()
  {
    return $this->nom;
  }

  /**
   * Set ordre
   *
   * @param integer $ordre
   *
   * @return contenusTiers
   */
  public function setOrdre($ordre)
  {
    $this->ordre = $ordre;

    return $this;
  }

  /**
   * Get ordre
   *
   * @return int
   */
  public function getOrdre()
  {
    return $this->ordre;
  }

  /**
   * Set fiche
   *
   * @param boolean $fiche
   *
   * @return contenusTiers
   */
  public function setFiche($fiche)
  {
    $this->fiche = $fiche;

    return $this;
  }

  /**
   * Get fiche
   *
   * @return bool
   */
  public function getFiche()
  {
    return $this->fiche;
  }

  /**
   * Set resultats
   *
   * @param boolean $resultats
   *
   * @return contenusTiers
   */
  public function setResultats($resultats)
  {
    $this->resultats = $resultats;

    return $this;
  }

  /**
   * Get resultats
   *
   * @return bool
   */
  public function getResultats()
  {
    return $this->resultats;
  }

  /**
   * Set url
   *
   * @param integer $url
   *
   * @return contenusTiers
   */
  public function setUrl($url)
  {
    $this->url = $url;

    return $this;
  }

  /**
   * Get url
   *
   * @return int
   */
  public function getUrl()
  {
    return $this->url;
  }

   /** 
   * Get lot Id  
   * @VirtualProperty
   * @SerializedName("lotId")
   * 
   * @return array
   */
  public function getLotId() {
    return $this->getLot() ?  $this->getLot()->getId() : null;
  }
 
  /** 
   * Get lot Id  
   * @VirtualProperty
   * @SerializedName("typeId")
   * 
   * @return array
   */
  public function getTypeId() {
    return $this->getType() ?  $this->getType()->getId() : null;
  }
  
  /**
   * Get 
   *
   * @return  int
   */ 
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set 
   *
   * @param  int  $type  
   *
   * @return  self
   */ 
  public function setType($type)
  {
    $this->type = $type;

    return $this;
  }
}

