<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Lot
 *
 * @ORM\Table(name="bdterr.bdterr_lot")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\LotRepository")
 */
class Lot
{
    /**
     * @var int
     *
     * @ORM\Column(name="lot_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="bdterr.lot_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_uuid", type="string", length=255, nullable=true)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_table", type="string", length=255, nullable=true)
     */
    private $lotTable;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_alias", type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @var string
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\Theme", inversedBy="lots")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lot_theme_id", referencedColumnName="theme_id",   nullable=true )
     * })
     */
    private $theme;

    /**
     * @var int
     *
     * @Exclude
     *
     * @ORM\ManyToOne(targetEntity="App\Bdterr\BdcomBundle\Entity\ReferentielIntersection", inversedBy="lots")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lot_referentiel_id", referencedColumnName="referentiel_id",   nullable=true )
     * })
     */
    private $referentiel;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_carte", type="string", length=255, nullable=true)
     */
    private $carte;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_avertissement_message", type="string", length=255, nullable=true)
     */
    private $avertissementMessage;

    /**
     * @var bool
     *
     * @ORM\Column(name="lot_statistiques", type="boolean", nullable=true)
     */
    private $statistiques;

    /**
     * @var bool
     *
     * @ORM\Column(name="lot_visualiseur", type="boolean", nullable=true)
     */
    private $visualiseur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Exclude
     *
     * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\ContenuTier", mappedBy="lot")
     */
    private $contenusTiers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Exclude
     *
     * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\CoucheChamp", mappedBy="lot")
     * @ORM\OrderBy({"ordre" = "ASC"})
     */
    private $coucheChamps;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Exclude
     *
     * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\CoucheJointure", mappedBy="lot")
     */
    private $coucheJointures;


    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Exclude
     *
     * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\Donnee", mappedBy="lot")
     */
    private $donnees;

    /**
     * @var \Doctrine\Common\Collections\DateTime
     *
     * @ORM\Column(name="lot_date_maj", type="datetime", nullable=true)
     */
    private $dateMaj;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_statistic", type="text", nullable=true)
     */
    private $statistic;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_layer_name", type="text", nullable=true)
     */
    private $layerName;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_cutting_type", type="string", nullable=true, options={"default": "ref"})
     */
    private $cuttingType;

    /**
     * @var int
     *
     * @ORM\Column(name="lot_buffer", type="integer", options={"default" : 0})
     */
    private $buffer;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_display_type", type="string", nullable=false, options={"default": "bt"})
     */
    private $displayType;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_mapping", type="string", nullable=true)
     */
    private $mapping;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_mapping_field", type="string", nullable=true)
     */
    private $mappingField;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return Lot
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set lotTable
     *
     * @param string $lotTable
     *
     * @return Lot
     */
    public function setLotTable($lotTable)
    {
        $this->lotTable = $lotTable;

        return $this;
    }

    /**
     * Get lotTable
     *
     * @return string
     */
    public function getLotTable()
    {
        return $this->lotTable;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Lot
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Lot
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set referentiel
     *
     * @param integer $referentiel
     *
     * @return Lot
     */
    public function setReferentiel($referentiel)
    {
        $this->referentiel = $referentiel;

        return $this;
    }

    /**
     * Get referentiel
     *
     * @return int
     */
    public function getReferentiel()
    {
        return $this->referentiel;
    }

    /**
     * Set carte
     *
     * @param string $carte
     *
     * @return Lot
     */
    public function setCarte($carte)
    {
        $this->carte = $carte;

        return $this;
    }

    /**
     * Get carte
     *
     * @return string
     */
    public function getCarte()
    {
        return $this->carte;
    }

    /**
     * Set avertissementMessage
     *
     * @param string $avertissementMessage
     *
     * @return Lot
     */
    public function setAvertissementMessage($avertissementMessage)
    {
        $this->avertissementMessage = $avertissementMessage;

        return $this;
    }

    /**
     * Get avertissementMessage
     *
     * @return string
     */
    public function getAvertissementMessage()
    {
        return $this->avertissementMessage;
    }

    /**
     * Set statistiques
     *
     * @param boolean $statistiques
     *
     * @return Lot
     */
    public function setStatistiques($statistiques)
    {
        $this->statistiques = $statistiques;

        return $this;
    }

    /**
     * Get statistiques
     *
     * @return bool
     */
    public function getStatistiques()
    {
        return $this->statistiques;
    }

    /**
     * Set visualiseur
     *
     * @param boolean $visualiseur
     *
     * @return Lot
     */
    public function setVisualiseur($visualiseur)
    {
        $this->visualiseur = $visualiseur;

        return $this;
    }

    /**
     * Get visualiseur
     *
     * @return bool
     */
    public function getVisualiseur()
    {
        return $this->visualiseur;
    }

    /**
     * Get theme Id
     * @VirtualProperty
     * @SerializedName("themeId")
     *
     * @return string
     */
    public function getThemeId()
    {
        return $this->getTheme() ? $this->getTheme()->getId() : null;
    }

    /**
     * Get Referentiel Id
     * @VirtualProperty
     * @SerializedName("referentielId")
     *
     * @return array
     */
    public function getReferentielId()
    {
        return $this->getReferentiel() ? $this->getReferentiel()->getId() : null;
    }

    /**
     * Get the value of contenusTiers
     *
     * @return  \Doctrine\Common\Collections\Collection
     */
    public function getContenusTiers()
    {
        return $this->contenusTiers;
    }

    /**
     * Get the value of coucheChamps
     *
     * @return  \Doctrine\Common\Collections\Collection
     */
    public function getCoucheChamps()
    {
        return $this->coucheChamps;
    }

    /**
     * Get the value of coucheJointures
     *
     * @return  \Doctrine\Common\Collections\Collection
     */
    public function getCoucheJointures()
    {
        return $this->coucheJointures;
    }


    /**
     * Get the value of dateMaj
     *
     * @return  \Doctrine\Common\Collections\Date
     */
    public function getDateMaj()
    {
        return $this->dateMaj;
    }

    /**
     * Set the value of dateMaj
     *
     * @param \Doctrine\Common\Collections\Date $dateMaj
     *
     * @return  self
     */
    public function setDateMaj($dateMaj)
    {
        $this->dateMaj = $dateMaj;

        return $this;
    }

    /**
     * Get the value of donnees
     *
     * @return  \Doctrine\Common\Collections\Collection
     */
    public function getDonnees()
    {
        return $this->donnees;
    }

    /**
     * Set the value of donnees
     *
     * @param \Doctrine\Common\Collections\Collection $donnees
     *
     * @return  self
     */
    public function setDonnees($donnees)
    {
        $this->donnees = $donnees;

        return $this;
    }

    /**
     * Get the value of statistic
     *
     * @return  string
     */
    public function getStatistic()
    {
        return $this->statistic;
    }

    /**
     * Set the value of statistic
     *
     * @param string $statistic
     *
     * @return  self
     */
    public function setStatistic($statistic)
    {
        $this->statistic = $statistic;

        return $this;
    }

    /**
     * Get the value of layerName
     *
     * @return  string
     */
    public function getLayerName()
    {
        return $this->layerName;
    }

    /**
     * Set the value of layerName
     *
     * @param string $layerName
     *
     * @return  self
     */
    public function setLayerName($layerName)
    {
        $this->layerName = $layerName;

        return $this;
    }

    /**
     * Get the value of buffer
     *
     * @return  string
     */
    public function getBuffer()
    {
        return $this->buffer;
    }

    /**
     * Set the value of buffer
     *
     * @param string $buffer
     *
     * @return  self
     */
    public function setBuffer($buffer)
    {
        $this->buffer = $buffer;

        return $this;
    }

    /**
     * Get the value of cuttingType
     *
     * @return  string
     */
    public function getCuttingType()
    {
        return $this->cuttingType;
    }

    /**
     * Set the value of cuttingType
     *
     * @param string $cuttingType
     *
     * @return  self
     */
    public function setCuttingType($cuttingType)
    {
        $this->cuttingType = $cuttingType;

        return $this;
    }

    /**
     * Get the value of displayType
     *
     * @return  string
     */
    public function getDisplayType()
    {
        return $this->displayType;
    }

    /**
     * Set the value of displayType
     *
     * @param string $displayType
     *
     * @return  self
     */
    public function setDisplayType($displayType)
    {
        $this->displayType = $displayType;

        return $this;
    }

    /**
     * Get the value of mapping
     *
     * @return  string
     */
    public function getMapping()
    {
        return $this->mapping;
    }

    /**
     * Set the value of mapping
     *
     * @param string $mapping
     *
     * @return  self
     */
    public function setMapping($mapping)
    {
        $this->mapping = $mapping;

        return $this;
    }

    /**
     * Get the value of mapping field
     *
     * @return  string
     */
    public function getMappingField()
    {
        return $this->mappingField;
    }

    /**
     * Set the value of mapping field
     *
     * @param string $mappingField
     *
     * @return  self
     */
    public function setMappingField($mappingField)
    {
        $this->mappingField = $mappingField;

        return $this;
    }
}