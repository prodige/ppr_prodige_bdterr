<?php

namespace App\Bdterr\BdcomBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Themes
 *
 * @ORM\Table(name="bdterr_themes", schema="bdterr")
 * @ORM\Entity(repositoryClass="App\Bdterr\BdcomBundle\Repository\ThemeRepository")
 */
class Theme
{
    /**
     *
     * @var int
     *
     * @ORM\Column(name="theme_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="bdterr.theme_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="theme_nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="theme_icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="theme_alias", type="text", nullable=true)
     */
    private $alias;

    /**
     * @var int
     *
     * @ORM\Column(name="theme_ordre", type="integer", nullable=true)
     */
    private $ordre;

    /**
     * @Exclude
     * @MaxDepth(2)
     *
     * @var int
     *
     * One Category has Many Categories.
     * @ORM\OneToMany(targetEntity="Theme", mappedBy="parent")
     * @ORM\OrderBy({"ordre" = "ASC"})
     */
    private $children;

    /**
     * @Exclude
     *
     * @var int
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="Theme", inversedBy="children")
     * @ORM\JoinColumn(name="theme_parent", referencedColumnName="theme_id")
     */
    private $parent;

    /**
     * @var Collection
     *
     * @Exclude
     *
     * @ORM\OneToMany(targetEntity="App\Bdterr\BdcomBundle\Entity\Lot", mappedBy="theme")
     *
     */
    private $lots;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Theme
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return Theme
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Themes
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set ordre
     *
     * @param integer $ordre
     *
     * @return Themes
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     *
     * @return Themes
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return int
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get the value of children
     *
     * @return  int
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set the value of children
     *
     * @param int $children
     *
     * @return  self
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get leaf (utilisé par les arbres extjs)
     * @VirtualProperty
     * @SerializedName("leaf")
     *
     * @return array
     */
    public function getLeaf()
    {
        return !is_null($this->getParent()) && $this->getParent()->getId() > 0;
    }

    /**
     * Si Vrai theme sinon c'est sous-thème
     * Get isTheme
     * @VirtualProperty
     * @SerializedName("isTheme")
     *
     * @return array
     */
    public function getIsTheme()
    {
        return !$this->getLeaf();
    }

    /**
     * Get ParentId
     * @VirtualProperty
     * @SerializedName("parentId")
     *
     * @return array
     */
    public function getParentId()
    {
        return $this->getParent() ? $this->getParent()->getId() : null;
    }

    /**
     * Get the value of lots
     *
     * @return  Collection
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * Set the value of id
     *
     * @param int $id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}

