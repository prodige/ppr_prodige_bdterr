<?php

namespace App\Bdterr\BdcomBundle\Services;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use DateTime;

class ExportOdt {

  public static function getOdtFromHtml($htmlContent, $plateforme){

    // Sauvegarde du html
    /** Conversion du html en pdf  */
    $cache = "upload/odt/";
   
    $objDateTime = new DateTime('NOW');
    $date = $objDateTime->format('dmY_His'); 

    $name = $cache."export_".$plateforme."_base_territoriale_$date";
    
    file_put_contents($name.".html", $htmlContent);
    
    $process = new Process("unoconv -f docx -d document -o $name.docx $name.html"); 
    $process->setTimeout(600);
    $process->run();
    
    // executes after the command finishes
    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
      return ""; 
    }

    $process = new Process("unoconv -f odt -d document -o $name.odt $name.docx"); // ajout template -t template.ott
    $process->setTimeout(600);
    $process->run();
  
    // executes after the command finishes
    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
      return ""; 
    }
    
    return  "/".$name.".odt";
  }
}

?>