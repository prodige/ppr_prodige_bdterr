<?php

namespace App\Bdterr\BdcomBundle\Services;

use App\Bdterr\BdcomBundle\Services\Html2pdf;

use DateTime;

class ExportPdf {

  public static function getPdfFromHtml($htmlcontent, $plateforme){

    // Sauvegarde du html
    /** Conversion du html en pdf  */
    //TODO: définir les url de cache
    $cache = "upload/pdf/";
   
    $objDateTime = new DateTime('NOW');
    $date = $objDateTime->format('dmY_His'); 

    $name = "export_".$plateforme."_base_territoriale_$date";

    $oPdf = new Html2pdf($cache."", $cache."", rand(0,10000)."_pdftmp.htm", $htmlcontent, $cache, $name, true);

    $oPdf->setExecuteByWkHtml(true);
   
    $oPdf->setParam('media' , "A4");
    $oPdf->setModeOutput("FILE");
    
    $oPdf->getPdf();

    return  "/".$cache.$name.".pdf";
  }
}

?>