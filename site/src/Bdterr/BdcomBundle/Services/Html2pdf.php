<?php

namespace App\Bdterr\BdcomBundle\Services;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
/**
 * Classe de base pour la génération de pdf par shell.
 *        Attention : le nom du fichier pdf est donné par le tag <title>XXX</title> => XXX.pdf
 * Pour le mode FILE, le fichier PDF est généré dans le cache ou strPathDest si non vide
 *                    l'attribut bSupprFichierSource n'est pris en compte qu'en mode FILE
 * Dans les autres modes, pas de fichier générer dans le cache ou strPathDest si non vide
 *                        il reste le fichier $strPathSource.$strFichierSource
 */
class Html2pdf
{
    /** Url du service, valeur par défaut :
     *  - ALK_SIALKE_URL."services/html2pdf/"
     *  - "http://html2pdf.alkante.com/"
     * en fonction du param bUseServLocal du constructeur
     */
    protected $strUrlService;

    /** taille mémoire allouée pour la génération en mode PHP CLI = 512M par défaut */
    protected $iMemoryLimit = "512M";

    /** Url de base du fichier source html */
    protected $strUrlSource;

    /** Emplacement du fichier source html */
    protected $strPathSource;

    /** Nom du fichier source html */
    protected $strFichierSource;

    /** Emplacement  du fichier PDF généré */
    protected $strPathDest;

    /** Nom du fichier html créé au moment de la génération, si =vide par défaut*/
    protected $strFichierDest;

    /** Contenu HTML à générer en PDF */
    protected $strHtml;

    /** vrai si suppression du fichier source avant et après le processus */
    protected $bSupprFichierSource;

    /** vrai si on utilise la génération pdf locale, faux si service web alkante */
    protected $bUseServLocal;

    /** tableau de configuration pour la génération du PDF */
    protected $tabConfig = array();

    /** =true si passage des paramètres en session, =false par défaut avec passage sur l'url */
    protected $bEncode;

    /** true pour un envoi des données sérialisé (sess=1), false pour un envoi des données en url (sess=0) */
    protected $bSendSerialized;

    /** true pour une execution en mode PHP CLI */
    protected $bExecuteByPHPCli;

    /** true pour une execution avec l'outil WkHtmlToPdf */
    protected $bExecuteByWkHtml;
    /**
     * @brief Constructeur de la classe Import : initialisation des attributs de Import.
     *
     * @param strUrlSource          Url complete du fichier html source
     * @param strPathSource         Chemin complet du fichier html source
     * @param strFichierSource      Nom du fichier html source, si vide, un fichier est créé à partir du paramètre strHtml dans le répertoire upload/cache/src_[yyyymmdd_hhmmss]_[uniqid].htm
     * @param strHtml               Contenu html
     * @param strPathDest           Chemin complet du fichier pdf générer, si vide prend la valeur du répertoire upload/cache
     * @param strFichierDest        Nom du fichier pdf générer, sans l'extension .pdf, si vide, prend la valeur [yyyymmdd_hhmmss]_[uniqid]
     * @param bSupprFichierSource   supprimer le fichier html source avant la génération (dans le cas ou le contenu est passé en paramètre), false par défaut
     */
    public function __construct($strUrlSource="", $strPathSource="", $strFichierSource="", $strHtml="",
    $strPathDest="", $strFichierDest="", $bSupprFichierSource=false)
    {


        $this->bSendSerialized = true;
        $this->bExecuteByPHPCli = false;
        $this->bExecuteByWkHtml = false;

        $this->strUrlService       = CARMEN_URL_SERVER_FRONT."/html2pdf_2.0.42/";
        $this->bUseServLocal       = true;

        $this->strUrlSource        = $strUrlSource;
        $this->strPathSource       = $strPathSource;



        $this->strFichierSource    = $strFichierSource;

        $this->strPathDest         = $strPathDest;
        $this->strFichierDest      = $strFichierDest;

        $this->strHtml             = $strHtml;

        $this->bSupprFichierSource = $bSupprFichierSource;

        $this->tabConfig =
        array('compress'                => "",
            'cssmedia'                => "screen",           // handheld, print, projection, screen, tty, tv
            'debugbox'                => "",                 // 1 ou vide
            'debugnoclip'             => "",                 // 1 ou vide : 1 disabled clipping
            'pageborder'              => "",                 // 1 ou vide
            'encoding'                => "utf-8",                 // utf-8, iso-8859-1..15, windows-1250..1252
            'html2xhtml'              => "",                 // vide ou 1 : transformation auto html -> xhtml
            'imagequality_workaround' => "",                 // vide ou 1 : Use PS2PDF image quality problem workaround
            'landscape'               => "",                 // 1 pour paysage, vide pour portrait
            'leftmargin'              => 10,                // en mm
            'rightmargin'             => 10,                // en mm
            'topmargin'               => 10,                // en mm
            'bottommargin'             => 10,                // en mm
            'media'                   => "A4",   // Letter, Legal, A0Oversize, A0..A5, B5, Folio, A6..A10, Screenshot640, Screenshot800,Screenshot1024
            'method'                  => "fpdf", // fastps, pdflib, fpdf, png
            'mode'                    => 'html',
            'output'                  => 1,      // 0=PDF will be opened in browser, 1=Browser download as file, 2=File on server
            'pixels'                  => 1280,    // largeur page en pixel : 640, 800, 1024, 1280
            'pdfversion'              => "1.4",  // 1.3=Reader 4, 1.4=Reader 5, 1.5=Reader 6
            'ps2pdf'                  => 0,
            'pslevel'                 => 3,  // 2 ou 3 dans le cas Postcript
            'renderfields'            => 1,  // vide ou 1
            'renderforms'             => "",  // vide ou 1
            'renderimages'            => 1,  // vide ou 1
            'renderlinks'             => 1,  // vide ou 1
            'scalepoints'             => 1,  // vide ou 1 : Keep screen pixel/point ratio
            'smartpagebreak'          => 1,  // vide ou 1 : Use "smart" pagebreaking algorith
            'transparency_workaround' => "",  // vide ou 1 : Use PS2PDF transparency problem workaround
            'headerhtml'              => "",
            'footerhtml'              => "",
            'watermarkhtml'           => "",
            'toc'                     => "",   // vide ou 1 : table des matières
            'toc-location'            => "before",  // before (1ere page), after (dernière page), placeholder

            'URL'                     => $this->strPathSource.$this->strFichierSource,
            'bHote'                   => "ALK",
            'fileName'                => $this->strFichierDest,
            'output_file_directory'   => $this->strPathDest,
        );
    }

    /**
     * Fournit la source html
     * @param strHtml   code html à transformer
     */
    public function setHtml($strHtml)
    {
        $this->strHtml = $strHtml;
    }

    /**
     * Fournit le répertoire source du fichier html à partir d'un chemin complet
     * Si le chemin n'existe pas, le chemin par défaut est fixé (.../upload/cache/)
     * Modifie en conséquence l'url source correspondant à ce répertoire
     * Retourne faux si le chemin fourni n'existe pas, vrai sinon
     * @param strPathBase  chemin complet du fichier source html
     * @param strFileName  nom du fichier source html, = vide par défaut pour créer un nom temporaire unique
     */
    public function setSrcBaseByPath($strPathBase, $strFileName="")
    {
        $this->strPathSource = ( $strPathBase != "" && file_exists($strPathBase) && is_dir($strPathBase)
            ? $strPathBase
            : ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD.ALK_UPLOAD_CACHE );
        $this->strUrlSource = str_replace(ALK_ALKANET_ROOT_PATH, ALK_ALKANET_ROOT_URL, $this->strUrlSource);
        $this->setSrcFileName($strFileName);
    }

    /**
     * Fournit l'url de base source du fichier html (url complète sans le nom de fichier avec slash en fin de chaîne)
     * Modifie si possible, le chemin source correspondant à cette url
     * @param strUrlBase    url de base du fichier source html
     * @param strFileName   nom du fichier source html, = vide par défaut pour créer un nom temporaire unique
     */
    public function setSrcBaseByUrl($strUrlBase, $strFileNameSrc="")
    {
        $this->strUrlSource = ( $strUrlBase != "" ? $strUrlBase : ALK_ALKANET_ROOT_URL.ALK_ROOT_UPLOAD.ALK_UPLOAD_CACHE );
        $this->setSrcFileName($strFileNameSrc);

        if( substr($this->strUrlSource, 0, strlen(ALK_ALKANET_ROOT_URL)) == ALK_ALKANET_ROOT_URL ) {
            $this->strPathSource = str_replace(ALK_ALKANET_ROOT_URL, ALK_ALKANET_ROOT_PATH, $this->strUrlSource);
        }
    }

    /**
     * Fournit le chemin de base et le nom de fichier (sans extension) de la destination pdf
     * @param strTargetPath       chemin de base du fichier destination pdf
     * @param strTargetFileName   nom du fichier destination pdf, = vide par défaut pour créer un nom temporaire unique
     */
    public function setTargetPathFileName($strTargetPath, $strTargetFileName="")
    {
        $this->strPathDest = ( $strTargetPath != "" && file_exists($strTargetPath) && is_dir($strTargetPath)
            ? $strTargetPath
            : ALK_ALKANET_ROOT_PATH.ALK_ROOT_UPLOAD.ALK_UPLOAD_CACHE );
        $this->setParam('output_file_directory', $this->strPathDest);
        $this->setTargetFileName($strTargetFileName);
    }

    /**
     * Fournit le nom du fichier source html
     * @param strFileName   nom du fichier source html, = vide par défaut pour créer un nom temporaire unique
     */
    public function setSrcFileName($strSrcFileName="")
    {
        $this->strFichierSource = ( $strSrcFileName != "" ? $strSrcFileName : "src_".$this->getUniqFileName().".htm" );
        $this->setParam('URL', $this->strUrlSource.$this->strFichierSource);
    }

    /**
     * Fournit le nom du fichier destination sans l'extension
     * @param strTargetFileName   nom du fichier destination pdf, = vide par défaut pour créer un nom temporaire unique
     */
    public function setTargetFileName($strTargetFileName="")
    {
        $this->strFichierDest = ( $strTargetFileName != "" ? $strTargetFileName : "dest_".$this->getUniqFileName() );
        $this->setParam('fileName', $this->strFichierDest);
    }

    /**
     * Change le mode d'envoi des paramètres :
     * @param bSendSerialized true=> sess=1&tab=[encodeParam(serialize(tabConfig))], false=sess=1&{key_tabConfig=value_tabConfig}
     */
    public function setSendSerializedMode($bSendSerialized)
    {
        $this->bSendSerialized = $bSendSerialized;
    }

    /**
     * Change le mode d'exécution de la génération
     * @param bExecuteByPHPCli true=> exécution en mode cli
     */
    public function setExecuteByPHPCli($bExecuteByPHPCli)
    {
        $this->bExecuteByPHPCli = $bExecuteByPHPCli;
        $this->bExecuteByWkHtml = ( $bExecuteByPHPCli ? false : $this->bExecuteByWkHtml );
        if ( !defined("ALK_CMD_PHP") ){
            trigger_error(__CLASS__."[line ".__LINE__."] : Veuillez définir la constante ALK_CMD_PHP donnant le chemin physique de l'exécutable php (exemple : /usr/bin/php)", E_USER_ERROR);
        }
    }

    /**
     * Change le mode d'exécution de la génération
     * @param bExecuteByWkHtml true=> exécution avec l'outil wkhtmltopdf
     */
    public function setExecuteByWkHtml($bExecuteByWkHtml)
    {
        $this->bExecuteByWkHtml = $bExecuteByWkHtml;
        $this->bExecuteByPHPCli = ( $bExecuteByWkHtml ? false : $this->bExecuteByPHPCli );
        if ( !defined("ALK_CMD_WKHTMLTOPDF") ){
            trigger_error(__CLASS__."[line ".__LINE__."] : Veuillez définir la constante ALK_CMD_WKHTMLTOPDF donnant le chemin physique de l'exécutable wkhtmltopdf (exemple : /usr/bin/wkhtmltopdf)", E_USER_ERROR);
        }
    }

    /**
     * Fixe la taille mémoire limite pour la génération en mode PHP-CLI
     * @param iMemoryLimit  taille mémoire (=512M par défaut)
     */
    public function setMemoryLimit($iMemoryLimit)
    {
        $this->iMemoryLimit = $iMemoryLimit;
    }

    /**
     * Retourne le chemin complet du fichier pdf généré
     * Si le fichier n'existe pas, retourne une chaine vide
     * @return string
     */
    public function getPathFileNamePDF()
    {
        $strPathFilePDF = $this->strPathDest.( substr($this->strPathDest, -1)!="/" ? "/" : "" ).$this->strFichierDest.".pdf";
        return ( file_exists($strPathFilePDF) && is_file($strPathFilePDF)
            ? $strPathFilePDF
            : "" );
    }

    /**
     * Modifie le paramétrage du pdf par réception de soumission (GET ou POST) ou décodage de token
     */
    public function requestParam()
    {
        foreach( $this->tabConfig as $strParamName=>$strDefaultValue) {
            if ( is_numeric($strDefaultValue) ) {
                $this->tabConfig[$strParamName] = AlkRequest::getToken($strParamName, AlkRequest::_REQUESTint($strParamName, $strDefaultValue));
            }
            else {
                $this->tabConfig[$strParamName] = AlkRequest::getToken($strParamName, AlkRequest::_REQUEST($strParamName, $strDefaultValue));
            }
        }
    }

    /**
     * Modifie le paramétrage du pdf
     * @param strParamName nom du paramètre pdf
     * @param strValue     valeur du paramètre
     */
    public function setParam($strParamName, $strValue)
    {
        $this->tabConfig[$strParamName] = $strValue;
    }

    /**
     * Fixe de type de sortie :
     *   BROWSERATTACHMENT = fichier PDF envoyé sur la sortie standard
     *   BROWSER           = Redirige vers le fichier PDF
     *   FILE              = fichier téléchargé plus tard
     *
     * @param strMode  Type de sortie = BROWSER (par défaut), BROWSERATTACHMENT ou FILE
     */
    public function setModeOutput($strMode="BROWSER")
    {
        switch ($strMode) {
          case "BROWSERATTACHMENT" : // fichier PDF envoyé sur la sortie standard
              $this->tabConfig['output'] = '1';
              break;

          case "FILE" : // fichier téléchargé plus tard
              $this->tabConfig['output'] = '2';
              break;

          case "BROWSER" : // Redirige vers le fichier PDF
          default:
              $this->tabConfig['output'] = '0';
              break;
        }
    }

    /**
     * @brief Fonction qui prépare puis exécute la génération du pdf
     *        Retourne 0 si ok, ko sinon
     *
     * @return string Retourne un entier
     */
    public function getPdf()
    {
        $baseTempFileName = $this->getUniqFileName();

        if( @file_exists($this->strPathDest.$this->strFichierDest) &&
        @is_file($this->strPathDest.$this->strFichierDest) ) {
            // suppression du fichier pdf si il existe
            @unlink($this->strPathDest.$this->strFichierDest);
        }


        if( $this->strHtml != "" ) {
            if( @file_exists($this->strPathSource.$this->strFichierSource) &&
            @is_file($this->strPathSource.$this->strFichierSource) ) {
                // suppression du fichier html source
                @unlink($this->strPathSource.$this->strFichierSource);
            }

            //génération d'un fichier html temporaire
            $this->strFichierSource = ( $this->strFichierSource != "" ? $this->strFichierSource : "src_".$baseTempFileName.".htm" );

            $hTemp = @fopen($this->strPathSource.$this->strFichierSource, "w");
            if( $hTemp ) {
                @fwrite($hTemp, $this->strHtml);
                @fclose($hTemp);
            }
        }
        $strRet = $this->createPdf();
        return $strRet;
    }

    /**
     * Retourne la configuration nécessaire à la génération avec wkhtml2pdf
     * Met à jour le tableau interne de configuration
     * Retourne dans une chaine, les paramètres de la ligne de commande système qui suffixera la commmande wkhtml2pdf
     * @return string
     */
    protected function getConfigWKHtmlToPdf()
    {
        $tabTraduction = array(
            'compress'                => array(
                "opt" => "disable-pdf-compression",
                "values" => array(""=>"_OFF_", "_ELSE_"=>"_ON_")
            ),
            'cssmedia'                => array(
                "opt" => "print-media-type",
                "values" => array("print" => "_ON_", "_ELSE_" => "_ON_")
            ),           // handheld, print, projection, screen, tty, tv
            'debugbox'                => null,                 // 1 ou vide
            'debugnoclip'             => null,                 // 1 ou vide : 1 disabled clipping
            'pageborder'              => null,                 // 1 ou vide
            'encoding'                => array(
                "opt" => "encoding",
                "values" => array("_ANY_"=>"_SAME_")
            ),                 // utf-8, iso-8859-1..15, windows-1250..1252
            'html2xhtml'              => null,                 // vide ou 1 : transformation auto html -> xhtml
            'imagequality_workaround' => null,                 // vide ou 1 : Use PS2PDF image quality problem workaround
            'landscape'               => array(
                "opt" => "orientation",
                "values" => array("1" => "Landscape", "" => "Portrait")
            ),                 // 1 pour paysage, vide pour portrait
            'leftmargin'              => array(
                "opt" => "margin-left",   "values" => array("_ANY_"=>"_SAME_")
            ),                // en mm
            'rightmargin'             => array(
                "opt" => "margin-right",  "values" => array("_ANY_"=>"_SAME_")
            ),                // en mm
            'topmargin'               => array(
                "opt" => "margin-top",    "values" => array("_ANY_"=>"_SAME_")
            ),                // en mm
            'bottommargin'            => array(
                "opt" => "margin-bottom", "values" => array("_ANY_"=>"_SAME_")
            ),                // en mm
            'media'                   => array(
                "opt" => "page-size", "values" => array("_ANY_"=>"_SAME_")
            ),   // Letter, Legal, A0Oversize, A0..A5, B5, Folio, A6..A10, Screenshot640, Screenshot800,Screenshot1024
            'method'                  => null, // fastps, pdflib, fpdf, png
            'mode'                    => null,
            'output'                  => array(
                "conf" => "output", "values" => array("_ANY_"=>"_SAME_")
            ),      // 0=PDF will be opened in browser, 1=Browser download as file, 2=File on server
            'pixels'                  => null,    // largeur page en pixel : 640, 800, 1024, 1280
            'pdfversion'              => null,  // 1.3=Reader 4, 1.4=Reader 5, 1.5=Reader 6
            'ps2pdf'                  => null,
            'pslevel'                 => null,  // 2 ou 3 dans le cas Postcript
            'renderfields'            => null,  // vide ou 1
            'renderforms'             => null,  // vide ou 1
            'renderimages'            => null,  // vide ou 1
            'renderlinks'             => null,  // vide ou 1
            'scalepoints'             => array(
                "opt" => "zoom",
                "values" => array("" => "_OFF_", "_ELSE_" => "_SAME_")
            ),  // vide ou 1 : Keep screen pixel/point ratio
            'smartpagebreak'          => null,  // vide ou 1 : Use "smart" pagebreaking algorith
            'transparency_workaround' => null,  // vide ou 1 : Use PS2PDF transparency problem workaround
            'headerhtml'              => array(
                "opt" => "header-center", "values" => array("_ANY_"=>"_SAME_")
            ),
            'footerhtml'              =>  array(
                "opt" => "footer-center", "values" => array("_ANY_"=>"_SAME_")
            ),
            'watermarkhtml'           => null,
            'toc'                     => array(
                "opt" => "toc",
                "values" => array("1" => "_ON_", "_ELSE_" => "_OFF_")
            ),   // vide ou 1 : table des matières
            'toc-location'            => null,  // before (1ere page), after (dernière page), placeholder
            'toc-title'            => array(
                "opt" => "toc-header-text", "values" => array("_ANY_"=>"_SAME_")
            ),  // before (1ere page), after (dernière page), placeholder

            'URL'                     => array(
                "arg" => "2", "values" => array("_ANY_"=>"_SAME_"), "after"=>" "
            ),
            'bHote'                   => null,
            'fileName'                => array(
                "arg" => "4", "values" => array("_ANY_"=>"_SAME_"), "after"=>".pdf"
            ),
            'output_file_directory'   => array(
                "arg" => "3", "values" => array("_ANY_"=>"_SAME_"), "after"=>""
            ),
        ) ;

        $tabOldConfig = $this->tabConfig;
        $tabNewConfig = array("opt"=>array(), "arg"=>array(), "conf"=>array());
        foreach ($tabOldConfig as $key=>$value){
            if ( !array_key_exists($key, $tabTraduction) ){
                $tabNewConfig["opt"][$key] = $value;
                continue;
            }

            $tabTrad = $tabTraduction[$key];
            if ( is_null($tabTrad) ) continue;

            $part = "opt";
            if ( array_key_exists("opt", $tabTrad)){
                $key = $tabTrad["opt"];
                $part = "opt";
            }
            else if ( array_key_exists("arg", $tabTrad)) {
                $part = "arg";
                $key = $tabTrad["arg"];
            }
            else if ( array_key_exists("conf", $tabTrad)) {
                $part = "conf";
                $key = $tabTrad["conf"];
            }
            $after = (array_key_exists("after", $tabTrad) ? $tabTrad["after"] : "");

            $tabValueUsed = array();
            foreach ($tabTrad["values"] as $value_input=>$value_output){
                if ( $value=="_OFF_" ) continue;
                switch( $value_output ){
                  case "_ON_" :
                      switch ($value_input){
                        case $value :
                        case "_ANY_" :
                            $tabValueUsed[] = $value_input;
                            $tabNewConfig[$part][$key] = "";
                            break;
                        case "_ELSE_" :
                            if ( !in_array($value, $tabValueUsed) )
                                $tabNewConfig[$part][$key] = "";
                            break;
                      }
                      break;

                  case "_OFF_" ://not added
                      $tabValueUsed[] = $value_input;
                      break;

                  case "_SAME_" :
                      switch ($value_input){
                        case $value :
                        case "_ANY_" :
                            $tabValueUsed[] = $value_input;
                            if  ( $value!="" )
                                $tabNewConfig[$part][$key] = $value.$after;
                            break;
                        case "_ELSE_" :
                            if ( !in_array($value, $tabValueUsed) && $value!="")
                                $tabNewConfig[$part][$key] = $value.$after;
                            break;
                      }
                      break;

                  default :
                      switch ($value_input){
                        case $value :
                        case "_ANY_" :
                            $tabValueUsed[] = $value_input;
                            $tabNewConfig[$part][$key] = $value_output.$after;
                            break;
                        case "_ELSE_" :
                            if ( !in_array($value, $tabValueUsed) )
                                $tabNewConfig[$part][$key] = $value_output.$after;
                            break;
                      }
                      break;
                }
            }
        }
        if ( array_key_exists("fileName", $this->tabConfig) && $this->tabConfig["fileName"]!="" ){
            $tabNewConfig["opt"]["title"] = preg_replace("!\.\w+$!", "", $this->tabConfig["fileName"]);
        }
        $tabNewConfig["opt"]["disable-smart-shrinking"] = "";

        $strArgv = "";
        foreach ($tabNewConfig["opt"] as $option=>$value){
            $strArgv .= " --".$option.($value!="" ? " \"".$value."\"" : "");
        }
        $strArgv .= " ";
        ksort($tabNewConfig["arg"]);
        foreach ($tabNewConfig["arg"] as $argument){
            $strArgv .= $argument;
        }
        
        $this->tabConfig = $tabNewConfig["conf"];
        return $strArgv;
    }

    /**
     * Lance l'exécution de la génération du pdf,
     * Evalue le mode output :  0=PDF will be opened in browser, 1=Browser download as file, 2=File on server
     * Retourne 0 si ok, ko sinon avec le message d'erreur
     * @return string
     */
    protected function createPdf()
    {
        $strRet = "0";
        $tabRes = array();
        if( $this->bExecuteByWkHtml || $this->bExecuteByPHPCli ) {
            $tabRes = ( $this->bExecuteByWkHtml
                ? $this->doCreatePDFByWKHtmlToPDF()
                : $this->doCreatePDFByServiceHtmlToPDF_PHPCLI() );

            $strPathFilePDF = $this->strPathDest."/".$this->strFichierDest.".pdf";
            $bRes = file_exists($strPathFilePDF) && is_file($strPathFilePDF);

            $strRet = implode("\r\n", $tabRes).
            ( $bRes
                ? "\r\nLe fichier ".$strPathFilePDF." a été généré"
                : "\r\nLe fichier ".$strPathFilePDF." n'a pas été généré".json_encode(file_exists($strPathFilePDF))." ".json_encode(is_file($strPathFilePDF)));

            if( !$bRes ) {
                return $strRet;
            }

            // fixe les droits sur le fichier pdf généré
            @chmod($strPathFilePDF, 0775);

            switch( $this->tabConfig['output'] ) {
              case '0': // Redirige vers le fichier PDF
                  $strUrlPDF = str_replace(ALK_ROOT_PATH, ALK_ROOT_URL, $strPathFilePDF);
                  $this->sendPDFByRedirect($strUrlPDF);
                  break;

              case '1': // Envoi sur la sortie standard, le contenu du PDF
                  $this->sendPDFOnOuput($strPathFilePDF);
                  break;

              case '2':// ne fait rien
                  break;
            }
        }
        else {
            $this->tabConfig["URL"] = $this->strUrlSource.$this->strFichierSource;
            $strParam = ( $this->bSendSerialized
                ? "sess=1&tab=".AlkRequest::getEncodeParam(serialize($this->tabConfig))
                : "sess=0&".http_build_query($this->tabConfig) );

            $strUrlServ = $this->strUrlService."alk_html2ps.php?".$strParam;

            switch( $this->tabConfig['output'] ) {
              case '0': // Redirige vers le fichier PDF
              case '1': // pas possible dans ce mode puisque ce sercice peut être externalisé au site actuel
                  $this->sendPDFByRedirect($strUrlServ);
                  break;

              case '2'://File on server
                  // appel le service de génération du pdf
                  $contents = @file_get_contents($strUrlServ);

                  // $contents contient  = "File saved as: <a href="file:///[chemin][nom du pdf]">nompdf</a>"
                  if( strpos($contents, "File saved as: ") === false ) {
                      $strRet = "1";
                  }
                  break;
            }
        }

        // suppression du fichier html temporaire
        if( $this->bSupprFichierSource &&
        @file_exists($this->strPathSource.$this->strFichierSource) &&
        @is_file($this->strPathSource.$this->strFichierSource) ) {
            @unlink($this->strPathSource.$this->strFichierSource);
        }

        return $strPathFilePDF;
    }

    /**
     * Lance la création du PDF avec WKHtml2Pdf
     * Retourne un tableau contenant la trace d'exécution sur la sortie standard
     * @return array
     */
    protected function doCreatePDFByWKHtmlToPDF()
    {
        $strArgv = $this->getConfigWKHtmlToPdf();
        $cmd_service_html2pdf = ALK_CMD_WKHTMLTOPDF.$strArgv;
        $tabRes = array();
        exec($cmd_service_html2pdf, $tabRes);
        
        return $tabRes;
    }

    /**
     * Lance la création du PDF avec le service Html2Pdf
     * Retourne un tableau contenant la trace d'exécution sur la sortie standard
     * @return array
     */
    protected function doCreatePDFByServiceHtmlToPDF_PHPCLI()
    {
        $this->strUrlService = str_replace(ALK_ALKANET_ROOT_URL, ALK_ALKANET_ROOT_PATH, $this->strUrlService);

        $strArgv = "";
        foreach( $this->tabConfig as $strParam => $valParam ) {
            $strParamValue = str_replace(ALK_ALKANET_ROOT_URL, "file://".ALK_ALKANET_ROOT_PATH, $valParam);
            if( !(strpos($strParamValue, " ") === false) ) {
                $strParamValue = "alktoken".AlkRequest::getEncodeParam($strParamValue);
            }
            $strArgv .= " ".$strParam."=".$strParamValue;
        }

        $cmd_service_html2pdf = ALK_CMD_PHP.
        " -d memory_limit=".$this->iMemoryLimit.
        " ".$this->strUrlService."alkcli_html2ps.php".$strArgv;
        
        
        $process_args = array("ALK_CMD_PHP", "-d","memory_limit=".$this->iMemoryLimit, $this->strUrlService."alkcli_html2ps.php" , $strArgv); 
        $process = new Process($process_args); 
        
        $tabRes = array();
        $process->run(); 
        $tabRes = explode (" ", $process->getOutput()); 
        //exec($cmd_service_html2pdf, $tabRes);

        return $tabRes;
    }

    /**
     * Redirige en javascript vers le fichier PDF si les entêtes ont déjà été envoyés
     * sinon effectue une redirection serveur si bServerRedirect=true
     * @param strUrlPDF   url du pdf généré
     * @param bServerRedirect =false par défaut, =true pour une redirection serveur si l'entête n'a pas déjà été envoyé
     */
    protected function sendPDFByRedirect($strUrlPDF, $bServerRedirect=false)
    {
        if( !$bServerRedirect || (headers_sent() && $bServerRedirect) ) {
            echo '<html><head><script type="text/javascript">'.
                ' function onLoadWindPdf() {'.
                '   this.document.location = "'.$strUrlPDF.'";'.
                ' } </script></head>'.
                '<body onload="onLoadWindPdf()"><!--a href="'.$strUrlPDF.'">go</a--></body></html>';
        } else {
            header("location: ".$strUrlPDF);
        }
    }

    /**
     * Envoi sur la sortie standard, le contenu du PDF
     * puis supprime le pdf ensuite
     * @param strPathFilePDF   chemin complet du pdf généré
     */
    protected function sendPDFOnOuput($strPathFilePDF)
    {
        AffHeaderFileDownload($strPathFilePDF, $this->strFichierDest.".pdf");
        readfile($strPathFilePDF);
        @unlink($strPathFilePDF);
    }

    /**
     * Retourne une base de nom de fichier temporaire
     * @return string
     */
    protected function getUniqFileName()
    {
        return date("Ymd_is")."_".uniqid();
    }

    /**
     * Echappe un nom de fichier
     * @param filename  nom du fichier à échapper
     * @return string
     */
    protected function filename_escape($filename)
    {
        return preg_replace("/[^a-z0-9-]/i", "_", $filename);
    }
}
