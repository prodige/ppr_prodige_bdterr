<?php

namespace App\Bdterr\BdcomBundle\Services;


use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Bdterr\BdcomBundle\Services\ExportOdt;
use App\Bdterr\BdcomBundle\Services\ExportPdf;

use DateTime;

class Export {

  /**
   * @param hml<string>     contenu htlml
   * @param format<string>  pdf ou  odt
   * @return string
   */
  public static function exportHtml($html, $format, $plateforme = ""){
    switch($format){
      case 'pdf': 
        return ExportPdf::getPdfFromHtml( $html, $plateforme );
      case 'odt':
        return ExportOdt::getOdtFromHtml( $html, $plateforme );
    }

    return "";
  }
}
?>