<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Doctrine\DBAL\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use App\Bdterr\BdcomBundle\Entity\ChampType;

use App\Bdterr\BdcomBundle\Utils\PostgresqlUtil;

/**
 * @Route("/base_territoriale_admin")
 */
class CatalogueController extends BdcomBaseController
{
    /**
     * @Head("/catalogue",  name="bdcom_api_catalogue", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     * Séries de données
     *
     * @Get("/catalogue/dataserie")
     *
     * @QueryParam(name="domain",   requirements="\d+", default="", nullable=true)
     * @QueryParam(name="subdomain", requirements="\d+", default="", nullable=true)
     * @QueryParam(name="rubrique",  requirements="\d+", default="", nullable=true)
     * @throws Exception|\Doctrine\DBAL\Driver\Exception
     */
    function getDataSerie(ParamFetcherInterface $paramFetcher): JsonResponse
    {
        $conn = $this->getCatalogueConnection("catalogue");
        $connProdige = $this->getProdigeConnection();

        $domain = intval($paramFetcher->get('domain'));
        $subdomain = intval($paramFetcher->get('subdomain'));
        $rubrique = intval($paramFetcher->get('rubrique'));

        $ids = array(-3, -2, -1, 0, 1, 2, 3);
        $params = $ids;


        $inQuery = implode(',', array_fill(0, count($ids), '?'));
        $sql = "SELECT distinct fmeta_id
          , xpath('//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, ".
            "('<?xml version=\"1.0\" encoding=\"utf-8\"?>'::text || data)::xml, ".
            "ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], ".
            "ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]])::text AS \"metadatatitle\"
          , couchd_type_stockage
          , couchd_emplacement_stockage
          , metadata.uuid as uuid
          , couchd_nom
          , couchd_description
          from couche_sdom 
          inner join public.metadata on (couche_sdom.fmeta_id::bigint = metadata.id) 
          inner join public.operationallowed opallow ON (metadata.id = opallow.metadataid AND opallow.operationid = 0 AND opallow.groupid = 1) 
         
          where couchd_type_stockage in (".$inQuery." ) and couchd_visualisable=1";

        // si on doit filtrer par sous-domaine, domaine ou rubrique
        $filterSql = null;
        if ($subdomain) {
            $filterSql = $subdomain;
        } else {
            if ($domain) {
                $filterSql = "(SELECT pk_sous_domaine FROM sous_domaine where ssdom_fk_domaine = ? )";
                $params[] = $domain;
            } else {
                if ($rubrique) {
                    $filterSql = "( SELECT pk_sous_domaine FROM sous_domaine WHERE ssdom_fk_domaine IN (SELECT pk_domaine FROM domaine WHERE dom_rubric= ? ) )";
                    $params[] = $rubrique;
                }
            }
        }

        if (!is_null($filterSql)) {
            $sql .= " and pk_sous_domaine IN (".$filterSql.")";
        }

        $sql .= " order by metadatatitle";

        $sth = $conn->prepare($sql);

        // bindValue is 1-indexed, so $k+1
        foreach ($params as $k => $id) {
            $sth->bindValue(($k + 1), $id);
        }

        $sth->execute();

        $data = $sth->fetchAll();
        $result = array();


        foreach ($data as $value) { // Renommage des champs + filtre sur les tables qui n'ont pas de colonne geom
            if (isset($value['couchd_emplacement_stockage']) && PostgresqlUtil::checkIfTableExist(
                    $connProdige,
                    $value['couchd_emplacement_stockage']
                )
                && PostgresqlUtil::checkOneColumnInTable(
                    $connProdige,
                    $value['couchd_emplacement_stockage'],
                    "the_geom"
                )) {
                $data = array();

                $data["title"] = str_replace(array("{", "\"", "}"), "", $value['metadatatitle']);
                $data["nom"] = $value['couchd_nom'];
                $data["uuid"] = $value['uuid'];
                $data["description"] = $value['couchd_description'];
                $data["table_postgis"] = $value['couchd_emplacement_stockage'];

                $result[] = $data;
            }
        }

        $result = array(
            'DataSerie' => $result,
            'succes' => true,
        );

        return new JsonResponse($result);
    }

    /**
     * Liste des Rubriques
     *
     * @Get("/catalogue/rubrique")
     * @throws \Doctrine\DBAL\Driver\Exception|Exception
     */
    private function getRubrique(): array
    {
        $conn = $this->getCatalogueConnection("catalogue");

        $sql = "select * from rubric_param";

        $sth = $conn->prepare($sql);
        $sth->execute();

        $result = $sth->fetchAll();
        $data = array();

        foreach ($result as $key => $value) {
            if (isset($value['rubric_id']) && isset($value['rubric_name'])) {
                $data[$key]['id'] = $value['rubric_id'];
                $data[$key]['name'] = $value['rubric_name'];
            }
        }


        return $data;
    }

    /**
     * Liste des domaines
     *
     * @Get("/catalogue/domaine")
     *
     * @QueryParam(name="rubrique_id", requirements="\w+", default="", nullable=true)
     * @throws \Doctrine\DBAL\Driver\Exception|Exception
     */
    private function getDomaine($rubriqueId): array
    {
        $conn = $this->getCatalogueConnection("catalogue");

        $sql = "select * from domaine ";

        $param = array();
        if ($rubriqueId) {
            $sql .= " where dom_rubric = :rubriqueId";
            $param = array('rubriqueId' => $rubriqueId);
        }

        $sth = $conn->prepare($sql);
        $sth->execute($param);

        $result = $sth->fetchAll();
        $data = array();

        foreach ($result as $key => $value) {
            if (isset($value['pk_domaine']) && isset($value['dom_nom'])) {
                $data[$key]['id'] = $value['pk_domaine'];
                $data[$key]['name'] = $value['dom_nom'];
            }
        }

        return $data;
    }

    /**
     * Liste des domaines
     *
     * @Get("/catalogue/sousdomaine")
     *
     * @QueryParam(name="domaine_id", requirements="\w+", default="", nullable=true)
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws Exception
     */
    private function getSousDomaine($domaineId): array
    {
        $conn = $this->getCatalogueConnection("catalogue");

        $sql = "select * from sous_domaine ";

        $param = array();
        if ($domaineId) {
            $sql .= " where ssdom_fk_domaine = :domaineId";
            $param = array('domaineId' => $domaineId);
        }

        $sth = $conn->prepare($sql);
        $sth->execute($param);

        $result = $sth->fetchAll();
        $data = array();

        foreach ($result as $key => $value) {
            if (isset($value['pk_sous_domaine']) && isset($value['ssdom_nom'])) {
                $data[$key]['id'] = $value['pk_sous_domaine'];
                $data[$key]['name'] = $value['ssdom_nom'];
            }
        }

        return $data;
    }

    private function getChampType(): array
    {
        $data = [];
        $entities = $this->em->getRepository(ChampType::class)->findAll();

        foreach ($entities as $key => $entity) {
            $data[$key]['id'] = $entity->getId();
            $data[$key]['name'] = $entity->getNom();
            $data[$key]['inContenuTier'] = $entity->getInContenuTier();
        }

        return $data;
    }

    /**
     * Récupère le contenu d'un dico
     *
     * dico valide : ['domaine', 'sous_domaine', 'rubrique']
     *
     * Filter_id
     *  => Dico domaine, filtre sur les rubriques
     *  => Dico sous-domaine, filtre sur les domaines
     *
     * @Get("/catalogue/dicos")
     *
     * @QueryParam(name="name", requirements="\w+", default="", nullable=false)
     * @QueryParam(name="filter_id", requirements="\d+", default="", nullable=true)
     */
    public function getDicoAction(ParamFetcherInterface $paramFetcher): JsonResponse
    {
        $valiDicoName = ['domaine', 'sous_domaine', 'rubrique', 'champ_type'];

        $name = $paramFetcher->get('name');
        $filterId = intval($paramFetcher->get('filter_id'));

        $result = array('succes' => false);
        $code = 404;

        try {
            if (in_array($name, $valiDicoName)) {
                $code = 200;
                switch ($name) {
                    case 'rubrique':
                        $result['Dico'] = $this->getRubrique();
                        break;
                    case 'domaine':
                        $result['Dico'] = $this->getDomaine($filterId);
                        break;
                    case 'sous_domaine':
                        $result['Dico'] = $this->getSousDomaine($filterId);
                        break;
                    case 'champ_type':
                        $result['Dico'] = $this->getChampType();
                        break;
                }
            }
        }catch (\Doctrine\DBAL\Driver\Exception|Exception $e) {
        }

        return new JsonResponse($result, $code);
    }

    /**
     *
     * @Get("/catalogue/profiles",)
     *
     * @IsGranted("ROLE_USER")
     *
     * @throws \Doctrine\DBAL\Driver\Exception|Exception
     */
    function getProfils(): JsonResponse
    {
        $conn = $this->getCatalogueConnection('catalogue');

        $sql = "select * from groupe_profil";

        $sth = $conn->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();

        $result = array();
        foreach ($data as $key => $value) {
            $result[$key]["id"] = $value['pk_groupe_profil'];
            $result[$key]["time"] = $value['ts'];
            $result[$key]["nom"] = $value['grp_id'];
            $result[$key]["alias"] = $value['grp_nom'];
            $result[$key]["description"] = $value['grp_description'];
            $result[$key]["isDefaultInstallation"] = $value['grp_is_default_installation'];
            $result[$key]["nomLdap"] = $value['grp_nom_ldap'];
        }

        $response = array(
            "Profile" => $result,
            "success" => !empty($result),
        );

        return new JsonResponse($response, !empty($result) ? 200 : 404);
    }
}
