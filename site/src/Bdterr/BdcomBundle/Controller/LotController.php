<?php

namespace App\Bdterr\BdcomBundle\Controller;


use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * @Route("/base_territoriale_admin",  options={"expose"=true})
 */
class LotController extends BdcomBaseController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Head("/lot",  name="bdcom_api_lot", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     * Permet d'avoir les thèmes racine (avec id => root en paramètre)
     * ou les sous-thèmes d'un thème
     *
     * @Get("/lot")
     *
     */
    function getAction(): JsonResponse
    {
        return $this->genericGetActionAll('Lot');
    }

    /**
     *
     * @Post("/lot" )
     *
     * @IsGranted("ROLE_USER")
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('Lot', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Patch("/lot/{id}", name="bdcom_edit_lot", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('Lot', $id, $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Delete("/lot/{id}", defaults={"id"=null} )
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function deleteAction($id = null): JsonResponse
    {
        $data = $this->genericDelete('Lot', $id);

        return new JsonResponse($data, $data['code']);
    }
}