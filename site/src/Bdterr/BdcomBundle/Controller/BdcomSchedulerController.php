<?php

namespace App\Bdterr\BdcomBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Prodige\ProdigeBundle\Controller\BaseController;

/**
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/base_territoriale_admin/scheduler")
 */
class BdcomSchedulerController extends BaseController
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->setPropertyNamingStrategy(
            new SerializedNameAnnotationStrategy(new IdenticalPropertyNamingStrategy())
        )->build();
    }

    protected function jsonSerializeEntity($entity, array $groups = null): string
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $context->enableMaxDepthChecks();
        if ($groups) {
            $context->setGroups($groups);
        }
        return $this->serializer->serialize($entity, 'json', $context);
    }

}