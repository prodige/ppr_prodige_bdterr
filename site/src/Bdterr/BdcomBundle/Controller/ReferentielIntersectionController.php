<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * @Route("/base_territoriale_admin",  defaults={}, options={"expose"=true})
 */
class ReferentielIntersectionController extends BdcomBaseController
{
    /**
     * @Get("/referentiel_intersection")
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/referentiel_intersection", name="bdcom_api_referentiel_intersection", options={"expose"=true})
     */
    function getAction(): JsonResponse
    {
        return $this->genericGetActionAll('ReferentielIntersection');
    }

    /**
     * @Post("/referentiel_intersection" )
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/referentiel_intersection", name="bdcom_api_referentiel_intersection", options={"expose"=true})
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('ReferentielIntersection', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Patch("/referentiel_intersection/{id}", defaults={"id"=null} )
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/referentiel_intersection/{id}", defaults={"id"=null}, name="bdcom_api_referentiel_intersection", options={"expose"=true})
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('ReferentielIntersection', $id, $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Delete("/referentiel_intersection/{id}", defaults={"id"=null} )
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/referentiel_intersection/{id}", defaults={"id"=null}, name="bdcom_api_referentiel_intersection", options={"expose"=true})
     */
    function deleteAction($id = null): JsonResponse
    {
        $data = $this->genericDelete('ReferentielIntersection', $id);

        return new JsonResponse($data, $data['code']);
    }
}