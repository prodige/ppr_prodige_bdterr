<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * @Route("/base_territoriale_admin",  options={"expose"=true})
 */
class RapportController extends BdcomBaseController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Head("/rapport",  name="bdcom_api_rapport", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     *
     * @Get("/rapport")
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function getAction(): JsonResponse
    {
        return $this->genericGetActionAll('Rapport');
    }

    /**
     *
     * @Post("/rapport" )
     *
     * @IsGranted("ROLE_USER")
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('Rapport', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Patch("/rapport/{id}", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('Rapport', $id, $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Delete("/rapport/{id}", defaults={"id"=null} )
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function deleteAction($id = null): JsonResponse
    {
        $data = $this->genericDelete('Rapport', $id);

        return new JsonResponse($data, $data['code']);
    }
}