<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Patch;

/**
 * @Route("/base_territoriale_admin",  defaults={}, options={"expose"=true})
 */
class ReferentielRechercheController extends BdcomBaseController
{

    /**
     * @Get("/referentiel_recherche")
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/referentiel_recherche", name="bdcom_api_referentiel_recherche", options={"expose"=true})
     */
    function getAction(): JsonResponse
    {
        return $this->genericGetActionAll('ReferentielRecherche');
    }

    /**
     *
     * @Patch("/referentiel_recherche/{id}", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/referentiel_recherche/{id}", defaults={"id"=null}, name="bdcom_api_referentiel_recherche", options={"expose"=true})
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('ReferentielRecherche', $id, $request);

        return new JsonResponse($data, $data['code']);
    }
}