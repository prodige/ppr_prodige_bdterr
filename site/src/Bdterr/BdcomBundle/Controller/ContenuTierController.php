<?php

namespace App\Bdterr\BdcomBundle\Controller;

use App\Bdterr\BdcomBundle\Entity\ContenuTier;
use App\Bdterr\BdcomBundle\Entity\Lot;
use App\Bdterr\BdcomBundle\Utils\MessageCode;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;

/**
 * @Route("/base_territoriale_admin")
 */
class ContenuTierController extends BdcomBaseController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Head("/contenuTier",  name="bdcom_api_contenu", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     *
     * @Get("/contenuTier")
     *
     * @QueryParam(name="idLot",   requirements="\d+", default="", nullable=true)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function getAction(ParamFetcherInterface $paramFetcher): JsonResponse
    {
        $idLot = intval($paramFetcher->get('idLot'));

        $data = array("success" => false, "ContenuTier" => array());

        if ($idLot) {
            $lots = $this->em->getRepository(Lot::class)->findById($idLot);

            if (count($lots)) {
                $lot = $lots[0];
                $entities = $this->em->getRepository(ContenuTier::class)->findByLot($lot);

                $data['ContenuTier'] = ($entities ? json_decode($this->jsonSerializeEntity($entities), true) : array());
                $data['success'] = true;
            }
        } else {
            return $this->genericGetActionAll('ContenuTier');
        }

        return new JsonResponse($data);
    }

    /**
     *
     * @Post("/contenuTier" )
     *
     * @IsGranted("ROLE_USER")
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('ContenuTier', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Patch("/contenuTier/{id}", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('ContenuTier', $id, $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Patch("/contenuTier/{id}/move", defaults={"id"=null})
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function moveAction(Request $request, $id = null): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $messageCode = MessageCode::OTHER_ERROR;

        $targetId = ($data['target_id'] ?? null);
        $method = ($data['method'] ?? null);

        // Mise à jour des données
        if (is_null($targetId) || is_null($method) || is_null($id)) {
            return new JsonResponse(array(is_null($targetId), is_null($method), is_null($id), 'success' => false), 500);
        }

        if ($method == 'after' || $method == 'before') {
            $messageCode = $this->em->getRepository(ContenuTier::class)->move($id, $targetId, $method);
        }


        $code = ($messageCode == MessageCode::UPDATED ? 200 : 500);

        return new JsonResponse(array('messageCode' => $messageCode, 'success' => $messageCode == MessageCode::UPDATED),
            $code);
    }

    /**
     *
     * @Delete("/contenuTier/{id}", defaults={"id"=null} )
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function deleteAction($id = null): JsonResponse
    {
        $data = $this->genericDelete('ContenuTier', $id);

        return new JsonResponse($data, $data['code']);
    }
}