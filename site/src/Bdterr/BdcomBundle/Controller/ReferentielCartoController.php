<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * @Route("/base_territoriale_admin",  defaults={}, options={"expose"=true})
 */
class ReferentielCartoController extends BdcomBaseController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Head("/referentiel_carto",  name="bdcom_api_referentiel_carto", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     * @Get("/referentiel_carto")
     *
     */
    function getAction(): JsonResponse
    {
        return $this->genericGetActionAll('ReferentielCarto');
    }

    /**
     * @Patch("/referentiel_carto/{id}", defaults={"id"=null},)
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('ReferentielCarto', $id, $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Post("/referentiel_carto" )
     *
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('ReferentielCarto', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Delete("/referentiel_carto/{id}", defaults={"id"=null} )
     *
     */
    function deleteAction($id = null): JsonResponse
    {
        $data = $this->genericDelete('ReferentielCarto', $id);

        return new JsonResponse($data, $data['code']);
    }
}