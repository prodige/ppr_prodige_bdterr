<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Doctrine\DBAL\Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\QueryParam;

/**
 * @Route("/base_territoriale_admin")
 */
class ProdigeController extends BdcomBaseController
{
    /**
     * @Get("/prodige/table")
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/prodige/table", defaults={}, name="bdcom_prodige_table", options={"expose"=true})
     * @throws \Doctrine\DBAL\Driver\Exception|Exception
     */
    function getTablesAction(): JsonResponse
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = " SELECT distinct couchd_nom || ' ('||couchd_emplacement_stockage||')' as nom, couchd_emplacement_stockage  
             from couche_sdom order by nom;";

        $sth = $conn->prepare($sql);
        $sth->execute();

        $result["ProdigeTable"] = $sth->fetchAll();
        $result["success"] = true;

        return new JsonResponse($result, 200);
    }

    /**
     * @Get("/prodige/column",)
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/prodige/column", name="bdcom_prodige_column", options={"expose"=true})
     *
     * @QueryParam(name="table",  description="Page of the overview.")
     * @throws \Doctrine\DBAL\Driver\Exception|Exception
     *
     */
    function getColmunAction(ParamFetcherInterface $paramFetcher): JsonResponse
    {
        $conn = $this->getProdigeConnection();
        $columns = array();
        $table = $paramFetcher->get('table');

        if (!is_null($table)) {
            $sql = "SELECT column_name FROM information_schema.columns 
      WHERE table_name = :table_name";

            $sth = $conn->prepare($sql);
            $sth->execute(array('table_name' => $table));

            $columns = $sth->fetchAll();
        }

        $result = array(
            "ProdigeColumn" => $columns,
            "success" => !empty($columns),
        );

        return new JsonResponse($result, !empty($columns) ? 200 : 404);
    }

    /**
     * @Get("/prodige/map",)
     *
     * @IsGranted("ROLE_USER")
     *
     * @Route("/prodige/map", name="bdcom_prodige_map", options={"expose"=true})
     * @throws \Doctrine\DBAL\Driver\Exception|Exception
     */
    function getMap(Request $request): JsonResponse
    {
        $conn = $this->getProdigeConnection('carmen');
        $sql = "SELECT  map_file as file, map_title as title, map_wmsmetadata_uuid as wmsmetadata_uuid  FROM carmen.map 
            WHERE published_id is null and map_model = false
            and map_file not like 'local_data/%' 
            and map_file not like 'layers/WMS/%'
            and map_file not like 'layers/TMP%'
            and map_file not like 'carteperso/%'
            and map_file not like 'wfs_%'
            and map_file not like 'wms_%'";

        $path = $request->getScheme().'://'.$request->getHttpHost().$request->getBasePath()."/";

        $sth = $conn->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();

        foreach ($data as $key => $value) {
            $data[$key]['url_file'] = $path.$value['file'].".map";
        }

        $result = array(
            "Map" => $data,
            "success" => !empty($data),
        );

        return new JsonResponse($result, !empty($data) ? 200 : 404);
    }

}
