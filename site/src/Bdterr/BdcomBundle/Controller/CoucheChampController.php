<?php

namespace App\Bdterr\BdcomBundle\Controller;

use App\Bdterr\BdcomBundle\Entity\CoucheChamp;
use App\Bdterr\BdcomBundle\Entity\Lot;
use App\Bdterr\BdcomBundle\Utils\MessageCode;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;


/**
 * @Route("/base_territoriale_admin")
 */
class CoucheChampController extends BdcomBaseController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Head("/coucheChamp",  name="bdcom_api_champs", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     *
     * @Get("/coucheChamp")
     *
     * @QueryParam(name="idLot",   requirements="\d+", default="", nullable=true)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function getAction(ParamFetcherInterface $paramFetcher): JsonResponse
    {
        $idLot = intval($paramFetcher->get('idLot'));

        $data = array("success" => false, "CoucheChamp" => array());

        if ($idLot) {
            $lots = $this->em->getRepository(Lot::class)->findById($idLot);

            if (count($lots)) {
                $lot = $lots[0];
                $entities = $this->em->getRepository(CoucheChamp::class)->findByLot($lot);

                if (count($entities) === 0) {
                    $entities = $this->em->getRepository(CoucheChamp::class)->createFromLot(
                        $lot,
                        $this->getProdigeConnection()
                    );
                }


                $data['CoucheChamp'] = ($entities ? json_decode($this->jsonSerializeEntity($entities), true) : array());
                $data['success'] = true;
            }
        } else {
            return $this->genericGetActionAll('CoucheChamp');
        }

        return new JsonResponse($data);
    }

    /**
     *
     * @Post("/coucheChamp" )
     *
     * @IsGranted("ROLE_USER")
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('CoucheChamp', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     *
     * @Patch("/coucheChamp/{id}/move", defaults={"id"=null})
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function moveAction(Request $request, $id = null): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $messageCode = MessageCode::OTHER_ERROR;

        $targetId = ($data['target_id'] ?? null);
        $method = ($data['method'] ?? null);

        // Mise à jour des données
        if (is_null($targetId) || is_null($method) || is_null($id)) {
            return new JsonResponse(array(is_null($targetId), is_null($method), is_null($id), 'success' => false), 500);
        }

        if ($method == 'after' || $method == 'before') {
            $messageCode = $this->em->getRepository(CoucheChamp::class)->move($id, $targetId, $method);
        }

        $code = ($messageCode == MessageCode::UPDATED ? 200 : 500);

        return new JsonResponse(array('messageCode' => $messageCode, 'success' => $messageCode == MessageCode::UPDATED),
            $code);
    }

    /**
     *
     * @Patch("/coucheChamp/{id}", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('CoucheChamp', $id, $request);

        return new JsonResponse($data, $data['code']);
    }
}