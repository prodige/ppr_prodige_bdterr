<?php

namespace App\Bdterr\BdcomBundle\Controller;

use App\Bdterr\BdcomBundle\Entity\Donnee;
use App\Bdterr\BdcomBundle\Entity\Lot;
use App\Bdterr\BdcomBundle\Entity\RapportsProfil;
use App\Bdterr\BdcomBundle\Entity\ReferentielCarto;
use App\Bdterr\BdcomBundle\Entity\ReferentielRecherche;
use App\Bdterr\BdcomBundle\Entity\Theme;
use App\Bdterr\BdcomBundle\Services\Export;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Filesystem\Filesystem;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Nelmio\ApiDocBundle\Annotation\Operation;
use OpenApi\Annotations as OA;

use Prodige\ProdigeBundle\Controller\User;

/**
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/base_territoriale")
 */
class ApiController extends BdcomBaseController
{
    /**
     * Liste les thèmes /sous-thèmes de la base communale
     *
     * #### Exemple
     *
     * GET /base_territoriale/themes
     * ```
     * [{
     *     id          : "1", //identifiant unique
     *     name        : "AGRICULTURE", //nom informatique
     *     text        : "Agriculture" // nom réel
     *     children    : [
     *      {
     *          id          : "2",
     *          name        : "AGRICULTURE-ENVIRONNEMENT",
     *          text        : "Agriculture-Environnement"
     *      },
     *      {
     *          id          : "3",
     *          name        : "AGRO-ALIMENTAIRE",
     *          text        : "Agro-Alimentaire"
     *      }
     *
     *     ]
     *
     *}]
     * ```
     *
     * @Get("/themes")
     *
     * @Operation(
     *   tags={"Config"},
     *   summary="Liste les thèmes sous-thèmes disponibles",
     *   @OA\Response(
     *       response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *       response="404",
     *      description="Ressource non trouvée"
     *   ),
     * )
     *
     * @return JsonResponse
     */
    public function themesAction(): JsonResponse
    {
        return $this->genericGetActionAll('Theme', true);
    }

    /**
     * Liste les territoires de la base communale
     * (Les noms des champs et le nombre de champs du format de retour dépendent de la configuration de la base communale)
     * #### Exemple
     *
     * GET /base_territoriale/territoires
     * ```
     *
     * [{
     *   "insee" : "44001",
     *   "insee_comm_deleguee" : null,
     *   "commune":"ABBARETZ",
     *   "code_epci" :"244400537",
     *   "nom_epci" : "CC de la Région de Nozay",
     *   "code_dept" : "44",
     *   "nom_dept" : "Loire-Atlantique",
     *   "code_scot" : "xxx",
     *   "nom_scot" : "SCoT du Pays de Châteaubriant"
     * },{
     *   "insee" : "44002",
     *   "insee_comm_deleguee" : null,
     *   "commune" : "AIGREFEUILLE-SUR-MAINE",
     *   "code_epci" : "244400446",
     *   "nom_epci" : "CC de la Vallée de Clisson",
     *   "code_dept" : "44",
     *   "nom_dept" : "Loire-Atlantique",
     *   "code_scot" : "yyyy",
     *   "nom_scot" : "SCot du Vignoble Nantais"
     * }]
     * ```
     *
     * @Get("/territoires")
     *
     * @Operation(
     *   tags={"Config"},
     *   summary="Liste les territoires configurés",
     *   @OA\Response(
     *       response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *       response="404",
     *      description="Ressource non trouvée"
     *   ),
     * )
     *
     * @return JsonResponse
     */
    public function territoiresAction(): JsonResponse
    {
        return $this->genericGetActionAll('ReferentielRecherche', true);
    }

    /**
     * Liste les résutats d'une recherche
     * #### Exemple
     * POST ?
     * GET /results?mode=geojson
     * ```
     *
     * [{
     *     nom_couche  : "Installations classées (SEVESO et autorisées)"
     *     nom_table  : "seveso"
     *     carto_url   : "https://carto.sigloire.fr/1/seveso.map?layer=Seveso",
     *     theme : 21,
     *     avertissement : "",
     *     objets : [
     *       {
     *         id : 452,
     *         carto_url : "https://carto.sigloire.fr/1/seveso.map?objectid=Seveso;gid;452",
     *         insee : "44001",
     *         insee_commune_deleguee : null,
     *         champs : [
     *            {
     *              nom : "siret",
     *              alias : "Numéro Siret",
     *              valeur : "32492746600023",
     *              type : "text"
     *            },
     *            {
     *              nom : "regime",
     *              alias : "Régime",
     *              valeur : "S",
     *              type : "text"
     *            }
     *         ],
     *         ressources : [
     *            {
     *              alias : "Consulter l'avis publié sur le site Internet de la DREAL",
     *              valeur : "http://www.pays-de-la-loire.developpement-durable.gouv.fr/vendee-a1999.html",
     *              type : "URL"
     *            },
     *            {
     *              alias : "Photographie",
     *              valeur : "http://www.pays-de-la-loire.developpement-durable.gouv.fr/images/452.jpeg",
     *              type : "Image"
     *            },
     *            {
     *              alias : "Vidéo",
     *              valeur : "https://catalogue.sigloire.fr/base_territoriale/data/44.mp4",
     *              type : "Video"
     *            }
     *          ],
     *          feature : {
     *              type: "Feature",
     *              geometry: {
     *                  "type": "Point",
     *                  "coordinates": [102.0, 0.5]
     *              }
     *
     * }
     *       }
     *     ]
     *  }
     *  ]
     * ```
     *
     *
     * @Post("/results")
     * @Get("/results")
     *
     * @RequestParam(name="mode",  description="mode d'affichages des résultats (json, geojson , odt, pdf)", nullable = true, default="json", requirements={"json|geojson|odt|pdf"})
     * @RequestParam(name="insee[]",  description="tableau de codes INSEE", nullable = true )
     * @RequestParam(name="themes[]", description="tableau des identifiants de thèmes filtrés", nullable = true )
     * @RequestParam(name="type",    description="string, type d'affichage", nullable = true, default="bt" )
     * @RequestParam(name="limit",    description="entier, nombre de lots de données maximal (pagination)", nullable = true, requirements="\d+", default="20" )
     * @RequestParam(name="offset",   description="entier, début du premier lot envoyé (pagination)", nullable = true, requirements="\d+", default="1" )
     *
     * @RequestParam(name="list", description="", nullable = true, default=false )
     * @RequestParam(name="ressource", description="", nullable = true, default=true )
     * @RequestParam(name="simplify", description="",  nullable = true, default=false )
     *
     * @Operation(
     *   tags={"Résultats"},
     *   summary="Résultats d'une recherche",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *      response="404",
     *      description="Ressource non trouvée"
     *   ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function resultsAction(Request $request): JsonResponse
    {
        $params = json_decode($request->getContent(), true);

        $params["uri"] = $this->getParameter("PRODIGE_URL_FRONTCARTO");
        $plateforme = $this->getParameter("NOM_PLATEFORME");

        $mode = $params["mode"] ?? "json";
        $type = $params["type"] ?? "bt";

        $simplify = $params["simplify"] ?? false;

        $url = "";

        // traitement des données pour les exports
        if ($mode == 'pdf' || $mode == 'odt') {
            $rapport = null;
            $user = User::GetUser();
            $groups = User::GetUserGroup($user->GetUserId());

            $params["mode"] = "pdf";

            // Récupération des données
            $communes = $this->em->getRepository(ReferentielRecherche::class)->findAllForApi(
                $this->getProdigeConnection(),
                $params
            );

            $response = $this->em->getRepository(Lot::class)->getLotByFilter($params);
            $themeIds = array();
            foreach ($response as $sousTheme => $values) {
                $themeIds[] = $sousTheme;
            }

            $themes = $this->em->getRepository(Theme::class)->findAllForApi($this->getProdigeConnection(), $themeIds);

            $style = array();

            if ($mode == 'odt') {
                $style = array(
                    "h2Color" => "#337ab7",
                );
            }


            $templateUrl = $this->getParameter("URL_EXPORT_TEMPLATE");
            $templateUrl = $templateUrl ?: 'BdcomBundle/export/';

            $profil = $this->em->getRepository(RapportsProfil::class)->getOneByProfilIds($groups);

            if ($profil) {
                $rapport = $profil->getRapport();
            }

            if ($rapport) {
                $fileSystem = new Filesystem();
                $file = ($mode == 'pdf') ? $rapport->getGabaritHtml() : $rapport->getGabaritOdt();

                if (!$fileSystem->exists($templateUrl.$file)) {
                    $file = ($mode == 'pdf') ? "template_pdf_$type.html.twig" : "template_odt_$type.html.twig";
                }

                $html = $this->renderView(
                    $file,
                    array(
                        'communes' => $communes,
                        'themes' => $themes,
                        'donnees' => $response,
                        'style' => $style,
                        'simplify' => $simplify,
                    )
                );

                // Export
                $path = Export::exportHtml($html, $mode, $plateforme);

                $url = $this->container->get('request_stack')->getCurrentRequest()->getUriForPath($path);
            }

            return new JsonResponse(array("url" => $url), 200);
        }

        $response = $this->em->getRepository(Lot::class)->getLotByFilter($params);

        return new JsonResponse($response, 200);
    }

    /**
     * Liste les résutats d'une recherche
     * #### Exemple
     *
     *
     * GET /fiche?id=154&table=seveso&mode=geojson
     * ```
     *
     * {
     *         id : 452,
     *         nom : "TITANOBEL",
     *         carto_url : "https://carto.sigloire.fr/1/seveso.map?objectid=Seveso;gid;452",
     *         communes : [
     *           {
     *            insee : "44001",
     *            commune : "ABBARETZ"
     *           },
     *           {
     *            insee : "44113",
     *            commune : "NOZAY"
     *           }
     *         ],
     *         champs : [
     *            {
     *              nom : "siret",
     *              alias : "Numéro Siret",
     *              valeur : "32492746600023",
     *              type : "text"
     *            },
     *            {
     *              nom : "regime",
     *              alias : "Régime",
     *              valeur : "S",
     *              type : "text"
     *            }
     *         ],
     *         ressources : [
     *            {
     *              alias : "Consulter l'avis publié sur le site Internet de la DREAL",
     *              valeur : "http://www.pays-de-la-loire.developpement-durable.gouv.fr/vendee-a1999.html",
     *              type : "URL"
     *            },
     *            {
     *              alias : "Photographie",
     *              valeur : "http://www.pays-de-la-loire.developpement-durable.gouv.fr/images/452.jpeg",
     *              type : "Image"
     *               },
     *            {
     *              alias : "Vidéo",
     *              valeur : "https://catalogue.sigloire.fr/base_territoriale/data/44.mp4",
     *              type : "Video"
     *            }
     *          ],
     *          stats : [
     *              stat1 : "valeur",
     *              stat2 : "valeur",
     *          ],
     *          feature : {
     *              type: "Feature",
     *              geometry: {
     *                  "type": "Point",
     *                  "coordinates": [102.0, 0.5]
     *              }
     *          }
     *}
     * ```
     *
     * @Get("/fiche")
     *
     *
     * @QueryParam(name="mode",     description="mode d'affichages des résultats (json, geojson)", default="json", requirements={"json|geojson|odt|pdf"}, nullable = true)
     * @QueryParam(name="id",       description="identifiant de l'objet", default="", nullable = false)
     * @QueryParam(name="nom_table",description="nom de table identifiant le lot de données", default="", nullable = false )
     *
     * @Operation(
     *   tags={"Résultats"},
     *   summary="Fiche objet",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *      response="404",
     *      description="Ressource non trouvée"
     *   ),
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @return JsonResponse
     */
    public function ficheAction(ParamFetcherInterface $paramFetcher): JsonResponse
    {
        $params["uri"] = $this->getParameter("PRODIGE_URL_FRONTCARTO");
        $params["mode"] = $paramFetcher->get('mode');
        $params["id"] = $paramFetcher->get('id');
        $params["table"] = $paramFetcher->get('nom_table');

        $response = $this->em->getRepository(Donnee::class)->getFiche($this->getProdigeConnection(), $params);


        return new JsonResponse($response, 200);
    }

    /**
     *
     * @Get("/statistic")
     *
     * @Operation(
     *   tags={"Résultats"},
     *   summary="Fiche objet",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *      response="404",
     *      description="Ressource non trouvée"
     *   ),
     * )
     *
     * @return JsonResponse
     */
    public function statisticAction(): JsonResponse
    {
        $response = $this->em->getRepository(Lot::class)->getStatistiqueForApi();

        return new JsonResponse($response, 200);
    }

    /**
     *
     * @Get("/couches/{nomTable}")
     *
     * @Operation(
     *   tags={"Résultats"},
     *   summary="Fiche objet",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *      response="404",
     *      description="Ressource non trouvée"
     *   ),
     * )
     *
     * @param $nomTable
     * @return JsonResponse
     */
    public function geographicalLayerAction($nomTable): JsonResponse
    {
        $response = $this->em->getRepository(ReferentielCarto::class)->getGeographicalLayer(
            $nomTable,
            $this->getProdigeConnection()
        );
        $code = empty($response) ? 404 : 200;

        return new JsonResponse($response, $code);
    }

    /**
     * specific route for docs
     * @Route("/data/{file}", name="base_territoriale_file", requirements={"file"=".+"}, options={"expose"=true})
     *
     * @return BinaryFileResponse|Response Object
     */
    public function readfileDataAccountMetadata($file)
    {
        $fileDoc = $this->getParameter("PRODIGE_PATH_DATA")."/base_territoriale/$file";
        if (file_exists($fileDoc)) {
            return new BinaryFileResponse($fileDoc);
        } else {
            return new Response(Response::$statusTexts[Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Authentification au service
     *
     * @Post("/login")
     *
     * @RequestParam(name="username",  description="login de l'utilisateur", nullable=false)
     * @RequestParam(name="password",  description="mot de passe de l'utilisateur", nullable = false)
     *
     * @Operation(
     *   tags={"Authentification"},
     *   summary="Connexion au service",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     *   @OA\Response(
     *      response="401",
     *      description="Echec de l'authentification"
     *   ),
     * )
     *
     * @param $username
     * @param $password
     * @return Response
     */
    public function loginAction($username, $password): Response
    {
        /*
          *
          S'authentification au CAS pour récupérer un TGT
          POST https://prodige4.alkante.com:8444/cas/v1/tickets
          Content-Type: application/x-www-form-urlencoded
          username=xxx
          password=xxx
          201 Created

          dans le header Location : <TGT-ID>

          Demander au CAS un ST pour le service démandé (url)
          POST https://prodige4.alkante.com:8444/cas/v1/tickets/<TGT-ID>
          Content-Type: application/x-www-form-urlencoded
          service=https://prodige4.alkante.com/...
          200 OK

          dans le body: <ST-ID>
          Appeler un premier service avec le ticket ST
          ANY https://prodige4.alkante.com/...?ticket=<ST-ID>
          Les autres appels seront connectés tant que la session est vivante
        *
          *
          */
        return new Response('Not impleted yet');
    }

    /**
     * Authentification au service
     *
     * @Get("/logout")
     *
     * @Operation(
     *   tags={"Authentification"},
     *   summary="Déconnexion au service",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     * )
     *
     * @return JsonResponse
     */
    public function logoutAction(): JsonResponse
    {
        /*
          *
        Se déconnecter
        DELETE https://prodige4.alkante.com:8444/cas/v1/tickets/<TGT-ID>
        => attention, ne tue pas la session du service appelé, donc on est surement toujours connecté au service
        3:25 PM
        Une fois connecté, pas besoin de se balader le ticket, il ne sert qu'à la première connexion
        Par contre le DELETE pour se déconnecter ne détruit pas la session PHP, donc on reste connecté tant qu'elle est valide même si le ticket a été detruit
        *
        *
        */
        return new JsonResponse('Not impleted yet');
    }

}
