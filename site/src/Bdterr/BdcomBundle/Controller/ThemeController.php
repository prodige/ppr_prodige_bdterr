<?php

namespace App\Bdterr\BdcomBundle\Controller;

use App\Bdterr\BdcomBundle\Entity\Theme;
use App\Bdterr\BdcomBundle\Utils\MessageCode;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;

/**
 * @Route("/base_territoriale_admin",  options={"expose"=true})
 */
class ThemeController extends BdcomBaseController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Head("/theme_tree",  name="bdcom_api_theme", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     * Permet d'avoir les thèmes racine (avec id => root en paramètre )
     * ou les sous-thèmes d'un thème
     *
     * @Get("/theme_tree/{id}")
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function getAction($id = null): JsonResponse
    {
        $id = ($id == "root" ? 0 : $id);
        if (!is_null($id)) {
            $entity = $this->em->getRepository(Theme::class)->findOneById($id);
            $entities = ($entity && $entity->getChildren() ? $entity->getChildren() : null);
        } else {
            $entities = $this->em->getRepository(Theme::class)->findAll();
        }

        $data["Theme"] = ($entities ? json_decode($this->jsonSerializeEntity($entities), true) : array());

        return new JsonResponse(array_merge($data, array("success" => "true")), 200);
    }

    /**
     * @Post("/theme_tree" )
     *
     * @IsGranted("ROLE_USER")
     */
    function postAction(Request $request): JsonResponse
    {
        $data = $this->genericCreate('Theme', $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Patch("/theme_tree/{id}", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $data = $this->genericUpdate('Theme', $id, $request);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Patch("/theme_tree/{id}/move", defaults={"id"=null})
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function moveAction(Request $request, $id = null): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $messageCode = MessageCode::OTHER_ERROR;

        $targetId = ($data['target_id'] ?? null);
        $method = ($data['method'] ?? null);

        // Mise à jour des données
        if (is_null($targetId) || is_null($method) || is_null($id)) {
            return new JsonResponse(array(is_null($targetId), is_null($method), is_null($id), 'success' => false), 500);
        }

        if ($method == 'after' || $method == 'before') {
            $messageCode = $this->em->getRepository(Theme::class)->move($id, $targetId, $method);
        } else {
            if ($method == 'append') {
                $messageCode = $this->em->getRepository(Theme::class)->appendToTheme($id, $targetId, true);
            }
        }

        $code = ($messageCode == MessageCode::UPDATED ? 200 : 500);

        return new JsonResponse(array('messageCode' => $messageCode, 'success' => $messageCode == MessageCode::UPDATED),
            $code);
    }

    /**
     *
     * @Delete("/theme_tree/{id}", defaults={"id"=null} )
     *
     */
    function deleteAction($id = null): JsonResponse
    {
        $data = $this->genericDelete('Theme', $id);

        return new JsonResponse($data, $data['code']);
    }

    /**
     * @Post("/theme_tree/generate")
     *
     */
    function generateThemeFromCsv(): JsonResponse
    {
        $path = $this->container->getParameter('kernel.root_dir').'/../web/bundles/bdcom/themes/themes.csv';

        $data = $this->readCsvFile($path);
        $this->em->getRepository(Theme::class)->addThemesFromArray($data);

        return new JsonResponse($data, 200);
    }

    /**
     * Read CSV File
     */
    private function readCsvFile($file): array
    {
        $themes = array();

        if (($handle = fopen($file, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                if (count($data) >= 2) {
                    $themes[$data[0]] = $themes[$data[0]] ?? array();
                    $themes[$data[0]][] = $data[1];
                }
            }
            fclose($handle);
        }

        return $themes;
    }

}