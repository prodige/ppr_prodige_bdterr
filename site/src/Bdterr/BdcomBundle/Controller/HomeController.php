<?php

namespace App\Bdterr\BdcomBundle\Controller;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;

use FOS\RestBundle\Controller\Annotations\Get;

/**
 * @Route("/base_territoriale_admin")
 */
class HomeController extends BdcomBaseController
{

    /**
     * @throws Exception
     */
    public function load($container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('menu.yml'); # another file of yours
    }

    /**
     *
     * @Get("/app.js")
     *
     * @Route("/app.js", name="bdcom_app.js", options={"expose"=true})
     * @throws Exception
     */
    public function getAppAction(): Response
    {
        $container = new ContainerBuilder();
        $this->load($container);

        $menus = $container->getParameter("sitemenus");
        $casloginUrl = "";

        $twigParams = array(
            'menus' => json_encode($menus),
            'cas_url' => $casloginUrl,
        );

        $rendered = $this->renderView('BdcomBundle/default/app.js.twig', $twigParams);
        $response = new Response($rendered);
        $response->headers->set('Content-Type', 'text/javascript');

        return $response;
    }

    /**
     * Affichage de la page d'accueil de BdcomBundle
     *
     * @Get("/")
     *
     * @Route("/", name="home")
     * @throws Exception
     */
    public function indexAction(): Response
    {
        $container = new ContainerBuilder();
        $this->load($container);

        $menus = $container->getParameter("sitemenus");

        $parameters = array(
            "user" => ($this->getUser() ? $this->getUser() : null),
            "sitemenus" => $menus,
        );

        return $this->render('BdcomBundle/default/index.html.twig', $parameters);
    }
}
