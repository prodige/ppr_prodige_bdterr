<?php

namespace App\Bdterr\BdcomBundle\Controller;

use App\Bdterr\BdcomBundle\Utils\MessageCode;

use Doctrine\Persistence\ManagerRegistry;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;

use Prodige\ProdigeBundle\Controller\BaseController;

/**
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/base_territoriale")
 */
class BdcomBaseController extends BaseController
{
    private $serializer;
    protected $em;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->em = $managerRegistry;
        $this->serializer = SerializerBuilder::create()->setPropertyNamingStrategy(
            new SerializedNameAnnotationStrategy(new IdenticalPropertyNamingStrategy())
        )->build();
    }

    /**
     * @Get("/debug/getall")
     * @IsGranted("ROLE_USER")
     */
    public function getAllDebug(): JsonResponse
    {
        $allThemes = array();

        $entities = [
            'Theme',
            'ChampType',
            'ContenuTier',
            'CoucheChamp',
            'Lot',
            'Rapport',
            'Donnee',
            'RapportsProfil',
            'ReferentielIntersection',
            'ReferentielRecherche',
        ];

        foreach ($entities as $entityName) {
            $allThemes[$entityName] = $this->em->getRepository(
                'App\Bdterr\BdcomBundle\Entity\\'.$entityName
            )->findAllInArray();
        }

        return new JsonResponse(array_merge($allThemes, array("success" => true)), 200);
    }

    /**
     * Retourne toutes les données d'une entité
     */
    public function genericGetActionAll($entityName, $forApi = false): JsonResponse
    {
        $response = array("success" => false);

        if (class_exists('App\Bdterr\BdcomBundle\Entity\\'.$entityName)) {
            if ($forApi) {
                $params = null;

                $response = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->findAllForApi(
                    $this->getProdigeConnection(),
                    $params
                );

                return new JsonResponse($response, 200);
            } else {
                $entities = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->findAll();

                $response[$entityName] = ($entities ? json_decode(
                    $this->jsonSerializeEntity($entities),
                    true
                ) : array());

                return new JsonResponse(array_merge($response, array("success" => true)), 200);
            }
        }

        return new JsonResponse($response, 404);
    }

    /**
     *
     */
    public function genericUpdate($entityName, $id, Request $request): array
    {
        $data = json_decode($request->getContent(), true);
        $conn = $this->getProdigeConnection('carmen');

        $code = 500;
        $success = false;

        $message = 'Problème pendant la mise à jour.';

        $messageCode = MessageCode::OTHER_ERROR;

        if (!is_null($id) && !empty($data) && class_exists('App\Bdterr\BdcomBundle\Entity\\'.$entityName)) {
            if (isset($data[0]) && is_array($data[0])) {
                $messageCode = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->updates($data);
            } else {
                $messageCode = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->update(
                    $id,
                    $data,
                    $conn
                );
            }
        }

        if ($messageCode === MessageCode::UPDATED) {
            $code = 200;
            $message = 'Mise à jour des données réussie.';
            $success = true;
        }

        return array(
            'messageCode' => $messageCode,
            'code' => $code,
            'message' => $message,
            'success' => $success,
        );
    }

    /**
     *
     */
    public function genericCreate($entityName, Request $request): array
    {
        $data = json_decode($request->getContent(), true);
        $code = 500;
        $success = false;
        $message = "Problème pendant la création";

        $messageCode = MessageCode::OTHER_ERROR;

        $isOk = false;
        if (!empty($data) && class_exists('App\Bdterr\BdcomBundle\Entity\\'.$entityName)) {
            if (isset($data[0]) && is_array($data[0])) {
                $messageCode = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->creates(
                    $data,
                    $this->getProdigeConnection()
                );

                $isOk = true;
                foreach ($messageCode as $value) {
                    $isOk = $isOk && ($value === MessageCode::CREATED);
                }
            } else {
                $messageCode = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->create(
                    $data,
                    $this->getProdigeConnection()
                );
                $isOk = ($messageCode === MessageCode::CREATED);
            }
        }

        if ($isOk) {
            $code = 201;
            $message = 'Création réussie';
            $success = true;
        }

        return array(
            'messageCode' => $messageCode,
            'code' => $code,
            'message' => $message,
            'success' => $success,
        );
    }

    /**
     *
     */
    public function genericDelete($entityName, $id): array
    {
        $entity = null;
        $code = 500;
        $success = false;
        $isOk = false;
        $message = "Problème pour suprimer l'élément.";

        $messageCode = MessageCode::OTHER_ERROR;

        if (!is_null($id) && class_exists('App\Bdterr\BdcomBundle\Entity\\'.$entityName)) {
            if (isset($data[0]) && is_array($data[0])) {
                $messageCode = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->deletes(
                    $data,
                    $entity,
                    $this->container
                );

                $isOk = true;
                foreach ($messageCode as $value) {
                    $isOk = $isOk && ($value === MessageCode::DELETED);
                }
            } else {
                $messageCode = $this->em->getRepository('App\Bdterr\BdcomBundle\Entity\\'.$entityName)->delete(
                    $id,
                    $entity,
                    $this->container
                );
                $isOk = ($messageCode === MessageCode::DELETED);
            }
        }

        if ($isOk) {
            $code = 200;
            $message = "L'enregistrement à bien été supprimé.";
            $success = true;
        }

        return array(
            'messageCode' => $messageCode,
            'code' => $messageCode === MessageCode::EXIST_NOT ? 404 : $code,
            'message' => $message,
            'success' => $success,
        );
    }

    /**
     * Serialize an entity as a json string.
     *
     * @see http://jmsyst.com/libs/serializer/master/cookbook/exclusion_strategies
     *
     * @param array|Object $entity The entity to serialize
     * @param array|null $groups A list of serialization groups (see link above)
     *
     * @return string The entity formatted as a json string
     */
    protected function jsonSerializeEntity($entity, array $groups = null): string
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);
        $context->enableMaxDepthChecks();
        if ($groups) {
            $context->setGroups($groups);
        }

        return $this->serializer->serialize($entity, 'json', $context);
    }
}