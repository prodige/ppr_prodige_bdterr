<?php

namespace App\Bdterr\BdcomBundle\Repository;


use App\Bdterr\BdcomBundle\Repository\BaseRepository;
use App\Bdterr\BdcomBundle\Entity\ReferentielCarto;
use App\Bdterr\BdcomBundle\Utils\MessageCode;

use App\Bdterr\BdcomBundle\Entity\Donnee;
use Doctrine\DBAL\DBALException;
use App\Bdterr\BdcomBundle\Utils\PostgresqlUtil;
/**
 * ReferentielCartoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReferentielCartoRepository extends BaseRepository
{
    /**
   * 
   */
  public function update($id, $data) {
   
    // $this->mergeData($data);

    $messageCode = MessageCode::EXCEPTION_ERROR;

    if ($id) {
      $entity = $this->findOneById($id);
    }

    if ($entity) {
      if(isset($data['referentielTable'])){
        $entity->setReferentielTable($data['referentielTable']);
      }

      if(isset($data['nom'])){
        $entity->setNom($data['nom']);
      }
      
      if(isset($data['apiChamps'])){
        $entity->setApiChamps( $data['apiChamps'] );
      }

      $execption = $this->persistEntity($entity);

      return is_null($execption) ? MessageCode::UPDATED  : MessageCode::EXCEPTION_ERROR;
    }

    return MessageCode::EXIST_NOT;
  }
 
   /**
    * 
    */
  public function create($data, $prodigeConn = null){
    $em = $this->getEntityManager();
    // $this->mergeData($data);
  
    $entity = new ReferentielCarto();
  
    if(isset($data['referentielTable'])){
      $entity->setReferentielTable($data['referentielTable']);
    }

    if(isset($data['nom'])){
      $entity->setNom($data['nom']);
    }

    if(isset($data['apiChamps'])){
      $entity->setApiChamps( $data['apiChamps'] );
    }
    
    $execption = $this->persistEntity($entity);

    return is_null($execption) ? MessageCode::CREATED  : MessageCode::EXCEPTION_ERROR ;
 
  }
 
  public function delete($id) {
    $em = $this->getEntityManager();

    if( !is_null($id) ){
      $entity = $this->find($id);
    }

    if ($entity) {
      $messageCode = MessageCode::EXCEPTION_ERROR;

      try{
        $em->remove($entity);
        $em->flush();

        $messageCode = MessageCode::DELETED;
      }
      catch(Exception $e){
        $messageCode = MessageCode::EXCEPTION_ERROR;
      }
      return $messageCode;
    }
    return MessageCode::EXIST_NOT;
  }

  public function getGeographicalLayer($nomTable, $prodigeConn){
    $result = array();

    $checkTable = false;
    $checkGeom = false;
    
    $entity =  !is_null($nomTable) ? $this->findOneByReferentielTable($nomTable) : null;

    if($entity){
      // Vérification table et colonne
      $checkTable = $this->checkIfTableExist($prodigeConn, $entity->getReferentielTable());

      $apiChamps = $entity->getApiChamps();
      $apiChamps =  !is_null($apiChamps) && !empty($apiChamps) ? $apiChamps : array();
      $apiChamps[] = "the_geom";
    
      // on vérifie si les colonne existe
      $columns  =  $this->getColumnInTableWithFilter($prodigeConn, $entity->getReferentielTable(),$apiChamps );

      $columnSql = array();

      if($columns){
        // mise en forme sql pour les champs commmun 
        foreach($columns as $key => $column){
          if( $column !== "the_geom" ){
            $columnSql[] = $column;
          }
        }
        $data['message'] = 'column not found';
     
        if(!empty($columnSql) && in_array("the_geom", $columns  ) ){
          $sql = "SELECT ".implode(",", $columnSql).", ST_AsGeojson(st_transform(st_simplify(the_geom, 100), 3857)) as geojson  from ".  $entity->getReferentielTable() ."";

          $sth = $prodigeConn->prepare($sql);
          $sth->execute();

          $datas = $sth->fetchAll();
          
          $result = array(
            "type" =>  "FeatureCollection",
            "crs" =>  array(
              "type" => "name",
              "properties" => array( 
                "name" => "urn:ogc:def:crs:EPSG::3857" 
              )
            ),
            "features" => array()
          );
          
          foreach($datas as $key => $data){
            if(isset($data['geojson'])){
              $properties = array();

              foreach($columns as $key2 => $column){
                if($column !== "the_geom"  && isset($data[$column]) ){
                  $properties[$column] = $data[$column];
                }
              }
  
              $result["features"][] = array(
                "type" => "Feature",
                "properties" => $properties,
                'geometry' =>  json_decode($data['geojson'])
              );
            }
            
          }
        }
      }
    }

    
    return $result;
  }
}
