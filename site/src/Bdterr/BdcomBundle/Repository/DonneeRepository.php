<?php

namespace App\Bdterr\BdcomBundle\Repository;

use App\Bdterr\BdcomBundle\Entity\CoucheChamp;
use App\Bdterr\BdcomBundle\Entity\Lot;
use App\Bdterr\BdcomBundle\Entity\ReferentielIntersection;
use App\Bdterr\BdcomBundle\Entity\ReferentielRecherche;
use App\Bdterr\BdcomBundle\Repository\BaseRepository;
use App\Bdterr\BdcomBundle\Entity\Donnee;
use Doctrine\DBAL\DBALException;
use App\Bdterr\BdcomBundle\Utils\PostgresqlUtil;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Tools\Pagination\Paginator;

use PDO;
use Psr\Container\ContainerInterface;

/**
 * DonneeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DonneeRepository extends BaseRepository
{
    /** */
    private function getNameByInsees($prodigeConn, $insees)
    {
        $responses = array();

        $refRepo = $this->getEntityManager()->getRepository(ReferentielRecherche::class);
        $entities = $refRepo->findAll();

        if (!empty($insees) && count($entities) > 0) {
            $refEntity = $entities[0];

            // vérification de table et collonnes
            $checkTable = PostgresqlUtil::checkIfTableExist($prodigeConn, $refEntity->getReferentielTable());

            $checkColmuns = $checkTable && PostgresqlUtil::checkColumnsInTable(
                    $prodigeConn, $refEntity->getReferentielTable(),
                    array($refEntity->getCommune(), $refEntity->getInsee())
                );

            if ($checkTable && $checkColmuns) {
                // préparation parameter
                $placeholders = str_repeat('?, ', count($insees) - 1).'?';

                // selection des commune
                $sql = "SELECT distinct ".$refEntity->getInsee()." as insee,".$refEntity->getCommune()." as commune  
                FROM ".$refEntity->getReferentielTable()." where ".$refEntity->getInsee()." in ($placeholders)";

                $sth = $prodigeConn->prepare($sql);
                $sth->execute($insees);

                $responses = $sth->fetchAll();
            }
        }

        // Vérification table et colonne
        return $responses;
    }

    /**
     * Création de l'entity
     */
    public function updateOne($entity, $data, $withFlush = true)
    {
        $em = $this->getEntityManager();

        // Nom
        if (isset($data['nom'])) {
            $entity->setObjetNom($data['nom']);
        }

        // Fiche
        if (isset($data['fiche'])) {
            $entity->setAttributsFiche($data['fiche']);
        }

        // Resultat
        if (isset($data['resultat'])) {
            $entity->setAttributsResultat($data['resultat']);
        }

        // Ressource resultat
        if (isset($data['resultat_ress'])) {
            $entity->setRessourcesResultat($data['resultat_ress']);
        }

        // Ressource Fiche
        if (isset($data['fiche_ress'])) {
            $entity->setRessourcesFiche($data['fiche_ress']);
        }

        // Insee
        if (isset($data['insee'])) {
            $entity->setInsee($data['insee']);
        }

        // Insee délégué
        if (isset($data['inseeDelegue'])) {
            $entity->setInseeDelegue($data['inseeDelegue']);
        }

        $em->persist($entity);

        $exception = null;

        if ($withFlush) {
            try {
                //On enregistre dans la base
                $em->flush();
            } catch (Exception $e) {
                $exception = $e;
            }

            return is_null($exception) ? true : "Exception error : ".$exception;
        }

        return true;
    }

    /**
     * Création de l'entity
     */
    public function createOne($data, $withFlush = true)
    {
        $em = $this->getEntityManager();

        $entity = new Donnee();

        $entity->setObjetId($data['id']);

        // Lot
        if (isset($data['lot'])) {
            $lotRepo = $this->getEntityManager()->getRepository(Lot::class);

            $lot = $lotRepo->find($data['lot']);

            if ($lot) {
                $entity->setLot($lot);
            } else {
                return "Lot non trouvé ";
            }
        } else {
            return "ERROR : lot IS NULL";
        }

        // Nom
        if (isset($data['nom'])) {
            $entity->setObjetNom($data['nom']);
        } else {
            return "ERROR : nom IS NULL";
        }

        // Fiche
        if (isset($data['fiche'])) {
            $entity->setAttributsFiche($data['fiche']);
        } else {
            return "ERROR : fiche IS NULL";
        }

        // Resultat
        if (isset($data['resultat'])) {
            $entity->setAttributsResultat($data['resultat']);
        } else {
            return "ERROR : resultat IS NULL";
        }

        // Ressource resultat
        if (isset($data['resultat_ress'])) {
            $entity->setRessourcesResultat($data['resultat_ress']);
        } else {
            return "ERROR : resultat_ress  IS NULL";
        }

        // Ressource Fiche
        if (isset($data['fiche_ress'])) {
            $entity->setRessourcesFiche($data['fiche_ress']);
        } else {
            return "ERROR : fiche_ress  IS NULL";
        }

        // Insee
        if (isset($data['insee'])) {
            $entity->setInsee($data['insee']);
        } else {
            return "ERROR : insee  IS NULL";
        }

        // Insee délégué
        if (isset($data['inseeDelegue'])) {
            $entity->setInseeDelegue($data['inseeDelegue']);
        }

        $em->persist($entity);

        $execption = null;

        if ($withFlush) {
            try {
                //On enregistre dans la base
                $em->flush();
            } catch (Exception $e) {
                $execption = $e;
            }

            return is_null($execption) ? true : "Exception error : ".$execption;
        }

        return true;
    }

    /**
     * Création de l'entity
     */
    public function createOrUpdate($data, $withFlush = true)
    {
        $em = $this->getEntityManager();
        // $this->mergeData($data);

        if (!isset($data['id'])) {
            return "ERROR : id  IS NULL";
        }

        if (!isset($data['lot'])) {
            return "ERROR : lot  IS NULL";
        }

        //$entity = $this->findBy(array('objetId' => $data['id'], 'lot' => $data['lot']) );

        /* if($entity && count($entity) > 0) {
           return $this->updateOne($entity[0], $data, $withFlush);
         }*/

        return $this->createOne($data, $withFlush);
    }

    public function deleteAllByLot($lot)
    {
        $em = $this->getEntityManager();

        $em->createQueryBuilder('d')
            ->delete('BdcomBundle:Donnee', 'd')
            ->where('d.lot = ?1')
            ->setParameter(1, $lot)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param prodigeConn Connection pdo à la base prodige
     */
    public function findAllForApi($prodigeConn, $params)
    {
        $limit = isset($params['limit']) ? $params['limit'] : 20;
        $offset = isset($params['offset']) ? $params['offset'] : 0;

        $insee = isset($params['insee[]']) && is_array($params['insee[]']) ? $params['insee[]'] : array();
        $list = isset($params['list']) ? $params['list'] : false;
        $ressource = isset($params['ressource']) ? $params['ressource'] : true;
        $mode = isset($params['mode']) ? $params['mode'] : 'json';

        $themeFilter = isset($params['themes[]']) && is_array($params['themes[]']) ? $params['themes[]'] : array();

        $result = array();

        $tabLots = array();
        $lotByTheme = array();
        $entities = array();

        if (!empty($insee)) {
            // Requêtte sql
            $qb = $this->createQueryBuilder('d');

            $qb = $qb->select(
                'partial d.{id, objetId, insee, inseeDelegue, objetNom, attributsResultat'.($mode == "geojson" ? ', geomJson' : '').($ressource ? ', ressourcesResultat' : '').'}'
            ) //TODO: prende en compte (mode geojson: geomJson, par défaut: ressourcesResultat)
            ->where('d.insee in ( ?1 )')
                ->innerJoin("d.lot", 'l')
                ->addSelect('partial l.{id, alias, lotTable, carte, avertissementMessage, layerName, theme}');

            if (!empty($themeFilter)) {
                $qb = $qb->andWhere('l.theme in (?2)')->setParameter(2, $themeFilter);
            }

            $qb = $qb->setParameter(1, $insee);

            $qb = $qb->getQuery();
            if ($limit > -1) {
                $qb = $qb->setMaxResults($limit)->setFirstResult($offset);
                $entities = new Paginator($qb, true);
            } else {
                $entities = $qb->setFirstResult($offset)->getResult();
            }


            $objectIdNom = null;
            $currentLot = null;

            // Génération json
            foreach ($entities as $entity) {
                $url = $params['uri']."/1/".$entity->getLot()->getCarte().".map?";
                $objectIdNom = $currentLot !== $entity->getLot() ? null : $objectIdNom;
                if (is_null($objectIdNom)) {
                    $currentLot = $entity->getLot();
                    $champsIdent = $this->getEntityManager()->getRepository(CoucheChamp::class)->findOneBy(
                        array("lot" => $entity->getLot()->getId(), "identifiant" => true)
                    );

                    if ($champsIdent) {
                        $objectIdNom = $champsIdent->getNom();
                    }
                }

                if (!$list) {
                    // Consturction de la partie objet
                    $data = array(
                        'id' => $entity->getObjetId(),
                        'nom_couche' => $entity->getLot()->getAlias(),
                        'nom' => $entity->getObjetNom(),
                        'insee' => $entity->getInsee(),
                        'insee_commune_deleguee' => $entity->getInseeDelegue(),
                        'carto_url' => $url."object=".$entity->getLot()->getLayerName(
                            ).";".$objectIdNom.";".$entity->getObjetId(),
                    );

                    if ($mode == 'pdf') {
                        $champForData = array();
                        $champsData = json_decode($entity->getAttributsResultat(), true);
                        foreach ($champsData as $champ) {
                            $champForData[$champ['nom']] = $champ;
                        }
                        $data["champs"] = $champForData;
                    } else {
                        $data["champs"] = json_decode($entity->getAttributsResultat(), true);
                    }
                    // Contenu tier pour les résultat
                    if ($ressource) {
                        $data['ressources'] = json_decode($entity->getRessourcesResultat());
                    }

                    // Geometrie GeoJson
                    if ($mode == 'geojson') {
                        $data['feature'] = array(
                            "type" => "Feature",
                            "geometry" => json_decode($entity->getGeomJson()),
                        );
                    }

                    // Ajout dans le lot de donnée
                    if (!isset($tabLots [$entity->getLot()->getId()])) {
                        $champs = array();
                        $champsValid = array();

                        $champDatas = $entity->getLot()->getCoucheChamps();

                        // champs statique dans l'objet json
                        foreach ($data["champs"] as $champ) {
                            if (isset($champ['nom'])) {
                                $champsValid[$champ['nom']] = true;
                            }
                        }

                        // champs dynamique depuis la base
                        foreach ($champDatas as $key => $champ) {
                            if (isset($champsValid[$champ->getNom()])) {
                                $champs[] = array(
                                    "nom" => $champ->getNom(),
                                    "alias" => $champ->getAlias(),
                                    "type" => $champ->getType() ? $champ->getType()->getNom() : null,
                                );
                            }
                        }
                        $lot = array();
                        $lot['nom_couche'] = $entity->getLot()->getAlias();
                        $lot['nom_table'] = $entity->getLot()->getLotTable();
                        $lot['carto_url'] = $url;
                        $lot['theme'] = $entity->getLot()->getTheme()->getId();
                        $lot['avertissement'] = $entity->getLot()->getAvertissementMessage();
                        $lot['champs'] = $champs;

                        $tabLots[$entity->getLot()->getId()] = $lot;

                        if ($mode == "pdf") {
                            if (!isset($lotByTheme[$lot['theme']])) {
                                $lotByTheme[$lot['theme']] = array();
                            }
                        }
                    }
                    $tabLots[$entity->getLot()->getId()]['objets'][] = $data;

                    if ($mode == "pdf") {
                        $lotByTheme[$lot['theme']][$entity->getLot()->getId()] = $tabLots[$entity->getLot()->getId()];
                    }
                } else {
                    $tabLots[$entity->getInsee()] = $entity->getInsee();
                }
            }
        }


        return $mode == "pdf" ? $lotByTheme : array_values($tabLots);
    }


    /**
     * @param prodigeConn Connection pdo à la base prodige
     */
    public function getFiche($prodigeConn, $params)
    {
        $idObject = isset($params['id']) ? $params['id'] : null;
        $table = isset($params['table']) ? $params['table'] : null;

        $data = array();

        if ($idObject && $table) {
            $qb = $this->createQueryBuilder('d');

            $qb->select('d')
                ->innerJoin("d.lot", 'l')
                ->where('d.objetId =  ?1')
                ->andWhere('l.lotTable = ?2')
                ->setParameter(1, $idObject)
                ->setParameter(2, $table);

            $entities = $qb->getQuery()->getResult();

            if (count($entities)) {
                $entity = $entities[0];

                $objectIdNom = "";
                $champsIdent = $this->getEntityManager()->getRepository(CoucheChamp::class)->findOneBy(
                    array("lot" => $entity->getLot(), "identifiant" => true)
                );
                if ($champsIdent) {
                    $objectIdNom = $champsIdent->getNom();
                }

                // Url de la carte
                //[URL_CARTO]/[lot_carte].map?object=[layer_name];[champ_identifiant];[identifiant]
                $url = $params['uri']."/1/".$entity->getLot()->getCarte().".map?object=".$entity->getLot()->getLayerName().";".$objectIdNom.";";

                // Récupération des nom de commune
                $referentiel = $entity->getLot()->getReferentiel();

                $communes = array();
                if ($referentiel) {
                    $insees = array();
                    foreach ($entities as $key => $commune) {
                        $insees[] = $commune->getInsee();
                    }
                    $communes = $this->getNameByInsees($prodigeConn, $insees);
                }

                // Création des données de la fiche
                $data = array(
                    'id' => $entity->getObjetId(),
                    'nom_couche' => $entity->getLot()->getAlias(),
                    'nom' => $entity->getObjetNom(),
                    'carto_url' => $url.$entity->getObjetId(),
                    'communes' => $communes,
                    'champs' => json_decode($entity->getAttributsFiche()),
                    'ressources' => json_decode($entity->getRessourcesFiche()),
                    'feature' => array("type" => "Feature", "geometry" => json_decode($entity->getGeomJson())),
                );
            }
        }

        return $data;
    }

    public function getStatistique($prodigeConn, $catalogueConn, $lot, $referential)
    {
        $results = [];
        $referentialTable = $referential->getReferentielTable();

        // Check table
        if (!PostgresqlUtil::checkIfTableExist($prodigeConn, $referentialTable)) {
            return ['erreur' => "La table ".$referentialTable." n'existe pas dans la base prodige"];
        }

        // Check column
        if (!PostgresqlUtil::checkOneColumnInTable($prodigeConn, $referentialTable, "the_geom")) {
            return ['erreur' => "La colonne the_geom n'existe pas dans la table ".$referentialTable];
        }

        try {
            // Get referential geom
            $sql = "SELECT ST_Union(the_geom) as ref_geom from ".$referentialTable." WHERE ST_IsValid(the_geom)";
            $statement = $prodigeConn->prepare($sql);
            $statement->execute();
            $ref_geom = $statement->fetch();

            // General Statistic calculation
            if (isset($ref_geom["ref_geom"])) {
                // Type
                $sql = "SELECT ST_GeometryType(ST_UNION(d.donnee_geom)) AS type FROM bdterr.bdterr_donnee d WHERE ST_IsValid(d.donnee_geom) AND d.donnee_lot_id = '".$lot->getId()."'";
                $sth = $catalogueConn->prepare($sql);
                $sth->execute();
                $type = $sth->fetch()["type"];

                // SRID
                $sql = "SELECT Find_SRID('public', '".$referentialTable."', 'the_geom') AS srid;";
                $sth = $prodigeConn->prepare($sql);
                $sth->execute();
                $srid = $sth->fetch()["srid"];

                // Intersect data
                $qb = $this->createQueryBuilder('d');
                $qb->select("count(d.geom) as number");
                $statisticType = 'punctual'; //Points
                if (in_array($type, ['ST_MultiLineString', 'ST_LineString'])) { // Linear
                    $qb->addSelect(" ST_length( ST_Union(d.geom) ) / 1000 as statistic ");
                    $statisticType = 'linear';
                } elseif (in_array($type, ['ST_MultiPolygon', 'ST_Polygon'])) { // Area
                    $qb->addSelect(" ST_Area( ST_Union(d.geom) ) / 10000 as statistic ");
                    $statisticType = 'area';
                }

                $qb->innerJoin('d.lot', 'l')
                    ->where('l = :lot')
                    ->andWhere("ST_IsValid(:lotGeom) = true")
                    ->andWhere("ST_IsValid(d.geom) = true")
                    ->andWhere("ST_Intersects(:lotGeom, ST_SetSRID(d.geom, ".$srid.")) = true");
                $qb->setParameter(':lot', $lot);
                $qb->setParameter(':lotGeom', $ref_geom["ref_geom"]);

                $result = $qb->getQuery()->getResult();
                $results = ['type' => $statisticType, 'data' => ['all' => ['number' => $result[0]['number']]]];
                if (array_key_exists('statistic', $result[0])) {
                    $results['data']['all']['statistic'] = $result[0]['statistic'];
                }

                // Referential(s) Statistics calculation
                $refIntersectionRepo = $this->getEntityManager()->getRepository(ReferentielIntersection::class);
                $refStats = $refIntersectionRepo->findBy(['statisticIntersect' => true]);
                foreach($refStats as $refStat) {

                    // Check table
                    $refStatsTable = $refStat->getReferentielTable();
                    if (!PostgresqlUtil::checkIfTableExist($prodigeConn, $refStatsTable)) {
                        return ['erreur' => "La table ".$refStatsTable." n'existe pas dans la base prodige"];
                    }

                    // Check column
                    if (!PostgresqlUtil::checkOneColumnInTable($prodigeConn, $refStatsTable, "the_geom")) {
                        return ['erreur' => "La colonne the_geom n'existe pas dans la table ".$refStatsTable];
                    }

                    // Referential geom
                    $sql = "SELECT ".$refStat->getInsee().", the_geom from ".$refStatsTable." WHERE ST_IsValid(the_geom)";
                    $statement = $prodigeConn->prepare($sql);
                    $statement->execute();
                    $geometries = $statement->fetchAll();

                    // Referential SRID
                    $sql = "SELECT Find_SRID('public', '".$refStat->getReferentielTable()."', 'the_geom') AS srid;";
                    $sth = $prodigeConn->prepare($sql);
                    $sth->execute();
                    $srid = $sth->fetch()["srid"];

                    foreach($geometries as $geometry)  {

                        $refStatInsee = $geometry[$refStat->getInsee()];
                        $qb = $this->createQueryBuilder('d');
                        $qb->select("count(d.geom) as number");
                        if (in_array($type, ['ST_MultiLineString', 'ST_LineString'])) { // Linear
                            $qb->addSelect(" COALESCE(ST_length( ST_Union(d.geom) ) / 1000, 0) as statistic ");
                        } elseif (in_array($type, ['ST_MultiPolygon', 'ST_Polygon'])) { // Area
                            $qb->addSelect(" COALESCE(ST_Area( ST_Union(d.geom) ) / 10000, 0) as statistic ");
                        }

                        $qb->innerJoin('d.lot', 'l')
                            ->where('l = :lot')
                            ->andWhere("ST_IsValid(:lotGeom) = true")
                            ->andWhere("ST_IsValid(d.geom) = true")
                            ->andWhere("ST_Intersects(:lotGeom, ST_SetSRID(d.geom, ".$srid.")) = true");
                        $qb->setParameter(':lot', $lot);
                        $qb->setParameter(':lotGeom', $geometry['the_geom']);

                        $result = $qb->getQuery()->getResult();
                        $results['data'][$refStatInsee]['number'] = $result[0]['number'];
                        if (array_key_exists('statistic', $result[0])) {
                            $results['data'][$refStatInsee]['statistic'] = $result[0]['statistic'];
                        }
                    }
                }
            }

        } catch (DBALException $e) {
            return ['erreur' => $e->getMessage()];
        }

        return $results;
    }
}
