<?php

namespace App\Bdterr\BdcomBundle\Command;

use App\Bdterr\BdcomBundle\Entity\Lot;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Bdterr\BdcomBundle\Utils\PostgresqlUtil;
use App\Bdterr\BdcomBundle\Entity\Donnee;

use PDO;
use DateTime;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

class GenerateDataCommand extends Command
{
    protected static $defaultName = 'prodige:generateData';

    /** @var ParameterBagInterface */
    protected $params;

    /** @var RequestStack */
    protected $requestStack;

    /** @var KernelInterface */
    protected $kernel;

    /** @var ManagerRegistry */
    protected $em;

    protected $container;

    private $output = null;
    private $coucheChamps = array();
    private $simpleFilter = array('insee', 'insee_delegue', 'the_geom', 'id', 'nom');
    private $jsonFilter = array('fiche', 'fiche_ress', 'resultat', 'resultat_ress');

    private $champsFilter = array(
        'insee' => array('insee'),
        'insee_delegue' => array('insee_delegue'),
        'the_geom' => array('the_geom'),
        'id' => array(),
        'nom' => array(),
        'fiche' => array(),
        'fiche_ress' => array(),
        'resultat' => array(),
        'resultat_ress' => array(),
    );

    private $ressourceData = array();

    private $contenuTierData = array("inFiche" => array(), "inResult" => array());

    private $champsDate = array();

    const ERROR_FILTER_PROCESS = "116_ERROR_FILTER_116";
    const CONNECTION_CATALOGUE = "catalogue";
    const CONNECTION_PRODIGE = "prodige";

    const DEFAULT_FORMAT_DATE = "d/m/Y";


    public function __construct(
        ParameterBagInterface $params,
        RequestStack $stack,
        KernelInterface $kernel,
        ContainerInterface $container,
        ManagerRegistry $managerRegistry
    ) {
        parent::__construct();
        $this->params = $params;
        $this->requestStack = $stack;
        $this->kernel = $kernel;
        $this->container = $container;
        $this->em = $managerRegistry;
    }

    protected function configure()
    {
        $this->setDescription('Generate data from bdcom layer configuration');
    }

    /**
     *
     */
    protected function getArrayResultFromSql($conn, $sql, $argumentFecthAll = null)
    {
        $sth = $conn->prepare($sql);
        $sth->execute();

        return !is_null($argumentFecthAll) ? $sth->fetchAll($argumentFecthAll) : $sth->fetchAll();
    }

    /**
     * Get Doctrine connection
     * @param string $connection_name
     * @param string $schema
     * @return Connection
     */
    protected function getConnection($connection_name, $schema = "public"): Connection
    {
        $conn = $this->getDoctrine()->getConnection($connection_name);
        $conn->exec('set search_path to '.$schema);

        return $conn;
    }

    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    private function reset()
    {
        $this->coucheChamps = array();
        $this->simpleFilter = array('insee', 'insee_delegue', 'the_geom', 'geom_json', 'id', 'nom');
        $this->jsonFilter = array('fiche', 'fiche_ress', 'resultat', 'resultat_ress');

        $this->champsFilter = array(
            'insee' => array('insee'),
            'insee_delegue' => array('insee_delegue'),
            'the_geom' => array('the_geom'),
            'geom_json' => array('geom_json'),
            'id' => array(),
            'nom' => array(),
            'fiche' => array(),
            'fiche_ress' => array(),
            'resultat' => array(),
            'resultat_ress' => array(),
        );

        $this->champsDate = array();
        $this->ressourceData = array();
        $this->contenuTierData = array("inFiche" => array(), "inResult" => array());
    }

    /**
     * Recherche des lots qui doivent être mis à jour
     */
    private function getLotForUpdate($connCatalogue)
    {
           //d'abord les lots en fonction de catalogue.couche_donnees
        $sqlGetLot = "SELECT lot.lot_id as id
                  from catalogue.couche_donnees as donnees, bdterr.bdterr_lot as lot, bdterr.bdterr_referentiel_intersection as ref
                  where donnees.couchd_emplacement_stockage = lot.lot_table
                  and ( lot.lot_date_maj is null or lot.lot_date_maj < donnees.changedate )
                  and lot.lot_referentiel_id is not null
                  and lot.lot_referentiel_id = ref.referentiel_id";

        $tab = $this->getArrayResultFromSql($connCatalogue, $sqlGetLot, PDO::FETCH_COLUMN);

        // Puis, on récupère les lots sous forme d'entity
        return $this->em->getRepository(Lot::class)->findBy(array('id' => $tab));
    }

    /**
     * Vérification des table et colonne Prodige
     */
    private function checkTableAndColumn($connProdige, $lot, $referentiel): bool
    {
        $testRefTable = PostgresqlUtil::checkIfTableExist($connProdige, $referentiel->getReferentielTable());
        $testZone = PostgresqlUtil::checkIfTableExist($connProdige, $lot->getLotTable());

        $testColumnsRef = PostgresqlUtil::checkOneColumnInTable(
            $connProdige,
            $referentiel->getReferentielTable(),
            $referentiel->getInsee()
        );
        $testColumnsGeom = PostgresqlUtil::checkOneColumnInTable($connProdige, $lot->getLotTable(), "the_geom");

        if ($this->output) {
            if (!$testColumnsRef) {
                $this->output->writeln(
                    'Problème : la colonne '.$referentiel->getInsee().' de la table '.$referentiel->getReferentielTable(
                    )." n'existe pas"
                );
            }

            if (!$testColumnsGeom) {
                $this->output->writeln(
                    'Problème : la colonne the_geom de la table '.$lot->getLotTable()." n'existe pas"
                );
            }

            if (!$testRefTable) {
                $this->output->writeln(
                    'Problème table name_couche_ref : '.$referentiel->getReferentielTable()." n'existe pas"
                );
            }

            if (!$testZone) {
                $this->output->writeln("Problème table name_couche_zone : ".$lot->getLotTable()." n'existe pas");
            }
        }


        return $testRefTable && $testZone && $testColumnsRef && $testColumnsGeom;
    }

    /**
     * Création des données selon le type de filtre
     */
    private function filterProcess($filterName, $column, $value, $filter = null)
    {
        if (in_array($filterName, $this->simpleFilter)) {
            return $value;
        }

        if (isset($this->champsDate[$column]) && !is_null($value)) {
            $date = date_create($value);
            $value = $date ? date_format($date, $this->champsDate[$column]) : $value;
        }

        switch ($filterName) {
            case 'resultat':
                return array('nom' => $column, 'valeur' => $value);
            case 'fiche':
                return array(
                    "alias" => $filter["alias"],
                    "valeur" => $value,
                    "nom" => $filter["nom"],
                    "type" => $filter["type"],
                    "ordre" => $filter["ordre"],
                );
            case 'fiche_ress':
            case 'resultat_ress':

                if (!empty($filter) && isset($filter["key"])) {
                    $return = [];
                    foreach ($filter["key"] as $key) {
                        if (isset($this->ressourceData[$key])) {
                            $data = $this->ressourceData[$key];
                            if (isset($data['column']) && $data['column'] === $column && isset($data["valeur"])) {
                                if (isset($data['type']) && in_array(
                                        $data['type'],
                                        array('dossier-images', 'dossier-autres-supports')
                                    )) {
                                    $values = [];
                                    $folder = str_replace("{{".$column."}}", $value, $data["valeur"]);
                                    $base_folder = $this->container->getParameter(
                                            "PRODIGE_PATH_DATA"
                                        )."/base_territoriale/";
                                    $file_folder = $base_folder.$folder;
                                    $base_url = $this->container->getParameter(
                                            "PRODIGE_URL_CATALOGUE"
                                        )."/base_territoriale/data/";
                                    if (is_dir($file_folder)) {
                                        $files = scandir($file_folder);
                                        foreach ($files as $file) {
                                            if ($file != '.' && $file != '..') {
                                                $file_url = $base_url.$folder.'/'.$file;
                                                $values[] = $file_url;
                                            }
                                        }
                                    }

                                    $return[] = array(
                                        "alias" => $data["alias"],
                                        "ordre" => $data["ordre"],
                                        "valeur" => $values,
                                        "type" => $data["type"],
                                    );
                                } else {
                                    $return[] = array(
                                        "alias" => $data["alias"],
                                        "ordre" => $data["ordre"],
                                        "valeur" => str_replace("{{".$column."}}", $value, $data["valeur"]),
                                        "type" => $data["type"],
                                    );
                                }
                            }
                        }
                    }
                    if (count($return)) {
                        return $return;
                    }
                }
        }

        return GenerateDataCommand::ERROR_FILTER_PROCESS;
    }

    /**
     *
     */
    static function compareAsc($a, $b): int
    {
        if ($a["ordre"] == $b["ordre"]) {
            return 0;
        }

        return ($a["ordre"] <= $b["ordre"]) ? -1 : 1;
    }

    static public function deleteKeyFromArrayObject($array, $keyForDel)
    {
        foreach ($array as $index => $objects) {
            foreach ($objects as $key => $value) {
                if ($key === $keyForDel) {
                    unset($array[$index][$key]);
                }
            }
        }

        return $array;
    }

    /**
     * Finalise le processus de transformation en mettant les données au bon format et ajout de ressource supplémentaire
     */
    private function postProcess($dataForEntity, $lot)
    {
        // fusion des ressources statique et dynamique
        $dataForEntity['resultat_ress'] = array_merge(
            $dataForEntity['resultat_ress'],
            $this->contenuTierData["inResult"]
        );
        $dataForEntity['fiche_ress'] = array_merge($dataForEntity["fiche_ress"], $this->contenuTierData["inFiche"]);

        // tri les ressource selon leur ordre
        $toSort = ['resultat_ress', 'fiche_ress', 'fiche'];
        foreach ($toSort as $index) {
            if (isset($dataForEntity[$index]['ordre'])) {
                usort($dataForEntity[$index], array($this, "compareAsc"));
                $dataForEntity[$index] = $this->deleteKeyFromArrayObject($dataForEntity[$index], 'ordre');
            }
        }

        // Valeur simple
        foreach ($this->simpleFilter as $filter) {
            if (isset($dataForEntity[$filter]) && is_array(
                    $dataForEntity[$filter]
                ) && !empty($dataForEntity[$filter])) {
                $dataForEntity[$filter] = $dataForEntity[$filter][0];
            } else {
                $dataForEntity[$filter] = null;
            }
        }

        // Tableau json
        foreach ($this->jsonFilter as $filter) {
            if (isset($dataForEntity[$filter])) {
                $dataForEntity[$filter] = json_encode($dataForEntity[$filter]);
            }
        }

        $dataForEntity["lot"] = $lot;

        return $dataForEntity;
    }

    /**
     * Transformation des données en fonction des filtres
     */
    private function transformDataFromRow($row, $lot): bool
    {
        // Création de l'entity
        $dataForEntity = array();

        foreach ($this->champsFilter as $filterName => $filters) { // Filtre F
            $dataForEntity[$filterName] = array();

            foreach ($filters as $filter) {  // Colonne C
                $column = $filter;
                if (is_array($filter)) {
                    $column = $filter["value"] ?? null;
                }

                $is_jointed_field = false;
                $key = $column;
                if (preg_match('/(.*) \((.*)\)/', $column, $matches)) {
                    if (array_key_exists($matches[1]."__".$matches[2], $row)) {
                        $is_jointed_field = true;
                        $key = $matches[1]."__".$matches[2];
                    }
                }

                if (!is_null($column) && (array_key_exists($column, $row) || $is_jointed_field)) {
                    $value = $this->filterProcess($filterName, $column, $row[$key], $filter);

                    if ($value !== GenerateDataCommand::ERROR_FILTER_PROCESS) {
                        if (in_array($filterName, ['fiche_ress', 'resultat_ress'])) {
                            foreach ($value as $v) {
                                $dataForEntity[$filterName][] = $v;
                            }
                        } else {
                            $dataForEntity[$filterName][] = $value;
                        }
                    }
                }
            }
        }

        $dataForEntity = $this->postProcess($dataForEntity, $lot);

        $message = $this->em->getRepository(Donnee::class)->createOrUpdate($dataForEntity, false);

        if ($message !== true) {
            $this->output->writeln("error ".$message);

            return false;
        }

        return true;
    }

    /**
     * Sauvegarde dans la base via doctrine
     */
    private function saveData($em, $lot): ?Exception
    {
        $exception = null;

        try {
            $lot->setDateMaj(new DateTime());
            $em->persist($lot);

            //On enregistre dans la base
            $em->flush();
        } catch (Exception $e) {
            $exception = $e;
        }

        return $exception;
    }

    private function mainProcess($connProdige, $connCatalogue, $lot, $referentiel): bool
    {
        $isOk = true;
        $referentialName = ($lot->getCuttingType(
            ) == 'file') ? "Le fichier CSV fourni en paramètre" : $lot->getLotTable()." (".$lot->getAlias().") ";
        $this->output->writeln(
            'Intersection entre '.$referentiel->getReferentielTable()." (".$referentiel->getNom().")"
            .' et '.$referentialName
        );

        $inseeDelegue = ($referentiel->getInseeDeleguee() ? " , ".$referentiel->getInseeDeleguee(
            )." as insee_delegue" : "");
        $inseeDeleguGroupBy = ($referentiel->getInseeDeleguee() ? " , insee_delegue " : "");

        //list all fields gid + id + nom + fiche + résultats
        $fields = array_merge(
            array("gid"),
            array_keys($this->champsFilter['fiche']),
            $this->champsFilter['resultat'],
            $this->champsFilter['id'],
            $this->champsFilter['nom']
        );

        // Get jointures
        $jointures = $lot->getCoucheJointures();
        $joinFieldData = array();
        foreach ($jointures as $jointure) {
            $joinFields = explode(',', $jointure->getProdigeField());
            foreach ($joinFields as $joinField) {
                $joinFieldData[$jointure->getProdigeTable()][$joinField] = array(
                    'prodige_table' => $jointure->getProdigeTable(),
                    'prodige_key' => $jointure->getProdigeKey(),
                    'lot_key' => $jointure->getLotField(),
                    'prodige_field' => $joinField,
                );
            }
        }

        $joinQuery = $joinGroupBy = "";
        $fields = array_unique($fields);
        foreach ($fields as $key => $item) {
            if (preg_match('/(.*) \((.*)\)/', $item, $matches)) {
                if (array_key_exists($matches[2], $joinFieldData) && is_array(
                        $joinFieldData[$matches[2]]
                    ) && array_key_exists($matches[1], $joinFieldData[$matches[2]])) {
                    $uniqId = uniqid();
                    $fields[$key] = "j".$uniqId.".".$matches[1]." as ".$matches[1]."__".$matches[2];
                    $prodige_table = $joinFieldData[$matches[2]][$matches[1]]['prodige_table'];
                    $prodige_key = $joinFieldData[$matches[2]][$matches[1]]['prodige_key'];
                    $lot_key = $joinFieldData[$matches[2]][$matches[1]]['lot_key'];
                    $joinQuery .= " LEFT JOIN (SELECT $prodige_key, string_agg(".$matches[1]."::text, '; ') as $matches[1] FROM $prodige_table GROUP BY $prodige_key) j$uniqId";
                    $joinQuery .= " ON v.$lot_key = j$uniqId.$prodige_key";
                    $joinGroupBy .= ", j$uniqId.$matches[1]";
                }
            } else {
                $fields[$key] = "v.".$item;
            }
        }

        // 1 - Recherche d'intersection
        if ($lot->getCuttingType() == 'ref') {
            $buffer = (!empty($lot->getBuffer())) ? $lot->getBuffer() : 0;
            if ($buffer > 0) {
                $this->output->writeln("Application d'un tampon de ".$buffer." mètre(s) sur l'intersection");
            }
            $sqlIntersect = "SELECT v.gid, w.".$referentiel->getInsee()." as insee, ".implode(
                    ",",
                    $fields
                ).$inseeDelegue."
                      FROM ".$referentiel->getReferentielTable()." w, ".$lot->getLotTable()." v ";

            if ($joinQuery != "") {
                $sqlIntersect .= $joinQuery;
            }

            $sqlIntersect .= " WHERE st_isvalid(w.the_geom)='true' and st_isvalid(v.the_geom)='true' and
                  w.the_geom && v.the_geom and
                  st_intersects(st_buffer(w.the_geom, ".$buffer."), v.the_geom)
                  group by v.gid, v.the_geom, w.".$referentiel->getInsee(
                ).", w.the_geom".$inseeDeleguGroupBy."$joinGroupBy;";
        } elseif ($lot->getCuttingType() == 'file') {
            $sqlIntersect = "SELECT v.gid, m.mapping_insee as insee, v.the_geom, st_asgeojson(v.the_geom) as geom_json, ".implode(
                    ",",
                    $fields
                )." FROM ".$lot->getLotTable()." v
                      JOIN bdterr.bdterr_mapping m ON m.mapping_object_id = v.gid";

            if ($joinQuery != "") {
                $sqlIntersect .= $joinQuery;
            }
        } elseif ($lot->getCuttingType() == 'field' && $lot->getMappingField() != '') {
            $sqlIntersect = "SELECT v.gid, v.".$lot->getMappingField(
                )." as insee, v.the_geom, st_asgeojson(v.the_geom) as geom_json, ".implode(
                    ",",
                    $fields
                )." FROM ".$lot->getLotTable()." v";

            if ($joinQuery != "") {
                $sqlIntersect .= $joinQuery;
            }
        }


        try {
            if (isset($sqlIntersect)) {
                $stmt = $connProdige->prepare($sqlIntersect);
                $stmt->execute();

                // 2 - Suppression des anciennes données du lot
                $nb = 0;
                $donneeOk = 0;

                $this->em->getRepository(Donnee::class)->deleteAllByLot($lot);

                //On enregistre dans la base
                $this->em->getManager()->flush();

                // 3 - Traitement des données
                $objectDatas = array();

                // Algo(DxFxC)
                while ($row = $stmt->fetch()) { // Donnée
                    // On vérifie qu'il n'y ait pas de doublon
                    if (isset($this->champsFilter['id'][0]) && isset($row[$this->champsFilter['id'][0]])
                        && (!isset($objectDatas[$row[$this->champsFilter['id'][0]]]) || ($objectDatas[$row[$this->champsFilter['id'][0]]] != $row['insee']))) {
                        $transformOk = $this->transformDataFromRow($row, $lot);

                        $donneeOk = $transformOk ? ($donneeOk + 1) : $donneeOk;

                        if ($transformOk) {
                            $objectDatas[$row[$this->champsFilter['id'][0]]] = $row['insee'];
                        }
                    }

                    $nb++;
                }

                // 4 - sauvegarde
                $exception = $this->saveData($this->em->getManager(), $lot);
                $connProdigeParams = $connProdige->getParams();

                // Mise à jour des géométries
                $sqlGeom = "UPDATE bdterr.bdterr_donnee destination
                SET donnee_geom_json = 
                  -- dblink connection
                  (SELECT * FROM public.dblink(
                  'host=".$connProdigeParams['host']." port=".$connProdigeParams['port']." dbname=".$connProdigeParams['dbname']." user=".$connProdigeParams['user']." password=".$connProdigeParams['password']."'::text, 
                  'SELECT st_AsGeojson(the_geom::geometry)::text as geom_json FROM ' || lot.lot_table || ' WHERE ' || lot.lot_colonne || '::text = ''' || destination.donnee_objet_id || '''::text') as origin(geom_json text))
                FROM (select l.*, cc.champ_nom as lot_colonne from bdterr.bdterr_lot l left join bdterr.bdterr_couche_champ cc on l.lot_id = cc.champ_lot_id where cc.champ_identifiant=true) lot
                WHERE lot.lot_id = destination.donnee_lot_id AND lot.lot_id = ".$lot->getId()." AND destination.donnee_geom_json IS NULL;
                UPDATE bdterr.bdterr_donnee SET donnee_geom = st_geomFromGeojson(donnee_geom_json) WHERE donnee_geom_json IS NOT NULL;";
                $connCatalogue->exec($sqlGeom);

                // 5 - Affichage pour les logs
                $ok = is_null($exception) || "Exception error";
                $isOk = $ok && ($nb > 0);

                if ($this->output) {
                    $this->output->writeln($nb.' intersections calculées et '.$donneeOk." bien enregistrées ");
                }
            }
        } catch (\Exception $e) {
            $isOk = false;
            $this->output->writeln('Exception pour récupérer les données');
            $this->output->writeln($e->getMessage());
        }

        return $isOk;
    }

    private function getFilterFromChamp($lot): bool
    {
        $this->output->writeln("");
        $this->output->writeln('Traitement couche champs');
        $this->champsFilter['id'][0] = null;
        $this->champsFilter['nom'][0] = null;

        $this->coucheChamps = $lot->getCoucheChamps();

        foreach ($this->coucheChamps as $champs) {
            if ($champs->getIdentifiant()) { // Object id
                $this->champsFilter['id'][0] = $champs->getNom();
            }

            if ($champs->getLabel()) {  //objet nom
                $this->champsFilter['nom'][0] = $champs->getNom();
            }

            if ($champs->getFiche()) {  // fiche attribut
                $this->champsFilter['fiche'][$champs->getNom()] = array(
                    "nom" => $champs->getNom(),
                    "alias" => $champs->getAlias(),
                    "type" => ($champs->getType() ? $champs->getType()->getNom() : null),
                    "value" => $champs->getNom(),
                    "ordre" => $champs->getOrdre(),
                );
            }

            if ($champs->getType() && $champs->getType()->getNom() == 'date') {
                $this->champsDate[$champs->getNom()] = $champs->getFormat() ? $champs->getFormat(
                ) : GenerateDataCommand::DEFAULT_FORMAT_DATE;
            }

            if ($champs->getResultats()) { // resultat attribut
                $this->champsFilter['resultat'][] = $champs->getNom();
            }

            $this->champsInApi[$champs->getNom()] = $champs->getFiche() || $champs->getResultats(
                ) || $champs->getIdentifiant() || $champs->getLabel();
        }

        if ($this->output) {
            $this->output->writeln("Objet id : ".$this->champsFilter['id'][0]);
            $this->output->writeln("Objet nom : ".$this->champsFilter['nom'][0]);
        }

        return !is_null($this->champsFilter['id'][0]) && !is_null($this->champsFilter['nom'][0]);
    }

    /**
     * Recherche des ressources / contenuTier
     */
    private function searchRessource($lot): bool
    {
        $contenuTiers = $lot->getContenusTiers();

        foreach ($contenuTiers as $key => $contenuTier) {
            preg_match_all("/\{{(.*?)\}}/", $contenuTier->getUrl(), $matches);

            $this->ressourceData[$key] = array(
                "alias" => $contenuTier->getNom(),
                "valeur" => $contenuTier->getUrl(),
                "ordre" => $contenuTier->getOrdre(),
                "type" => $contenuTier->getType() ? $contenuTier->getType()->getNom() : null,
            );

            if (!empty($matches) && !empty($matches[1]) && $contenuTier->getType()->getNom() == 'url') {
                $this->ressourceData[$key]["column"] = $matches[1][0];
                if ($contenuTier->getFiche()) {
                    if (isset($this->champsFilter['fiche_ress'][$matches[1][0]])) {
                        $this->champsFilter['fiche_ress'][$matches[1][0]]["key"][] = $key;
                    } else {
                        $this->champsFilter['fiche_ress'][$matches[1][0]]["value"] = $matches[1][0];
                        $this->champsFilter['fiche_ress'][$matches[1][0]]["key"] = array($key);
                    }
                }
                if ($contenuTier->getResultats()) {
                    if (isset($this->champsFilter['resultat_ress'][$matches[1][0]])) {
                        $this->champsFilter['resultat_ress'][$matches[1][0]]["key"][] = $key;
                    } else {
                        $this->champsFilter['resultat_ress'][$matches[1][0]]["value"] = $matches[1][0];
                        $this->champsFilter['resultat_ress'][$matches[1][0]]["key"] = array($key);
                    }
                }
            } else {
                $values = [];
                if (in_array($contenuTier->getType()->getNom(), array('dossier-images', 'dossier-autres-supports'))) {
                    $base_folder = $this->container->getParameter("PRODIGE_PATH_DATA")."/base_territoriale/";
                    $file_folder = $base_folder.$lot->getLotTable().'/'.$contenuTier->getUrl();
                    $base_url = $this->container->getParameter("PRODIGE_URL_CATALOGUE")."/base_territoriale/data/";
                    if (is_dir($file_folder)) {
                        $files = scandir($file_folder);
                        foreach ($files as $file) {
                            if ($file != '.' && $file != '..' && is_file($file_folder.'/'.$file)) {
                                $file_url = $base_url.$lot->getLotTable().'/'.$contenuTier->getUrl().'/'.$file;
                                $values[] = array(
                                    "alias" => $contenuTier->getNom(),
                                    "valeur" => $file_url,
                                    "type" => $contenuTier->getType()->getNom(),
                                );
                            }
                        }
                    }
                } else {
                    $values[] = array(
                        array(
                            "alias" => $contenuTier->getNom(),
                            "valeur" => $contenuTier->getUrl(),
                            "ordre" => $contenuTier->getOrdre(),
                            "type" => $contenuTier->getType() ? $contenuTier->getType()->getNom() : null,
                        ),
                    );
                }

                if ($contenuTier->getFiche() && count($values) > 0) {
                    $this->contenuTierData['inFiche'] = array_merge($this->contenuTierData['inFiche'], $values);
                }

                if ($contenuTier->getResultats() && count($values) > 0) {
                    $this->contenuTierData['inResult'] = array_merge($this->contenuTierData['inResult'], $values);
                }
            }
        }

        return true;
    }

    /**
     * Calcul stats
     */
    protected function calculStatistique($connProdige, $connCatalogue, $lot, $referentiel): bool
    {
        if (!$lot->getStatistiques()) {
            return true;
        }

        $result = $this->em->getRepository(Donnee::class)->getStatistique($connProdige, $connCatalogue, $lot, $referentiel);
        $isOk = false;

        if (isset($result['exception'])) {
            $this->output->writeln("Calcul des statistiques : ".$result['exception']);
        }
        if (isset($result['erreur'])) {
            $this->output->writeln($result['erreur']);
        } else {
            if (isset($result["data"])) {
                $isOk = true;

                $this->output->writeln("Calcul OK");

                $lot->setStatistic(json_encode($result));

                $this->saveData($this->em->getManager(), $lot);
            } else {
                $this->output->writeln("Erreur pendant le calcul de statistique");
            }
        }

        return $isOk;
    }

    /**
     * Execute command
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;

        $connCatalogue = $this->getDoctrine()->getConnection(GenerateDataCommand::CONNECTION_CATALOGUE);
        $connProdige = $this->getDoctrine()->getConnection(GenerateDataCommand::CONNECTION_PRODIGE);

        $lots = $this->getLotForUpdate($connCatalogue);
        $date = new DateTime();
        if (!empty($lots)) {
            $this->output->writeln(count($lots)." Lots à mettre jour (".$date->format('Y-m-d\TH:i:s.u').")");

            foreach ($lots as $lot) {
                $this->reset();

                //LOG
                $this->output->writeln("");
                $this->output->writeln("-----------------------------------------------");
                $this->output->writeln("Traitement du lot : ".$lot->getAlias()." (id:".$lot->getId().") ");
                $this->output->writeln("");

                $referentiel = $lot->getReferentiel();

                // Vérification des noms de tables et colonnes
                $isOk = $this->checkTableAndColumn($connProdige, $lot, $referentiel);

                // Recherche des filtres venant des champs
                $isOk = $isOk && $this->getFilterFromChamp($lot);

                // Recherche des ressources / contenu tiers supplémentaire
                $isOk = $isOk && $this->searchRessource($lot);

                // Calcul des intersections
                $isOk = $isOk && $this->mainProcess($connProdige, $connCatalogue, $lot, $referentiel);

                // Calcul des statistiques du lot de données
                $isOk = $isOk && $this->calculStatistique($connProdige, $connCatalogue, $lot, $referentiel);

                if ($isOk) {
                    $this->output->writeln(
                        "lot correctement  mis à jour (".$date->format('Y-m-d\TH:i:s.u').")"
                    );
                }
            }
        } else {
            $this->output->writeln("Aucun lot à  mettre à jour (".$date->format('Y-m-d\TH:i:s.u').")");
        }
        // print_r($result);
        $this->output->writeln("");
        $this->output->writeln('Fin');

        return Command::SUCCESS;
    }
}

