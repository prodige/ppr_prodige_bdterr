<?php

namespace App\JMose\CommandSchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Entity ScheduledCommand
 *
 * @author  Julien Guyon <julienguyon@hotmail.com>
 * @package JMose\CommandSchedulerBundle\Entity
 *//**
 * ScheduledCommand
 *
 * @ORM\Table(name="catalogue.scheduled_command")
 * @ORM\Entity(repositoryClass="App\JMose\CommandSchedulerBundle\Repository\ScheduledCommandRepository")
 */
class ScheduledCommand
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @var string
     * 
     * @ORM\Column(name="command", type="text", nullable=true)
     */
    private $command;

    /**
     * @var string
     * 
     * @ORM\Column(name="arguments", type="text", nullable=true)
     */
    private $arguments;

    /**
     * @see http://www.abunchofutils.com/utils/developer/cron-expression-helper/
     * @var string
     * 
     * @ORM\Column(name="cron_expression", type="text", nullable=true)
     */
    private $cronExpression;

    /**
     * @var \Doctrine\Common\Collections\DateTime
     *
     * @ORM\Column(name="last_execution", type="datetime", nullable=true)
     */
    private $lastExecution;

    /**
     * @var integer
     * 
     * @ORM\Column(name="last_return_code", type="integer", nullable=true, options={"default" : 0})
     */
    private $lastReturnCode;

    /**
     * Log's file name (without path)
     *
     * @var string
     * 
     * @ORM\Column(name="log_file", type="text", nullable=true)
     */
    private $logFile;

    /**
     * @var integer
     * 
     * @ORM\Column(name="priority", type="integer", nullable=true, options={"default" : 0})
     */
    private $priority;

    /**
     * If true, command will be execute next time regardless cron expression
     *
     * @var boolean
     * 
     * @ORM\Column(name="execute_immediately", type="boolean", nullable=true, options={"default" : false})
     */
    private $executeImmediately;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="disabled", type="boolean", nullable=true, options={"default" : false})
     */
    private $disabled;

    /**
     * @var boolean
     * 
     * @ORM\Column(name="locked", type="boolean", nullable=true, options={"default" : false})
     */
    private $locked;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_periodic", type="boolean", nullable=false)
     */
    private $isPeriodic;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="frequency", type="text", nullable=true)
     */
    private $frequency;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Exclude
     *
     * @ORM\OneToMany(targetEntity="App\JMose\CommandSchedulerBundle\Entity\ExecutionReport", mappedBy="scheduledCommand")
     */
    private $executionReports;

    /**
     * Init new ScheduledCommand
     */
    public function __construct()
    {
        $this->setLastExecution(new \DateTime());
        $this->setLocked(false);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ScheduledCommand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set command
     *
     * @param string $command
     * @return ScheduledCommand
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get arguments - If toArray is passed to true, then the argument string is transformed into an exploitable array
     *  to init and InputArgumentArray and run command
     *
     * @param bool $toArray
     * @return array|mixed
     */
    public function getArguments($toArray = false)
    {
        if (false === $toArray) {
            return $this->arguments;
        }

        $argsArray = array();
        if (null !== $this->arguments || '' != $this->arguments) {
            $flatArgsArray = explode(' ', preg_replace('/\s+/', ' ', $this->arguments));
            foreach ($flatArgsArray as $argument) {
                $tmpArray = explode('=', $argument);
                if (count($tmpArray) == 1) {
                    $argsArray[$tmpArray[0]] = true;
                } else {
                    $argsArray[$tmpArray[0]] = $tmpArray[1];
                }
            }
        }

        return $argsArray;
    }

    /**
     * Set arguments
     *
     * @param string $arguments
     * @return ScheduledCommand
     */
    public function setArguments($arguments)
    {
        $this->arguments = $arguments;

        return $this;
    }

    /**
     * Get cronExpression
     *
     * @return string
     */
    public function getCronExpression()
    {
        return $this->cronExpression;
    }

    /**
     * Set cronExpression
     *
     * @param string $cronExpression
     * @return ScheduledCommand
     */
    public function setCronExpression($cronExpression)
    {
      $this->cronExpression = $cronExpression;

      return $this;
    }

    /**
     * Get lastExecution
     *
     * @return \DateTime
     */
    public function getLastExecution()
    {
        return $this->lastExecution;
    }

    /**
     * Set lastExecution
     *
     * @param \DateTime $lastExecution
     * @return ScheduledCommand
     */
    public function setLastExecution($lastExecution)
    {
        $this->lastExecution = $lastExecution;

        return $this;
    }

    /**
     * Get logFile
     *
     * @return string
     */
    public function getLogFile()
    {
        return $this->logFile;
    }

    /**
     * Set logFile
     *
     * @param string $logFile
     * @return ScheduledCommand
     */
    public function setLogFile($logFile)
    {
        $this->logFile = $logFile;

        return $this;
    }

    /**
     * Get lastReturnCode
     *
     * @return integer
     */
    public function getLastReturnCode()
    {
        return $this->lastReturnCode;
    }

    /**
     * Set lastReturnCode
     *
     * @param integer $lastReturnCode
     * @return ScheduledCommand
     */
    public function setLastReturnCode($lastReturnCode)
    {
        $this->lastReturnCode = $lastReturnCode;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     * @return ScheduledCommand
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get executeImmediately
     *
     * @return bool
     */
    public function isExecuteImmediately()
    {
        return $this->executeImmediately;
    }

    /**
     * Get executeImmediately
     *
     * @return boolean
     */
    public function getExecuteImmediately()
    {
        return $this->executeImmediately;
    }

    /**
     * Set executeImmediately
     *
     * @param $executeImmediately
     * @return ScheduledCommand
     */
    public function setExecuteImmediately($executeImmediately)
    {
        $this->executeImmediately = $executeImmediately;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     * @return ScheduledCommand
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Locked Getter
     *
     * @return boolean
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * locked Getter
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * locked Setter
     *
     * @param boolean $locked
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get the value of isPeriodic
     *
     * @return  string
     */ 
    public function getIsPeriodic()
    {
        return $this->isPeriodic;
    }

    /**
     * Set the value of isPeriodic
     *
     * @param  string  $isPeriodic
     *
     * @return  self
     */ 
    public function setIsPeriodic( $isPeriodic)
    {
        $this->isPeriodic = $isPeriodic;

        return $this;
    }

    /**
     * Get enableds
     * Get ParentId 
     * 
     * @VirtualProperty
     * @SerializedName("enabled")
     * 
     * @return boolean
     */
    public function getEnabled()
    {
        return !$this->disabled;
    }

    /**
     * Get the value of year
     *
     * @return  integer
     */ 
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set the value of year
     *
     * @param  integer  $year
     *
     * @return  self
     */ 
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * 
     * @return array
     */
    public function getCronDate()
    {
      $cronDate = explode( " ", $this->getCronExpression());

      #TODO: optimisable ? 
      $cronDateTab = array(
        "minute"  => isset($cronDate[0]) ? $cronDate[0] : null,
        "hour"    => isset($cronDate[1]) ? $cronDate[1] : null, 
        "dayMonth"=> isset($cronDate[2]) ? $cronDate[2] : null, 
        "month"   => isset($cronDate[3]) ? $cronDate[3] : null, 
        "dayWeek" => isset($cronDate[4]) ? $cronDate[4] : null,
        "year"    =>  $this->getYear()
      );
      
      return $cronDateTab;
    }
    

    /**
     * Get the value of frequency
     *
     * @return  string
     */ 
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set the value of frequency
     *
     * @param  string  $frequency
     *
     * @return  self
     */ 
    public function setFrequency( $frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }
}
