<?php

namespace App\JMose\CommandSchedulerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * ExecutionReport
 *
 * @ORM\Table(name="catalogue.execution_report")
 * @ORM\Entity(repositoryClass="App\JMose\CommandSchedulerBundle\Repository\ExecutionReportRepository")
 */
class ExecutionReport
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="date_time", type="datetime")
   */
  private $dateTime;

  /**
   * @Exclude
   * 
   * @var string
   *
   * @ORM\Column(name="file", type="string", length=255)
   */
  private $file;


  /**
   * @var int
   * 
   * @Exclude
   * 
   * @ORM\ManyToOne(targetEntity="App\JMose\CommandSchedulerBundle\Entity\ScheduledCommand", inversedBy="executionReports")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="scheduled_command_id", referencedColumnName="id", nullable=true)
   * })
   */
  private $scheduledCommand;

  /**
   * @Exclude
   * 
   * @var string
   *
   * @ORM\Column(name="status", type="string", length=255, nullable=true )
   */
  private $status;


  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }



  /**
   * Set file
   *
   * @param string $file
   *
   * @return ExecutionReport
   */
  public function setFile($file)
  {
      $this->file = $file;

      return $this;
  }

  /**
   * Get file
   *
   * @return string
   */
  public function getFile()
  {
      return $this->file;
  }

  /**
   * Set scheduledCommand
   *
   * @param integer $scheduledCommand
   *
   * @return ExecutionReport
   */
  public function setScheduledCommand($scheduledCommand)
  {
      $this->scheduledCommand = $scheduledCommand;

      return $this;
  }

  /**
   * Get scheduledCommand
   *
   * @return int
   */
  public function getScheduledCommand()
  {
      return $this->scheduledCommand;
  }

  /**
   * Get the value of dateTime
   *
   * @return  \DateTime
   */ 
  public function getDateTime()
  {
      return $this->dateTime;
    }
  /**
   * Get Command name
   * 
   * @VirtualProperty
   * @SerializedName("commandName")
   * 
   * @return string
   */
  public function getCommandName()
  {
    return $this->scheduledCommand->getName();
  }

  

  /**
   * Set the value of dateTime
   *
   * @param  \DateTime  $dateTime
   *
   * @return  self
   */ 
  public function setDateTime(\DateTime $dateTime)
  {
    $this->dateTime = $dateTime;

    return $this;
  }

  /**
   * Get the value of status
   *
   * @return  string
   */ 
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set the value of status
   *
   * @param  string  $status
   *
   * @return  self
   */ 
  public function setStatus( $status)
  {
    $this->status = $status;

    return $this;
  }
}

