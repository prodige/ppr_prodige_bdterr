<?php

namespace App\JMose\CommandSchedulerBundle;

use App\JMose\CommandSchedulerBundle\DependencyInjection\JMoseCommandSchedulerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JMoseCommandSchedulerBundle extends Bundle
{
    /**
     * {@inheritdoc}
     * @return JMoseCommandSchedulerExtension
     */
    public function getContainerExtension(): JMoseCommandSchedulerExtension
    {
        $class = $this->getContainerExtensionClass();
        return new $class;
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensionClass(): string
    {
        return 'App\JMose\CommandSchedulerBundle\DependencyInjection\JMoseCommandSchedulerExtension';
    }
}
