<?php

namespace App\JMose\CommandSchedulerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use App\JMose\CommandSchedulerBundle\Entity\ScheduledCommand;
use DateTime;

/**
 * Class ScheduledCommandRepository
 *
 * @author  Julien Guyon <julienguyon@hotmail.com>
 * @package JMose\CommandSchedulerBundle\Entity\Repository
 */
class ScheduledCommandRepository extends EntityRepository
{
  /***
   * 
   */
  function frequencyType($cronExpression){
     $type = null;

    if(is_array($cronExpression) && count($cronExpression) >= 5 ){
       $dailyTest = $cronExpression[1];
       $weeklyTest = $cronExpression[4];

      if(is_numeric($weeklyTest) ){
        $type = "weekly";
      }
      elseif( is_numeric($dailyTest)){
        $type = "daily";
      }
    }
    
    return $type;
  }

  /**
   * 
   */
  function findAllForApi(){
    $qb = $this->createQueryBuilder('u');     
    $entities = $qb->getQuery()->getResult();
    
    $resoponse = array();
    foreach($entities as $key => $entity){
      $data = $entity->getCronDate();

      $data["id"] = $entity->getId();
      $data["name"] = $entity->getName();
      $data["command"] = $entity->getCommand();
      $data["enabled"] = $entity->getEnabled();
      $data["locked"] = $entity->getLocked();
      $data["isPeriodic"] = $entity->getIsPeriodic();
      $data["arguments"] = $entity->getArguments();
      $data["frequency"] = $this->frequencyType( explode(" ", $entity->getCronExpression()) );

      $resoponse[] = $data;
    }

    return $resoponse;
  }

  /**
   * 
   */
  public function update($id, $data){
    $em = $this->getEntityManager();
    $isOk = false;
    $entity = null;
    $cronTab = array();
      
    if($id){
      $entity = $this->find($id);
    }
    
    if($entity){
      if(isset($data['frequency'])){
        $entity->setFrequency($data['frequency']);
      }

      if(isset($data['name'])){
        $entity->setName($data['name']);
      }

      if(isset($data['isPeriodic'])){
        $entity->setIsPeriodic($data['isPeriodic']);
      }

      if(isset($data['enabled'])){
        $entity->setDisabled(!$data['enabled']);
      }

   
      if($entity->getCronExpression()){
        $cronTab = explode(" ", $entity->getCronExpression());

        if(count( $cronTab ) < 5 ) {
          $cronTab =  array('1', '1', '1', '1', '*');
        }
      }

      // Dans tous les cas
      if(isset($data['hour'])){
        $cronTab[1] = $data['hour'];
      }
      if(isset($data['minute'])){
        $cronTab[0] = $data['minute'];
      }

      // Commande périodique
      if($entity->getIsPeriodic()){
        $cronTab[4] = (isset($data['dayWeek']) && is_numeric($data['dayWeek'])) ?  $data['dayWeek'] : '*';
        
        $cronTab[3] = "*";
        $cronTab[2] = "*";

        if( $entity->getFrequency( ) == "daily" ){
          $cronTab[4] = "*";
        }
      }
      elseif(isset($data['date'])  ) {
        $date = DateTime::createFromFormat('m/d/Y', $data['date']);
        if($date){
          // m h j m j
          $cronTab[2] = $date->format('d');
          $cronTab[3] = $date->format('m');
          $cronTab[4] = "*";
        
          $entity->setYear( $date->format('Y') );
        }
 
      }

      $entity->setCronExpression( implode(" ", $cronTab) );
    }
    
    try{
      //On enregistre dans la base
      $em->persist($entity);
      $em->flush();

      $isOk = true;
    }
    catch(Exception $e){
      $isOk = false;
    }

    return $isOk;

  }

  public function create( $data){
    $em = $this->getEntityManager();
    $isOk = false;
    $entity = new  ScheduledCommand();
    $cronTab = array('1', '1', '1', '1', '*');
    
    if($entity){
      $entity->setCommand('prodige:generateData');
      
      if(isset($data['frequency'])){
        $entity->setFrequency($data['frequency']);
      }

      if(isset($data['name'])){
        $entity->setName($data['name']);
      }

      if(isset($data['isPeriodic'])){
        $entity->setIsPeriodic($data['isPeriodic']);
      }
      else{
        $entity->setIsPeriodic(false);
      }

      if(isset($data['enabled'])){
        $entity->setDisabled(!$data['enabled']);
      }
     

      // Dans tous les cas
      if(isset($data['hour'])){
        $cronTab[1] = $data['hour'];
      }
      if(isset($data['minute'])){
        $cronTab[0] = $data['minute'];
      }

      // Commande périodique
      if($entity->getIsPeriodic()){
        if(isset($data['dayWeek'])){
          $cronTab[4] = $data['dayWeek'];
        }
        
        $cronTab[3] = "*";
        $cronTab[2] = "*";

        if( $entity->getFrequency( ) == "daily" ){
          $cronTab[4] = "*";
        }
      }
      elseif(isset($data['date'])  ) {
        $date = DateTime::createFromFormat('m/d/Y', $data['date']);
        if($date){
          // m h j m j
          $cronTab[2] = $date->format('d');
          $cronTab[3] = $date->format('m');
          $cronTab[4] = "*";
        
          $entity->setYear( $date->format('Y') );
        }
 
      }

      $entity->setCronExpression( implode(" ", $cronTab) );
    }
    
    try{
      //On enregistre dans la base
      $em->persist($entity);
      $em->flush();

      $isOk = true;
    }
    catch(Exception $e){
      $isOk = false;
    }

    return $isOk;

  }
  
  /**
   * Find all enabled command ordered by priority
   *
   * @return ScheduledCommand[]
   */
  public function findEnabledCommand()
  {
      return $this->findBy(array('disabled' => false, 'locked' => false), array('priority' => 'DESC'));
  }

  /**
   * findAll override to implement the default orderBy clause
   *
   * @return ScheduledCommand[]
   */
  public function findAll()
  {
      return $this->findBy(array(), array('priority' => 'DESC'));
  }

  /**
   * Find all locked commands
   *
   * @return ScheduledCommand[]
   */
  public function findLockedCommand()
  {
      return $this->findBy(array('disabled' => false, 'locked' => true), array('priority' => 'DESC'));
  }

  /**
   * Find all failed command
   *
   * @return ScheduledCommand[]
   */
  public function findFailedCommand()
  {
      return $this->createQueryBuilder('command')
          ->where('command.disabled = false')
          ->andWhere('command.lastReturnCode != 0')
          ->getQuery()
          ->getResult();
  }

  /**
   * @param integer|bool $lockTimeout
   * @return array|\App\JMose\CommandSchedulerBundle\Entity\ScheduledCommand[]
   */
  public function findFailedAndTimeoutCommands($lockTimeout = false)
  {
      // Fist, get all failed commands (return != 0)
    $failedCommands = $this->findFailedCommand();

    // Then, si a timeout value is set, get locked commands and check timeout
    if (false !== $lockTimeout) {
      $lockedCommands = $this->findLockedCommand();
      foreach ($lockedCommands as $lockedCommand) {
        $now = time();
        if ($lockedCommand->getLastExecution()->getTimestamp() + $lockTimeout < $now) {
          $failedCommands[] = $lockedCommand;
        }
      }
    }

    return $failedCommands;
  }

  /**
   * Supression de l'entity
   */
  public function delete($id) {
    $em = $this->getEntityManager();

    if( !is_null($id) ){
      $entity = $this->find($id);
    }

    if ($entity) {
     

      try{
        $em->remove($entity);
        $em->flush();
      }
      catch(Exception $e){
        return false;
      }
      
      return true;
    }
  }
}
