<?php

namespace App\JMose\CommandSchedulerBundle\Controller;

use App\JMose\CommandSchedulerBundle\Entity\ScheduledCommand;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ListController
 *
 * @author  Julien Guyon <julienguyon@hotmail.com>
 * @package JMose\CommandSchedulerBundle\Controller
 */
class ListController extends AbstractController
{
    protected $em;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->em = $managerRegistry;
    }
    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        $scheduledCommands = $this->em->getRepository(ScheduledCommand::class)->findAll();

        return $this->render(
            'JMoseCommandSchedulerBundle:List:index.html.twig',
            array('scheduledCommands' => $scheduledCommands)
        );
    }

    /**
     * @param $id
     * @return Response
     */
    public function removeAction($id): Response
    {

        $scheduledCommand = $this->em->getRepository(ScheduledCommand::class)->find($id);
        $entityManager    = $this->em->getManager('default');
        $entityManager->remove($scheduledCommand);
        $entityManager->flush();

        // Add a flash message and do a redirect to the list
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.deleted', array(), 'JMoseCommandScheduler'));

        return $this->redirect($this->generateUrl('jmose_command_scheduler_list'));
    }

    /**
     * @param $id
     * @return Response
     */
    public function toggleAction($id): Response
    {
        $scheduledCommand = $this->em->getRepository(ScheduledCommand::class)->find($id);
        $scheduledCommand->setDisabled(!$scheduledCommand->isDisabled());

        $this->em->getManager('default')->flush();

        return $this->redirect($this->generateUrl('jmose_command_scheduler_list'));
    }

    /**
     * @param $id
     * @return Response
     */
    public function executeAction($id): Response
    {
        $scheduledCommand = $this->em->getRepository(ScheduledCommand::class)->find($id);
        $scheduledCommand->setExecuteImmediately(true);
        $this->em->getManager('default')->flush();

        // Add a flash message and do a redirect to the list
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.execute', array(), 'JMoseCommandScheduler'));

        return $this->redirect($this->generateUrl('jmose_command_scheduler_list'));
    }

    /**
     * @param $id
     * @return Response
     */
    public function unlockAction($id): Response
    {
        $scheduledCommand = $this->em->getRepository(ScheduledCommand::class)->find($id);
        $scheduledCommand->setLocked(false);
        $this->em->getManager('default')->flush();

        // Add a flash message and do a redirect to the list
        $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flash.unlocked', array(), 'JMoseCommandScheduler'));

        return $this->redirect($this->generateUrl('jmose_command_scheduler_list'));
    }

    /**
     * method checks if there are jobs which are enabled but did not return 0 on last execution or are locked.<br>
     * if a match is found, HTTP status 417 is sent along with an array which contains name, return code and locked-state.
     * if no matches found, HTTP status 200 is sent with an empty array
     *
     * @return JsonResponse
     */
    public function monitorAction(): JsonResponse
    {
        $failedCommands = $this->em->getRepository(ScheduledCommand::class)->findFailedAndTimeoutCommands($this->container->getParameter('jmose_command_scheduler.lock_timeout'));

        $jsonArray = array();
        foreach ($failedCommands as $command) {
            $jsonArray[$command->getName()] = array(
                'LAST_RETURN_CODE'  => $command->getLastReturnCode(),
                'B_LOCKED'          => $command->getLocked() ? 'true' : 'false',
                'DH_LAST_EXECUTION' => $command->getLastExecution()
            );
        }

        $response = new JsonResponse();
        $response->setContent(json_encode($jsonArray));
        $response->setStatusCode(count($jsonArray) > 0 ? Response::HTTP_EXPECTATION_FAILED : Response::HTTP_OK);

        return $response;
    }
}
