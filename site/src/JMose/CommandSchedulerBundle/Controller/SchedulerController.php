<?php

namespace App\JMose\CommandSchedulerBundle\Controller;

use App\Bdterr\BdcomBundle\Controller\BdcomBaseController;
use App\JMose\CommandSchedulerBundle\Entity\ExecutionReport;
use App\JMose\CommandSchedulerBundle\Entity\ScheduledCommand;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Head;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/base_territoriale_admin/scheduler")
 */
class SchedulerController extends BdcomBaseController
{
    /**
     * @Head("",  name="scheduler_api", options={"expose"=true})
     */
    public function restAction()
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     *
     * @Get("/logfile/{report_id}")
     *
     * @RequestParam(name="from_app_dir",   description="", nullable = true, requirements="\d+", default="false" )
     *
     */
    public function downloadFileAction($report_id): BinaryFileResponse
    {
        $entity = $this->em->getRepository(ExecutionReport::class)->find($report_id);
        $filePath = $entity->getFile();

        $fs = new FileSystem();
        if (!$fs->exists($filePath)) {
            throw $this->createNotFoundException();
        }

        $filename = pathinfo($filePath)['filename'];
        $response = new BinaryFileResponse($filePath);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $filename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $filename)
        );

        return $response;
    }

    /**
     *
     * @deprecated
     */
    private function deleteAllLog()
    {
        // Nothing to do
    }

    /**
     * @Delete("/reports/{id}", defaults={"id"=null} )
     *
     */
    public function deleteFileReport($id): JsonResponse
    {
        $response = array(
            'id' => $id,
            'file' => array(),
        );

        if ($id === 'clearall') {
            $executionReports = $this->em->getRepository(ExecutionReport::class)->findAll();

            foreach ($executionReports as $report) { // iterate files
                $file = $report->getFile();
                $response[] = $file;
                if (is_file($file)) {
                    unlink($file); // delete file
                    $response[] = 'toDel';
                } else {
                    if (is_file('../'.$file)) {
                        unlink('../'.$file); // delete file
                        $response[] = 'toDel';
                    }
                }
                $this->em->getManager()->remove($report);
            }

            $this->em->getManager()->flush();
        } elseif (is_numeric($id)) {
            $report = $this->em->getRepository(ExecutionReport::class)->delete($id);
            if ($report) {
                $this->em->getManager()->remove($report);
                $this->em->getManager()->flush();
            }
        }

        return new JsonResponse(array_merge($response, array("success" => true)), 200);
    }

    /**
     *
     * @Get("/reports")
     *
     */
    public function getExecutionReportAction(): JsonResponse
    {
        $entities = $this->em->getRepository(ExecutionReport::class)->findAll();
        $response['ExecutionReport'] = ($entities ? json_decode($this->jsonSerializeEntity($entities), true) : array());

        return new JsonResponse(array_merge($response, array("success" => true)), 200);
    }

    /**
     *
     * @Get("/command")
     *
     */
    public function getAction(): JsonResponse
    {
        $entities = $this->em->getRepository(ScheduledCommand::class)->findAllForApi();
        $response['Scheduler'] = ($entities ?: array());

        return new JsonResponse(array_merge($response, array("success" => true)), 200);
    }

    /**
     * @Patch("/command/{id}", defaults={"id"=null},)
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function patchAction(Request $request, $id = null): JsonResponse
    {
        $isOk = false;
        $data = json_decode($request->getContent(), true);

        if ($data && $id) {
            $isOk = $this->em->getRepository(ScheduledCommand::class)->update($id, $data);
        }

        return new JsonResponse(
            array("message" => ($isOk ? "Mis à jour des données réussie." : "Problème pendant la mise à jour.")),
            $isOk ? 200 : 500
        );
    }

    /**
     * @Post("/command")
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function postAction(Request $request): JsonResponse
    {
        $isOk = false;
        $data = json_decode($request->getContent(), true);

        if ($data) {
            $isOk = $this->em->getRepository(ScheduledCommand::class)->create($data);
        }

        return new JsonResponse(
            array("message" => ($isOk ? "Mis à jour des données réussie." : "Problème pendant la mise à jour.")),
            $isOk ? 200 : 500
        );
    }

    /**
     * @Delete("/command/{id}", defaults={"id"=null} )
     *
     * @IsGranted("ROLE_USER")
     *
     */
    function deleteAction($id = null): JsonResponse
    {
        $isOk = false;
        if ($id) {
            $isOk = $this->em->getRepository(ScheduledCommand::class)->delete($id);
        }

        return new JsonResponse(
            array("message" => ($isOk ? "L'enregistrement à bien été suprrimé." : "Problème pour suprimer l'élément.")),
            $isOk ? 200 : 500
        );
    }

    /**
     * @Get("/execute/{id}")
     */
    function executeAction($id = null): JsonResponse
    {
        $scheduledCommand = $this->em->getRepository(ScheduledCommand::class)->find($id);

        if ($scheduledCommand) {
            $scheduledCommand->setExecuteImmediately(true);
            $scheduledCommand->setDisabled(false);

            $this->em->getManager()->persist($scheduledCommand);
            $this->em->getManager()->flush();

            $message = "La tâche a été planifiée à la prochaine minute";

            return new JsonResponse(array("message" => $message, "success" => true), 200);
        }

        return new JsonResponse(array("message" => "Commande non trouvée", "success" => false), 404);
    }
} 