#!/bin/bash
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

# Wait postgresql restart
sleep 3

OWNER="www-data"
function run_as {
  su -pc "$1" -s /bin/bash $OWNER
}

if [ ! -d "${SCRIPTDIR}/../public/upload/pdf/" ]
then
  echo "Create folder public/upload/pdf/"
  mkdir "${SCRIPTDIR}/../public/upload/pdf/"
  chown $OWNER:$OWNER "${SCRIPTDIR}/../public/upload/pdf/"
fi
if [ ! -d "${SCRIPTDIR}/../public/upload/odt/" ]
then
  echo "Create folder public/upload/odt/"
  mkdir "${SCRIPTDIR}/../public/upload/odt/"
  chown $OWNER:$OWNER "${SCRIPTDIR}/../public/upload/odt/"
fi

run_as "php $SCRIPTDIR/console cache:clear --env dev"
run_as "php $SCRIPTDIR/console cache:clear --env prod"

# Migrate database
run_as "php $SCRIPTDIR/console doctrine:migrations:migrate --no-interaction"
