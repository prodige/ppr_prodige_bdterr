<?php

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20220211114500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Bdcom - Add theme icon field';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE bdterr.bdterr_themes ADD COLUMN theme_icon character varying(255) DEFAULT NULL::character varying");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE bdterr.bdterr_themes DROP COLUMN theme_icon");
    }
}
