<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040103 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Ajout des contraintes de la base CATALOGUE';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('set search_path to public');

        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_donnee ADD CONSTRAINT fk_30055129141f7bf0 FOREIGN KEY (donnee_lot_id) REFERENCES bdterr.bdterr_lot(lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot ADD CONSTRAINT fk_4674d3dfa9ee0a03 FOREIGN KEY (lot_referentiel_id) REFERENCES bdterr.bdterr_referentiel_intersection(referentiel_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot ADD CONSTRAINT fk_4674d3dffbc885b3 FOREIGN KEY (lot_theme_id) REFERENCES bdterr.bdterr_themes(theme_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers ADD CONSTRAINT fk_4819e6af8ae06949 FOREIGN KEY (contenu_type) REFERENCES bdterr.bdterr_champ_type(champtype_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers ADD CONSTRAINT fk_4819e6afa8cba5f7 FOREIGN KEY (lot_id) REFERENCES bdterr.bdterr_lot(lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_rapports_profils ADD CONSTRAINT fk_51faf077d1eae64c FOREIGN KEY (bterr_rapport_id) REFERENCES bdterr.bdterr_rapports(rapport_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ ADD CONSTRAINT fk_52a0ca003dc30acd FOREIGN KEY (champ_lot_id) REFERENCES bdterr.bdterr_lot(lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ ADD CONSTRAINT fk_52a0ca00d43aa457 FOREIGN KEY (champ_type) REFERENCES bdterr.bdterr_champ_type(champtype_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_themes ADD CONSTRAINT fk_a06003abedb65433 FOREIGN KEY (theme_parent) REFERENCES bdterr.bdterr_themes(theme_id)");
        }

    public function down(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_donnee DROP CONSTRAINT fk_30055129141f7bf0");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot DROP CONSTRAINT fk_4674d3dfa9ee0a03");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot DROP CONSTRAINT fk_4674d3dffbc885b3");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers DROP CONSTRAINT fk_4819e6af8ae06949");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers DROP CONSTRAINT fk_4819e6afa8cba5f7");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_rapports_profils DROP CONSTRAINT fk_51faf077d1eae64c");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ DROP CONSTRAINT fk_52a0ca003dc30acd");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ DROP CONSTRAINT fk_52a0ca00d43aa457");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_themes DROP CONSTRAINT fk_a06003abedb65433");

    }
}
