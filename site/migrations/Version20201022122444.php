<?php

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20201022122444 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Bdcom V2 - Add new contenu tier type & mapping field in lot';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (2, 'text', true)");
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (3, 'date', false)");
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (4, 'url', true)");
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (5, 'image', true)");
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (6, 'vidéo', true)");
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (7, 'dossier-images', true)");
        $this->addSql("INSERT INTO bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) VALUES (8, 'dossier-autres-supports', true)");
        $this->addSql("ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_mapping_field text");
        $this->addSql("create table bdterr.bdterr_couche_jointure(
            id            serial  not null
            constraint bdterr_couche_jointure_pk primary key constraint bdterr_couche_jointure_bdterr_lot_lot_id_fk references bdterr.bdterr_lot,
            lot_id        integer not null,
            lot_field     text    not null,
            prodige_table text    not null,
            prodige_field text    not null
        );");
        $this->addSql("CREATE SEQUENCE bdterr.join_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM bdterr.bdterr_champ_type WHERE champtype_nom ='dosser-images' OR champtype_nom='dossier-autres-supports'");
    }
}
