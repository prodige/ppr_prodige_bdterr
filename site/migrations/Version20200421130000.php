<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421130000 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version tampon : Patch 4.2.7';
    }

    public function up(Schema $schema) : void
    {
       //Ajout champ tampon sur lots
       $this->addSql("ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_buffer integer DEFAULT 0;");
       //Ajout champs gestion type de découpage
       $this->addSql("ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_cutting_type text DEFAULT 'ref';");
       $this->addSql("ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_mapping text;");
         
    }

    public function down(Schema $schema) : void
    {

        $this->addSql("ALTER TABLE bdterr.bdterr_lot drop COLUMN lot_buffer;");
        $this->addSql("ALTER TABLE bdterr.bdterr_lot drop COLUMN lot_cutting_type ;");
        $this->addSql("ALTER TABLE bdterr.bdterr_lot drop COLUMN lot_mapping ;");

    }
}
