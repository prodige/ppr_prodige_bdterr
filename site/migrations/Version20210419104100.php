<?php

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210419104100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Bdcom V2 - Add new contenu tier type & mapping field in lot';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("DROP TABLE bdterr.bdterr_couche_jointure;");
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_couche_jointure(
            join_id  serial  not null constraint bdterr_couche_jointure_pk primary key,
            join_lot_id  integer not null constraint bdterr_couche_jointure_bdterr_lot_lot_id_fk references bdterr.bdterr_lot,
            join_lot_field text    not null,
            join_prod_table text    not null,
            join_prod_key text    not null,
            join_prod_field text    not null
            );"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql("DROP TABLE bdterr.bdterr_couche_jointure;");
    }
}

