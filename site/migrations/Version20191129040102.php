<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Ajout des données initiales de la base CATALOGUE';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to public');

        $this->addSql("INSERT INTO bdterr.bdterr_themes (theme_id, theme_parent, theme_nom, theme_alias, theme_ordre) VALUES (0, NULL, 'root', NULL, 0)");

        $this->addSql("SELECT pg_catalog.setval('bdterr.bdterr_referentiel_carto_referentiel_id_seq', 1, false)");

        $this->addSql("INSERT INTO bdterr.bdterr_referentiel_recherche (referentiel_id, referentiel_table, referentiel_insee, referentiel_insee_delegue, referentiel_commune, referentiel_nom, referentiel_api_champs) VALUES (1, '', '', '', '', '', '')");

        $this->addSql("SELECT pg_catalog.setval('bdterr.champ_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.champtype_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.contenu_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.donnee_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.lot_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.rapport_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.rapports_profils_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.referentiel_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.referentiel_recherche_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.theme_id_seq', 1, false)");

   }

    public function down(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to public');

        $this->addSql("delete from bdterr.bdterr_referentiel_carto");
        $this->addSql("delete from bdterr.bdterr_referentiel_recherche");
        $this->addSql("delete from bdterr.bdterr_donnee");
        $this->addSql("delete from bdterr.bdterr_referentiel_intersection");
        $this->addSql("delete from bdterr.bdterr_contenus_tiers");
        $this->addSql("delete from bdterr.bdterr_rapports");
        $this->addSql("delete from bdterr.bdterr_rapports_profils");
        $this->addSql("delete from bdterr.bdterr_lot");
        $this->addSql("delete from bdterr.bdterr_champ_type");
        $this->addSql("delete from bdterr.bdterr_couche_champ");
        $this->addSql("delete from bdterr.bdterr_themes");
        
    }
}
