<?php

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20220811000000 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Bdcom - Add statistic intersect boolean on search referential entity';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE bdterr.bdterr_referentiel_intersection ADD COLUMN referentiel_statistics_ref boolean DEFAULT false");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE bdterr.bdterr_referentiel_intersection DROP COLUMN referentiel_statistics_ref");
    }
}
