<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "Création des tables du schema bdterr dans catalogue";
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA bdterr');
        $this->addSql('set search_path to public');

        $this->addSql("CREATE TABLE bdterr.bdterr_champ_type (    champtype_id integer NOT NULL,    champtype_nom character varying(255) DEFAULT NULL::character varying,    champtype_incontenutier boolean DEFAULT true)");
        $this->addSql("CREATE TABLE bdterr.bdterr_contenus_tiers (    contenu_id integer NOT NULL,    lot_id integer,    contenu_type integer,    contenu_nom character varying(255) DEFAULT NULL::character varying,    contenu_ordre integer,    contenu_fiche boolean,    contenu_resultats boolean,    contenu_url character varying(255) NOT NULL)");
        $this->addSql("CREATE TABLE bdterr.bdterr_couche_champ (    champ_id integer NOT NULL,    champ_lot_id integer NOT NULL,    champ_type integer,    champ_nom character varying(150) NOT NULL,    champ_alias character varying(255) DEFAULT NULL::character varying,    champ_ordre integer,    champ_format character varying(255) DEFAULT NULL::character varying,    champ_fiche boolean,    champ_resultats boolean,    champ_identifiant boolean,    champ_label boolean)");
        $this->addSql("CREATE TABLE bdterr.bdterr_donnee (    donnee_id integer NOT NULL,    donnee_lot_id integer,    donnee_objet_id text,    donnee_objet_nom text,    donnee_geom text,    donnee_geom_json text,    donnee_attributs_fiche text,    donnee_attributs_resultat text,    donnee_ressources_resultat text,    donnee_ressources_fiche text,    donnee_insee text,    donnee_insee_delegue text)");
        $this->addSql("CREATE TABLE bdterr.bdterr_lot (    lot_id integer NOT NULL,    lot_theme_id integer,    lot_referentiel_id integer,    lot_uuid character varying(255) DEFAULT NULL::character varying,    lot_table character varying(255) DEFAULT NULL::character varying,    lot_alias character varying(255) DEFAULT NULL::character varying,    lot_carte character varying(255) DEFAULT NULL::character varying,    lot_avertissement_message character varying(255) DEFAULT NULL::character varying,    lot_statistiques boolean,    lot_visualiseur boolean,    lot_date_maj timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,    lot_statistic text,    lot_layer_name text)");
        $this->addSql("CREATE TABLE bdterr.bdterr_rapports (    rapport_id integer NOT NULL,    rapport_nom character varying(255) DEFAULT NULL::character varying,    rapport_gabarit_html character varying(255) DEFAULT NULL::character varying,    rapport_gabarit_odt character varying(255) DEFAULT NULL::character varying)");
        $this->addSql("CREATE TABLE bdterr.bdterr_rapports_profils (    id integer NOT NULL,    bterr_rapport_id integer,    prodige_profil_id integer)");
        $this->addSql("CREATE TABLE bdterr.bdterr_referentiel_carto (    referentiel_id integer NOT NULL,    referentiel_table text NOT NULL,    referentiel_champs text)");
        $this->addSql("CREATE SEQUENCE bdterr.bdterr_referentiel_carto_referentiel_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE TABLE bdterr.bdterr_referentiel_intersection (    referentiel_id integer NOT NULL,    referentiel_table character varying(255) DEFAULT NULL::character varying,    referentiel_insee character varying(255) DEFAULT NULL::character varying,    referentiel_insee_deleguee character varying(255) DEFAULT NULL::character varying,    referentiel_nom character varying(255) DEFAULT NULL::character varying)");
        $this->addSql("CREATE TABLE bdterr.bdterr_referentiel_recherche (    referentiel_id integer NOT NULL,    referentiel_table character varying(255) DEFAULT NULL::character varying,    referentiel_insee character varying(255) DEFAULT NULL::character varying,    referentiel_insee_delegue character varying(255) DEFAULT NULL::character varying,    referentiel_commune character varying(255) DEFAULT NULL::character varying,    referentiel_nom text,    referentiel_api_champs text)");
        $this->addSql("CREATE TABLE bdterr.bdterr_themes (    theme_id integer NOT NULL,    theme_parent integer,    theme_nom character varying(255) NOT NULL,    theme_alias text,    theme_ordre integer)");
        $this->addSql("CREATE SEQUENCE bdterr.champ_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.champtype_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.contenu_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.donnee_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.lot_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.rapport_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.rapports_profils_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.referentiel_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.referentiel_recherche_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("CREATE SEQUENCE bdterr.theme_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_champ_type ADD CONSTRAINT bdterr_champ_type_pkey PRIMARY KEY (champtype_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers ADD CONSTRAINT bdterr_contenus_tiers_pkey PRIMARY KEY (contenu_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ ADD CONSTRAINT bdterr_couche_champ_pkey PRIMARY KEY (champ_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_donnee ADD CONSTRAINT bdterr_donnee_pkey PRIMARY KEY (donnee_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot ADD CONSTRAINT bdterr_lot_pkey PRIMARY KEY (lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_rapports ADD CONSTRAINT bdterr_rapports_pkey PRIMARY KEY (rapport_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_rapports_profils ADD CONSTRAINT bdterr_rapports_profils_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_referentiel_carto ADD CONSTRAINT bdterr_referentiel_carto_pkey PRIMARY KEY (referentiel_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_referentiel_intersection ADD CONSTRAINT bdterr_referentiel_intersection_pkey PRIMARY KEY (referentiel_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_referentiel_recherche ADD CONSTRAINT bdterr_referentiel_recherche_pkey PRIMARY KEY (referentiel_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_themes ADD CONSTRAINT bdterr_themes_pkey PRIMARY KEY (theme_id)");
        $this->addSql("CREATE INDEX idx_30055129141f7bf0 ON bdterr.bdterr_donnee USING btree (donnee_lot_id)");
        $this->addSql("CREATE INDEX idx_4674d3dfa9ee0a03 ON bdterr.bdterr_lot USING btree (lot_referentiel_id)");
        $this->addSql("CREATE INDEX idx_4674d3dffbc885b3 ON bdterr.bdterr_lot USING btree (lot_theme_id)");
        $this->addSql("CREATE INDEX idx_4819e6af8ae06949 ON bdterr.bdterr_contenus_tiers USING btree (contenu_type)");
        $this->addSql("CREATE INDEX idx_4819e6afa8cba5f7 ON bdterr.bdterr_contenus_tiers USING btree (lot_id)");
        $this->addSql("CREATE INDEX idx_51faf077d1eae64c ON bdterr.bdterr_rapports_profils USING btree (bterr_rapport_id)");
        $this->addSql("CREATE INDEX idx_52a0ca003dc30acd ON bdterr.bdterr_couche_champ USING btree (champ_lot_id)");
        $this->addSql("CREATE INDEX idx_52a0ca00d43aa457 ON bdterr.bdterr_couche_champ USING btree (champ_type)");
        $this->addSql("CREATE INDEX idx_a06003abedb65433 ON bdterr.bdterr_themes USING btree (theme_parent)");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to public');
        $this->addSql('DROP SCHEMA bdterr cascade');

    }
}
