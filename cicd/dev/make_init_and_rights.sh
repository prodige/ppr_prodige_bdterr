#!/bin/bash

mkdir -p ./data/db/backup
find ./data/db/backup -type d -exec chmod 0755 {} \;
find ./data/db/backup -type f -exec chmod 0644 {} \;
