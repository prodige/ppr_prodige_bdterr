# 4 modes d'installation de Prodige avec les conteneurs Docker

## Caractéristiques communes

 - Utilisation de docker-compose pour la gestion des dockers composant Prodige.
 - Les volumes ./data conservent les paramétrages et données persistantes ;
 - Les volumes ./var correspondent aux données persistantes qui n'ont pas besoin d'être sauvegardées (journaux, imports).

## Prérequis
 - Vous devez lancez le projet [pr_prodige](https://gitlab.adullact.net/prodigeadmin/pr_prodige)
 - En fonction de votre choix d'installation :

## A - Installation pour du test ou du développement

 - Utilise [docker-compose.yml](../docker-compose.yml) et [docker-compose.override.yml](../docker-compose.override.yml) avec un docker proxy (défini dans [docker-compose.override.yml](../docker-compose.override.yml))pour les configurations Nginx et les certificats.
 - Les conteneurs sont téléchargés.
 - Voir fichier [installation.md](installation.md)

## B - Installation sur serveur de production

 - Utilise [docker-compose.yml](../docker-compose.yml) et [production.yml](../docker-compose-custom/production.yml)
 - Les conteneurs sont téléchargés.
 - Voir fichier [installation_production.md](installation_production.md)

## C - Installation sur serveur de production sans le docker proxy

 - Utilise [docker-compose.yml](../docker-compose.yml), [production.yml](../docker-compose-custom/production.yml) et [docker-compose.nginx.yml](../nginx/docker-compose.nginx.yml)
 - Les conteneurs sont téléchargés.
 - Les certificats et la configuration du serveur web sont gérés en dehors du projet pr_prodige.
 - Voir fichier [installation_production_avec_nginx.md](installation_production_avec_nginx.md)

## D - Installation avec une construction locale des conteneurs

 - Utilise [docker-compose.yml](../docker-compose.yml) [docker-compose.override.yml](../docker-compose.override.yml) et [local_build.yml](../docker-compose-custom/local_build.yml).
 - Les conteneurs sont construits localement.
 - Les tags des dockers sont à ```latest``` (objet de [local_build.yml](../docker-compose-custom/local_build.yml)).
 - Voir fichier [installation_construction_locale.md](installation_construction_locale.md)
