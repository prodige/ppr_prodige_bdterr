# Installation de Prodige bdterr en local

## Principes

 - Utilise docker-compose.yml qui utilise le même réseau docker que le projet pr_prodige

## Initialisation des volumes de données

### Création des dossiers
Dossier data
```bash
mkdir -p data/bdterr/conf/
mkdir -p data/bdterr/upload
chmod -R o+rx data
chown 33:33 data/bdterr/upload
```
Dossier var
```bash
mkdir -p var/bdterr/log_symfony
chown 33:33 var/bdterr/log_symfony
mkdir -p var/bdterr/log
```

### Récupération de global_parameters.yml
Récupérer le fichier global_parameters.yml du projet catalogue du pr_prodige
```bash
sudo cp -a <path pr_prodige>/data/catalogue/conf/global_parameters.yml data/bdterr/conf/global_parameters.yml
```

### Modification de global_parameters.yml
Vous devez modifier la variable cas_proxy_chain pour qu'il inclut votre nom de domain pour bdterr

Fichier : data/bdterr/conf/global_parameters.yml
```
cas_proxy_chain: ^https?://(catalogue.prodige.internal|admincarto.prodige.internal|adminsite.prodige.internal|telecarto.prodige.internal|carto.prodige.internal|print.prodige.internal|bdterr.prodige.internal).*
```

### Adapter le chemin du pr_prodige
Dans le fichier .env vous devez spécifier le chemin du projet pr_prodige
```
PR_PRODIGE_PATH=../../../pr_prodige
```
Par défaut si ppr_prodige_bdterr est à côté de pr_prodige, vous n'avez rien à modifier.


## Connexion au docker registry
- Utilisateur : prodige
- Mot de passe : Yx@KN,ZXzR9u@

```bash
docker login docker.alkante.com -u prodige
docker-compose pull
```

## Domaines

Si les domaines ne sont pas publics (tests), ajouter dans ```/etc/hosts``` une ligne du type :

```
127.0.0.3 bdterr.prodige.internal
```

## Modification du projet pr_prodige
### Modification du proxy nginx
Vous devez ajouter la configuration de bdterr dans la conf nginx. Une configuration d'exemple est en place dans pr_prodige

Vous devez vous placer dans le dossier pr_prodige
```bash
sudo cp -a data/proxy/conf_optional/bdterr.conf data/proxy/conf
```

### Modification du CAS apereo
Le CAS apereo filtre les URL autoriser a se connecter dessus, vous devez ajouter l'URL' de bdterr. Une configuration d'exemple est en place dans pr_prodige

Vous devez vous placer dans le dossier pr_prodige
```bash
sudo cp -a data/cas/services_optional/prodigebdterr-100005.json data/cas/services/
```

## Démarrage de Prodige bdterr (domaine .prodige.internal)
Lancement des applicatifs
```
docker-compose up -d
```

## Prise en compte de la conf pr_prodige
```
docker restart jpr_prodige_cas_web prodige-proxy
```

## Journaux
```
docker-compose logs -f
```

## Vérification du bon démarrage
Bdterr (migration de bdd):
```
docker-compose logs ppr_prodige_bdterr_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```

## Arrêt
```
docker-compose down
```
