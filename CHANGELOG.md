# Changelog

All notable changes to [ppr_prodige_bdterr](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr) project will be documented in this file.

## [1.1.34](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.33...1.1.34) - 2024-07-19

## [1.1.33](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.32...1.1.33) - 2024-07-19

## [1.1.32](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.31...1.1.32) - 2024-07-08

## [1.1.31](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.30...1.1.31) - 2024-07-08

## [1.1.30](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.29...1.1.30) - 2024-07-08

## [1.1.29](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.28...1.1.29) - 2023-03-22

## [1.1.28](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.27...1.1.28) - 2023-03-22

## [1.1.27](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.26...1.1.27) - 2023-03-15

## [1.1.26](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.25...1.1.26) - 2023-02-27

## [1.1.25](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.24...1.1.25) - 2023-02-27

## [1.1.24](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.23...1.1.24) - 2023-02-27

## [1.1.23](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.22...1.1.23) - 2023-02-27

## [1.1.22](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.21...1.1.22) - 2023-02-14

## [1.1.21](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.20...1.1.21) - 2023-02-10

## [1.1.20](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.19...1.1.20) - 2023-01-12

## [1.1.19](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.18...1.1.19) - 2023-01-11

## [1.1.18](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.17...1.1.18) - 2023-01-11

## [1.1.17](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.16...1.1.17) - 2023-01-11

## [1.1.16](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.15...1.1.16) - 2023-01-11

## [1.1.15](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.14...1.1.15) - 2023-01-10

## [1.1.14](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.13...1.1.14) - 2023-01-05

## [1.1.13](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.12...1.1.13) - 2023-01-05

## [1.1.12](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.11...1.1.12) - 2022-12-02

## [1.1.11](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.10...1.1.11) - 2022-12-02

## [1.1.10](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.9...1.1.10) - 2022-12-02

## [1.1.9](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.8...1.1.9) - 2022-12-02

## [1.1.8](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.7...1.1.8) - 2022-12-02

## [1.1.7](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.6...1.1.7) - 2022-12-02

## [1.1.6](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.5...1.1.6) - 2022-12-02

## [1.1.5](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.4...1.1.5) - 2022-12-02

## [1.1.4](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.3...1.1.4) - 2022-12-02

## [1.1.3](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.2...1.1.3) - 2022-11-30

## [1.1.2](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.1...1.1.2) - 2022-11-30

## [1.1.1](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.1.0...1.1.1) - 2022-11-30

## [1.1.0](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.18...1.1.0) - 2022-11-30

## [1.0.18](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.17...1.0.18) - 2022-11-09

## [1.0.17](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.16...1.0.17) - 2022-11-09

## [1.0.16](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.15...1.0.16) - 2022-11-08

## [1.0.15](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.14...1.0.15) - 2022-11-08

## [1.0.14](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.13...1.0.14) - 2022-10-04

## [1.0.13](https://gitlab.adullact.net/prodige/ppr_prodige_bdterr/compare/1.0.12...1.0.13) - 2022-09-22

