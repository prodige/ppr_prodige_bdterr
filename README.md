# Module optionnel PRODIGE base territoriale

Module intégrant un back-office de configuration et une API à destination d'un front-office de présentation des résultats de découpage de données par commune.

### configuration

Modifier le fichier site/app/config/global_parameters.yml

### Installation

[documentation d'installation en production](cicd/prod/README.md).

[documentation d'installation en développement](cicd/dev/README.md).

## Accès à l'application

- [Accès au back-office](https://bdterr.prodige.internal/base_territoriale_admin).
- [Accès à l'API](https://bdterr.prodige.internal/base_territoriale_admin/).
